window.app = window.app || {};
app.collections = app.collections || {};

app.collections.tagsList = Backbone.Collection.extend({
    initialize : function() {
        this.model = app.models.tagsList;
    },
});
