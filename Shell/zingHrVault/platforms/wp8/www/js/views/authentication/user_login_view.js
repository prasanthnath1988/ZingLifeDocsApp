;
(function() {
	app.views.userLogin = Backbone.Marionette.ItemView.extend({
		template : '#userLoginView',
		events : {
			'click .login_h' : 'doLogin',
			'click .fb_login_h' : 'doFbLogin',
			'submit form' : 'doLogin',
			'keyup #userEmailAdd' : 'doneHandling',
			'keyup #userPassword' : 'doneHandling',
			'focusout #userEmailAdd':'handlingKeyBoard',
			'focusout #userPassword':'handlingKeyBoard'

		},
		initialize : function() {
			_.bindAll(this, 'doLogin');
			App.sessionVars.signupData = '';
		},
		doneHandling : function(e) {
			if (e.keyCode == 13) {
				this.doLogin();
			}
		},
		handlingKeyBoard:function(e){
			if (App.settings.isDeviceReady && cordova && cordova.plugins && cordova.plugins.Keyboard) {
				cordova.plugins.Keyboard.close();
			}
		},
		doLogin : function() {
			this.handlingKeyBoard();
			var _this = this;
			console.log("login button clicked");

			var checkNetworkConnection = PG_checkNetwork();
			if (checkNetworkConnection.result) {

				var uEmailAdd = this.$el.find("#userEmailAdd").val();
				var uPassword = this.$el.find("#userPassword").val();

				var emailPattern = /^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$/;
				if (uEmailAdd && uPassword) {
					if (emailPattern.exec(uEmailAdd)) {
						if (Parse) {
							App.controller.showPopup('loader');
							Parse.User.logIn(uEmailAdd, uPassword, {
								success : function(user) {//setting the installation id against the user name
									console.log("______________Intalation_ID_:  " + App.sessionVars.pushInstallationId);
									user.set("Installation_Id", App.sessionVars.pushInstallationId);                                    user.set("push_objectId", App.sessionVars.ObjectpushInstallationId);
                                    user.set("push_objectId", App.sessionVars.ObjectpushInstallationId);
									user.save(null, {
										success : function(user) {
											window.localStorage.setItem("user_Password", uPassword);
											window.localStorage.setItem("user_emailId", uEmailAdd);
											console.log("_____________Login Success_________");
											console.log(JSON.stringify(user));

											if (_this.$el.find('#signedCheck').is(':checked')) {
												App.sessionVars.keepMeSignedIn = 1;
												localStorage.setItem('keepMeSignedIn',1);

											} else {
												App.sessionVars.keepMeSignedIn = 0;
												localStorage.setItem('keepMeSignedIn',0);

											}

											App.models.user = new app.models.user();
											App.models.user.set(user);
											App.vent.trigger("closePopups");
											if (App.sessionVars.pushInstallationId) {
												_this.updatePushInstallationTable(uEmailAdd, App.sessionVars.pushInstallationId,App.sessionVars.ObjectpushInstallationId)
											} else {
												App.routers.mainRouter.navigate('#userHome', {
													trigger : true
												});
											}

										}
									});
								},
								error : function(user, error) {
									// The login failed. Check error to see why.
									App.vent.trigger("closePopups");
									if (error.code == 101) {
										console.log("invalid userid and password");
										var options = {
											"title" : App.settings.messageTitle.failureTitle,
											"message" : "Invalid userid or password"
										}
										App.controller.showPopup('alert', options);
										_this.$el.find("#userPassword").val("");

									} else {
										var options = {
											"title" : App.settings.messageTitle.failureTitle,
											"message" : "No Internet Connection"
										}
										App.controller.showPopup('alert', options);
										_this.$el.find("#userPassword").val("");

									}
								}
							});
						}

					} else {
						console.log("Email id is not valid");
						var options = {
							"title" : App.settings.messageTitle.failureTitle,
							"message" : App.settings.messages.inValidEmailId
						}
						App.controller.showPopup('alert', options);
					}
				} else {
					console.log("enter valid email ID and password");

					var options = {
						"title" : App.settings.messageTitle.failureTitle,
						"message" : App.settings.messages.blankFieldLoginMsg
					}
					App.controller.showPopup('alert', options);
				}

			} else {
				var options = {
					"title" : App.settings.messageTitle.failureTitle,
					"message" : App.settings.messageSubject.connectionLoss
				}
				App.controller.showPopup('alert', options);
			}

		},
		doFbLogin : function(e) {
			console.log("FB login intiate");
			var checkNetworkConnection = PG_checkNetwork();
			if (checkNetworkConnection.result) {
				PG_loginFacebook();
			} else {
				var options = {
					"title" : App.settings.messageTitle.failureTitle,
					"message" : App.settings.messageSubject.connectionLoss
				};
				App.controller.showPopup('alert', options);
			}
		},
		onRender : function() {

		},
		onClose : function() {
			console.log("VIEW CLOSE");
		},
		updatePushInstallationTable : function(userEmailId, deviceId, objectId) {


			var updatePushTable = Parse.Object.extend("pushInstallationTable");
			var changePushTable = new updatePushTable();
			var query = new Parse.Query(updatePushTable);
			query.equalTo("Installation_Id", deviceId);
			App.controller.showPopup('loader');
			query.find({
				success : function(results) {

					if (results.length > 0) {
						console.log(results[0].id);
						var changePushTable = new updatePushTable();
						var query = new Parse.Query(updatePushTable);
						changePushTable.id = results[0].id;
						changePushTable.set('Installation_Id', '');
						changePushTable.save(null, {
							success : function() {
								var changePushTable = new updatePushTable();
								var query = new Parse.Query(updatePushTable);
								query.equalTo("email", userEmailId);
								query.find({
									success : function(results) {
										if (results.length > 0) {
											var changePushTable = new updatePushTable();
											var query = new Parse.Query(updatePushTable);
											changePushTable.id = results[0].id;
											changePushTable.set('Installation_Id', deviceId);
                                    		changePushTable.set('push_objectId', objectId);
											changePushTable.save(null, {
												success : function() {
													App.vent.trigger("closePopups");
													App.routers.mainRouter.navigate('#userHome', {
														trigger : true
													});
												},
												error : function(e) {
													App.vent.trigger("closePopups");
													var options = {
														"title" : App.settings.messageTitle.failureTitle,
														"message" : App.settings.messageSubject.connectionLoss
													}
													App.controller.showPopup('alert', options);
												}
											});
										} else {

											var changePushTable = new updatePushTable();
											var query = new Parse.Query(updatePushTable);
											changePushTable.set('Installation_Id', deviceId);
                                    		changePushTable.set('push_objectId', objectId);
											changePushTable.set("email", userEmailId);

											changePushTable.save(null, {
												success : function() {
													App.vent.trigger("closePopups");
													App.routers.mainRouter.navigate('#userHome', {
														trigger : true
													});
												},
												error : function(e) {
													var options = {
														"title" : App.settings.messageTitle.failureTitle,
														"message" : App.settings.messageSubject.connectionLoss
													}
													App.controller.showPopup('alert', options);
												}
											});
										}
									},
									error : function(e) {
										App.vent.trigger("closePopups");
										var options = {
											"title" : App.settings.messageTitle.failureTitle,
											"message" : App.settings.messageSubject.connectionLoss
										}
										App.controller.showPopup('alert', options);
									}
								});
							},
							error : function(e) {
								App.vent.trigger("closePopups");
								var options = {
									"title" : App.settings.messageTitle.failureTitle,
									"message" : App.settings.messageSubject.connectionLoss
								}
								App.controller.showPopup('alert', options);
							}
						});
					} else {

						var changePushTable = new updatePushTable();
						var query = new Parse.Query(updatePushTable);
						query.equalTo("email", userEmailId);
						query.find({
							success : function(results) {
								if (results.length > 0) {
									var changePushTable = new updatePushTable();
									var query = new Parse.Query(updatePushTable);
									changePushTable.id = results[0].id;
									changePushTable.set('Installation_Id', deviceId);
                                    changePushTable.set('push_objectId', objectId);
									changePushTable.save(null, {
										success : function() {
											App.routers.mainRouter.navigate('#userHome', {
												trigger : true
											});
										},
										error : function(e) {
											var options = {
												"title" : App.settings.messageTitle.failureTitle,
												"message" : App.settings.messageSubject.connectionLoss
											}
											App.controller.showPopup('alert', options);
										}
									});
								} else {
									var changePushTable = new updatePushTable();
									var query = new Parse.Query(updatePushTable);

									changePushTable.set('Installation_Id', deviceId);
                                    changePushTable.set('push_objectId', objectId);
									changePushTable.set("email", userEmailId);

									changePushTable.save(null, {
										success : function() {
											App.routers.mainRouter.navigate('#userHome', {
												trigger : true
											});
										},
										error : function(e) {
											var options = {
												"title" : App.settings.messageTitle.failureTitle,
												"message" : App.settings.messageSubject.connectionLoss
											}
											App.controller.showPopup('alert', options);
										}
									});
								}
							},
							error : function(e) {

								var options = {
									"title" : App.settings.messageTitle.failureTitle,
									"message" : App.settings.messageSubject.connectionLoss
								}
								App.controller.showPopup('alert', options);
							}
						});

					}

					// Do something with the returned Parse.Object values
				},
				error : function(error) {
					var options = {
						"title" : App.settings.messageTitle.failureTitle,
						"message" : "No Internet Connection"
					}
					App.controller.showPopup('alert', options);
				}
			});
		}
	});
})();
