window.app = window.app || {};
app.views = app.views || {};
app.views.uploadOptions = Backbone.Marionette.ItemView.extend({
	template: '#uploadOptions',
	events: {
		'click .close_h': 'closePopup',
		'click .take_from_camera_h':'takeImageFromCamera',
		'click .take_from_gallery_h':'takeImageFromGallery',
	},
	initialize: function() {
		App.vent.trigger("closePopups");
		_.bindAll(this, 'takeImageFromCamera','takeImageFromGallery');
	},
	onRender: function() {
		if(this.options.title){
			this.$el.find('.list_title').text(this.options.title);
		}
	},
	closePopup: function() {
		App.vent.trigger("closePopups");
	},
	takeImageFromCamera : function(){
		var _this =this;
		console.log("images from Camera");

		navigator.camera.getPicture(onSuccess, onFail, {
			quality : 70,
			destinationType : Camera.DestinationType.DATA_URL,
			sourceType : Camera.PictureSourceType.CAMERA,
			allowEdit : true,
			encodingType : Camera.EncodingType.JPEG,
			targetWidth : 500,
			targetHeight : 500,
			popoverOptions : CameraPopoverOptions,
			saveToPhotoAlbum : false,
			correctOrientation : true
		});
		function onSuccess(imageData) {
			console.log("Image URI success");
			App.vent.trigger("closePopups");
			
			if(_this.options.from && _this.options.from == "userProfile"){
				App.sessionVars.profileImageData = imageData;
				App.pages.settings.$el.find('.imagesIds_profile').attr('src', "data:image/jpeg;base64," + App.sessionVars.profileImageData);
			}
			else if(_this.options.from && _this.options.from == "signature"){
				App.sessionVars.signatureImageData = imageData;
				App.pages.settings.$el.find('.imagesIds_signature').attr('src', "data:image/jpeg;base64," + App.sessionVars.signatureImageData);
				App.pages.settings.$el.find('.imagesIds_signature').show();
        		App.pages.settings.$el.find('#signature .signatureUpload').text('Edit Signature');
        		App.pages.settings.$el.find('#signature .signatureUpload').addClass('upload_pic_edit');
        		App.pages.settings.$el.find('#signature .signatureUpload').removeClass('upload_pic');
			}
			else{
				App.sessionVars.imageData = imageData;
				App.routers.mainRouter.navigate('#imageDetails/', {
					trigger : true
				});
			}
		}
		function onFail(message) {
			var options = {
					"title":App.settings.messageTitle.failureTitle,
					"message":'You closed camera, please try again'
			};
			App.controller.showPopup('alert',options);
		}
	},
	takeImageFromGallery : function(){
		console.log("images from Gallery");
		var _this =this;
		navigator.camera.getPicture(onSuccess, onFail, {
			quality : 100,
			destinationType : Camera.DestinationType.DATA_URL,
			sourceType : Camera.PictureSourceType.SAVEDPHOTOALBUM,
			allowEdit : true,
			encodingType : Camera.EncodingType.JPEG,
			targetWidth : 500,
			targetHeight : 500,
			popoverOptions : CameraPopoverOptions,
			saveToPhotoAlbum : false,
			correctOrientation : true
		});
		function onSuccess(imageData) {

			console.log("Image URI success");
			App.vent.trigger("closePopups");
			
			if(_this.options.from && _this.options.from == "userProfile"){
				App.sessionVars.profileImageData = imageData;
				App.pages.settings.$el.find('.imagesIds_profile').attr('src', "data:image/jpeg;base64," + App.sessionVars.profileImageData);
			}
			else if(_this.options.from && _this.options.from == "signature"){
				App.sessionVars.signatureImageData = imageData;
				App.pages.settings.$el.find('.imagesIds_signature').attr('src', "data:image/jpeg;base64," + App.sessionVars.signatureImageData);
				App.pages.settings.$el.find('.imagesIds_signature').show();
        		App.pages.settings.$el.find('#signature .signatureUpload').text('Edit Signature');
        		App.pages.settings.$el.find('#signature .signatureUpload').addClass('upload_pic_edit');
        		App.pages.settings.$el.find('#signature .signatureUpload').removeClass('upload_pic');
			}else{
				App.sessionVars.imageData = imageData;
				App.routers.mainRouter.navigate('#imageDetails/', {
					trigger : true
				});
			}
		
		}
		function onFail(message) {
			App.vent.trigger("closePopups");
			var options = {
					"title":App.settings.messageTitle.failureTitle,
					"message":'You did not choose any file, please try again'
			};
			App.controller.showPopup('alert',options);
		}
	},
	
	onClose: function() {
		
	}
});