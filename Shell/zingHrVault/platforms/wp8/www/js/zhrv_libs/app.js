/**
 * APP JS
 * This is the main app file with core regions, common functions, params, etc
 *
 * The Whole app will be namespaced into the App variable (app[objects] & App[instances]) for flexibility
 */
;
(function() {
    // Creates a new Marionette application.
    _.extend(App, new Backbone.Marionette.Application());
    // Add the main region, that will hold the page layout.
    App.addInitializer(function(options) {
        _.extend(App.regions, App.addRegions({
            header: app.regions.header,
            menu:app.regions.menu,
            body: app.regions.body,
            popup: app.regions.popup,
            footer: app.regions.footer
        }));
        // Adds any methods to be run after the app was initialized.
    });
    // Start backbone's history for hash navigation after the app was initialized.
    // Fires just after the initializers have finished
    App.on('initialize:after', function(options) {
               console.log("App initialized");
        App.controller.showHeader();
        App.controller.showFooter();
        App.controller.initialize();
        App.routers.mainRouter = new app.routers.AppRouter({
            controller: App.controller
        });
        App.routers.mainRouter.on('beforeroute', App.controller.beforeRoute);
        App.vent.on('closePopups', App.controller.popupsClose);
        App.vent.on('checkNetworkStatus', App.controller.checkNetwork);
        App.vent.on('page:load', App.controller.onPageLoad);
        if (Backbone.history) {
            Backbone.history.start();
        }
    });
    App.applyBodyID = function(DOMid) {
        this.$el = this.$el || $('body');
        this.$el.attr('id', DOMid + '_Body');
    };
})();