;
(function() {
	app.views.signUp = Backbone.Marionette.ItemView.extend({
		template : '#signUpView',
		events : {
			'click .signup_h' : 'doSignUp',
			'click .cancel_h' : 'cancelSignup',
			'click .terms_h' : 'showTerms',
			'change #userCountry' : 'addClassinSelected',
			'submit form' : 'doSignUp',
			'keyup #userConfirmPassword' : 'doneHandling',
			'keyup #userPassword' : 'doneHandling',
			'keyup #userEmailAdd' : 'doneHandling',
			'keyup #userName' : 'doneHandling',
			'focusout #userName':'handlingKeyBoard',
			'focusout #userEmailAdd':'handlingKeyBoard',
			'focusout #userPassword':'handlingKeyBoard',
			'focusout #userConfirmPassword':'handlingKeyBoard'

		},
		doneHandling : function(e) {
			if (e.keyCode == 13) {
				this.doSignUp();
			}
		},
		initialize : function() {
			_.bindAll(this, 'doSignUp');
		},

		onRender : function(e) {
			if(App.sessionVars.signupData!="undefined" && App.sessionVars.signupData!=undefined)
			{
				this.$el.find("#userName").val(App.sessionVars.signupData.userName);
				this.$el.find("#userEmailAdd").val(App.sessionVars.signupData.email);
				this.$el.find("#userPassword").val(App.sessionVars.signupData.userPassword);
				this.$el.find("#userConfirmPassword").val(App.sessionVars.signupData.userConfirmPassword);
			}
		},
		addClassinSelected : function() {
			if (this.$el.find("#userCountry").val() != 'country') {
				this.$el.find("#userCountry").addClass('text_white');
			} else {
				this.$el.find("#userCountry").removeClass('text_white');
			}
		},
		showTerms : function(){

			var userName = $("#userName").val();
        	var email =$("#userEmailAdd").val();
        	var userPassword =$("#userPassword").val();
        	var userConfirmPassword =$("#userConfirmPassword").val();

        	App.sessionVars.signupData = {
        			'userName':userName,
        			'email':email,
        			'userPassword':userPassword,
        			'userConfirmPassword':userConfirmPassword
        	}; 

			App.routers.mainRouter.navigate('#terms', {
			    trigger : true
			});
		},
		doSignUp : function() {

			App.sessionVars.signupData = '';
			//this.handlingKeyBoard();
			var _this = this;
			console.log("native signup initiate");
			var userName = this.$el.find("#userName").val();
			//			var uLastName = this.$el.find("#userLastName").val();
			var uAddress = this.$el.find("#userAddress").val();
			var uEmailAdd = this.$el.find("#userEmailAdd").val();
			var uPassword = this.$el.find("#userPassword").val();
			var promo = this.$el.find("#promoCode").val();
			var uConfirmPassword = this.$el.find("#userConfirmPassword").val();
			var uCountry = $('#userCountry').val();

			if (userName && uEmailAdd && uPassword && uConfirmPassword) {
				var emailPattern = /^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$/;
				if (!emailPattern.exec(uEmailAdd)) {
					var options = {
						"title" : App.settings.messageTitle.failureTitle,
						"message" : App.settings.messages.inValidEmailId
					};
					App.controller.showPopup('alert', options);
					return false;
				}

				var passwordPattern = /\s/g;
				if (passwordPattern.exec(uPassword) || passwordPattern.exec(uConfirmPassword)) {
					var options = {
						"title" : App.settings.messageTitle.failureTitle,
						"message" : App.settings.messages.invalidSingupPassword
					};
					App.controller.showPopup('alert', options);
					return false;
				}

				if (uPassword != uConfirmPassword) {
					console.log("password not matching");
					var options = {
						"title" : App.settings.messageTitle.failureTitle,
						"message" : App.settings.messages.passwordNotMatchMsg
					};
					App.controller.showPopup('alert', options);
					this.$el.find("#userPassword").val("");
					this.$el.find("#userConfirmPassword").val("");

					return false;
				}

				if (!this.$el.find('.terms_condition_check_h').is(':checked')) {
					var options = {
						"title" : App.settings.messageTitle.failureTitle,
						"message" : App.settings.messages.acceptTermsAndConditions
					}
					App.controller.showPopup('alert', options);
					return false;
				}

				if (Parse && Parse.User) {
					var user = new Parse.User();
					user.set("username", uEmailAdd);
					user.set("password", uPassword);
					user.set("email", uEmailAdd);
					user.set("name", userName);
					user.set("promo", promo);

					if (uAddress) {
						user.set("address", uAddress);
					}
					if (uCountry) {
						user.set("Country", uCountry);
					}

					var checkNetworkConnection = PG_checkNetwork();
					if (checkNetworkConnection.result) {
						App.controller.showPopup('loader');
						user.signUp(null, {
							success : function(user) {
								console.log("______________Intalation_ID_:  " + App.sessionVars.pushInstallationId);
								user.set("Installation_Id", App.sessionVars.pushInstallationId);
								user.set("push_objectId", App.sessionVars.ObjectpushInstallationId);
								user.save(null, {
									success : function(user) {

										App.models.user = new app.models.user();
										App.models.user.set(user);
										App.vent.trigger("closePopups");
										if (App.sessionVars.pushInstallationId) {
											setTimeout(function(){
												App.controller.showPopup('alert', {
												"title" : "Welcome to Zing-LifeDocs",
												"message" : "A highly secured cloud platform for your documents"
											});   
											}, 3000);                                            
											_this.updatePushInstallationTable(uEmailAdd, App.sessionVars.pushInstallationId,App.sessionVars.ObjectpushInstallationId)

										} else {
											App.routers.mainRouter.navigate('#userHome', {
												trigger : true
											});
											setTimeout(function(){
												App.controller.showPopup('alert', {
												"title" : "Welcome to Zing-LifeDocs",
												"message" : "A highly secured cloud platform for your documents"
											});   
											}, 3000);
												
										}

									}
								});

								// Hooray! Let them use the app now.
							},
							error : function(user, error) {
								App.vent.trigger("closePopups");
								// Show the error message somewhere and let the user try again.
								if (error.code == 202) {

									var options = {
										"title" : App.settings.messageTitle.failureTitle,
										"message" : "Email address already taken."
									}

								} else {
									var options = {
										"title" : App.settings.messageTitle.failureTitle,
										"message" : "No Internet Connection"
									}
								}

								App.controller.showPopup('alert', options);
								//								alert("Error: " + error.code + " " + error.message);
							}
						});
					} else {
						App.vent.trigger("closePopups");
						var options = {
							"title" : App.settings.messageTitle.failureTitle,
							"message" : App.settings.messageSubject.connectionLoss
						}
						App.controller.showPopup('alert', options);
					}
				} else {
					console.log("Error----- Parse not initialise.");
				}
			} else {
				var options = {
					"title" : App.settings.messageTitle.failureTitle,
					"message" : App.settings.messages.blankFieldLoginMsg
				}
				App.controller.showPopup('alert', options);
			}
		},
		onClose : function() {
			console.log("VIEW CLOSE");
		},
		cancelSignup : function() {
			App.sessionVars.signupData = '';
			App.routers.mainRouter.navigate('#userLogin/logout', {
				trigger : true
			})
		},
		updatePushInstallationTable : function(userEmailId, deviceId, objectId) {
			var updatePushTable = Parse.Object.extend("pushInstallationTable");
			var changePushTable = new updatePushTable();
			var query = new Parse.Query(updatePushTable);
			query.equalTo("Installation_Id", deviceId);
			App.controller.showPopup('loader');
			query.find({
				success : function(results) {

					if (results.length > 0) {
						console.log(results[0].id);
						var changePushTable = new updatePushTable();
						var query = new Parse.Query(updatePushTable);
						changePushTable.id = results[0].id;
						changePushTable.set('Installation_Id', '');
						changePushTable.save(null, {
							success : function() {
								var changePushTable = new updatePushTable();
								var query = new Parse.Query(updatePushTable);
								query.equalTo("email", userEmailId);
								query.find({
									success : function(results) {
										if (results.length > 0) {
											var changePushTable = new updatePushTable();
											var query = new Parse.Query(updatePushTable);
											changePushTable.id = results[0].id;
											changePushTable.set('Installation_Id', deviceId);
											changePushTable.set('push_objectId', objectId);
											changePushTable.save(null, {
												success : function() {
													App.vent.trigger("closePopups");
													App.routers.mainRouter.navigate('#userHome', {
														trigger : true
													});
												},
												error : function(e) {
													App.vent.trigger("closePopups");
													var options = {
														"title" : App.settings.messageTitle.failureTitle,
														"message" : App.settings.messageSubject.connectionLoss
													}
													App.controller.showPopup('alert', options);
												}
											});
										} else {

											var changePushTable = new updatePushTable();
											var query = new Parse.Query(updatePushTable);
											changePushTable.set('Installation_Id', deviceId);
											changePushTable.set('push_objectId', objectId);
											changePushTable.set("email", userEmailId);

											changePushTable.save(null, {
												success : function() {
													setTimeout(function(){
													console.log('-----------in set--------');
													App.controller.showPopup('alert', {
													"title" : "Welcome to Zing LifeDocs",
													"message" : "A highly secured cloud platform for your documents"
												});   
												}, 3000);
													App.vent.trigger("closePopups");
													App.routers.mainRouter.navigate('#userHome', {
														trigger : true
													});
												},
												error : function(e) {
													var options = {
														"title" : App.settings.messageTitle.failureTitle,
														"message" : App.settings.messageSubject.connectionLoss
													}
													App.controller.showPopup('alert', options);
												}
											});
										}
									},
									error : function(e) {
										App.vent.trigger("closePopups");
										var options = {
											"title" : App.settings.messageTitle.failureTitle,
											"message" : App.settings.messageSubject.connectionLoss
										}
										App.controller.showPopup('alert', options);
									}
								});
							},
							error : function(e) {
								App.vent.trigger("closePopups");
								var options = {
									"title" : App.settings.messageTitle.failureTitle,
									"message" : App.settings.messageSubject.connectionLoss
								}
								App.controller.showPopup('alert', options);
							}
						});
					} else {

						var changePushTable = new updatePushTable();
						var query = new Parse.Query(updatePushTable);
						query.equalTo("email", userEmailId);
						query.find({
							success : function(results) {
								if (results.length > 0) {
									var changePushTable = new updatePushTable();
									var query = new Parse.Query(updatePushTable);
									changePushTable.id = results[0].id;
									changePushTable.set('Installation_Id', deviceId);
									changePushTable.set('push_objectId', objectId);
									changePushTable.save(null, {
										success : function() {
											App.routers.mainRouter.navigate('#userHome', {
												trigger : true
											});
										},
										error : function(e) {
											var options = {
												"title" : App.settings.messageTitle.failureTitle,
												"message" : App.settings.messageSubject.connectionLoss
											}
											App.controller.showPopup('alert', options);
										}
									});
								} else {
									var changePushTable = new updatePushTable();
									var query = new Parse.Query(updatePushTable);

									changePushTable.set('Installation_Id', deviceId);
									changePushTable.set('push_objectId', objectId);
									changePushTable.set("email", userEmailId);

									changePushTable.save(null, {
										success : function() {
											App.routers.mainRouter.navigate('#userHome', {
												trigger : true
											});
										},
										error : function(e) {
											var options = {
												"title" : App.settings.messageTitle.failureTitle,
												"message" : App.settings.messageSubject.connectionLoss
											}
											App.controller.showPopup('alert', options);
										}
									});
								}
							},
							error : function(e) {

								var options = {
									"title" : App.settings.messageTitle.failureTitle,
									"message" : App.settings.messageSubject.connectionLoss
								}
								App.controller.showPopup('alert', options);
							}
						});

					}

					// Do something with the returned Parse.Object values
				},
				error : function(error) {
					var options = {
						"title" : App.settings.messageTitle.failureTitle,
						"message" : "No Internet Connection"
					}
					App.controller.showPopup('alert', options);
				}
			});
		}
	});
})();
//alskdjhaskjdklasjdlkajs