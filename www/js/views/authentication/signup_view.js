;
(function() {
	app.views.signUp = Backbone.Marionette.ItemView.extend({
		template : '#signUpView',
		events : {
			'click .signup_h':'doSignUp',
			'click .cancel_h':'cancelSignup',
			'change #userCountry':'addClassinSelected',
			'submit form':'doSignUp'

		},
		initialize : function() {
		},

		onRender : function() {
		},
		addClassinSelected : function(){
			if(this.$el.find("#userCountry").val() != 'country'){
				this.$el.find("#userCountry").addClass('text_white')
			}else{
				this.$el.find("#userCountry").removeClass('text_white')
			}
		},
		doSignUp :function(){
			console.log("native signup initiate");
			var userName = this.$el.find("#userName").val();
//			var uLastName = this.$el.find("#userLastName").val();
			var uAddress =  this.$el.find("#userAddress").val();
			var uEmailAdd = this.$el.find("#userEmailAdd").val();
			var uPassword = this.$el.find("#userPassword").val();
			var uConfirmPassword = this.$el.find("#userConfirmPassword").val();
			var uCountry = $('#userCountry').val();

			if(userName && uEmailAdd && uPassword && uConfirmPassword){
				
				var emailPattern = /^\w+@[a-zA-Z_]+?\.[a-zA-Z]{2,3}$/;
				if(!emailPattern.exec(uEmailAdd)){
					var options = {
							"title":App.settings.messageTitle.failureTitle,
							"message":App.settings.messages.inValidEmailId
					}
					App.controller.showPopup('alert',options);
					return false;
				}
				
				if(uPassword != uConfirmPassword){
					console.log("password not matching");
					var options = {
							"title":App.settings.messageTitle.failureTitle,
							"message":App.settings.messages.passwordNotMatchMsg
					}
					App.controller.showPopup('alert',options);
					this.$el.find("#userPassword").val("");
					this.$el.find("#userConfirmPassword").val("");
					
					return false;
				}
				
				if(!this.$el.find('.terms_condition_check_h').is(':checked')){
					var options = {
							"title":App.settings.messageTitle.failureTitle,
							"message":App.settings.messages.acceptTermsAndConditions
					}
					App.controller.showPopup('alert',options);
					return false;
				}
				
				if(Parse && Parse.User){
					var user = new Parse.User();
					user.set("username", uEmailAdd);
					user.set("password", uPassword);
					user.set("email", uEmailAdd);
					user.set("name",userName);
					if(uAddress){
						user.set("address",uAddress);
					}
					if(uCountry){
						user.set("Country",uCountry);
					}
					
//					if(uLastName){
//						user.set("lastName", uLastName);
//					}
					



					var checkNetworkConnection = PG_checkNetwork();
					if (checkNetworkConnection.result) {
						App.controller.showPopup('loader');
						user.signUp(null, {
							success: function(user) {
								console.log("______________Intalation_ID_:  "+App.sessionVars.pushInstallationId);
								user.set("Installation_Id", App.sessionVars.pushInstallationId);
		                        user.save(null, {
		                            success : function(user) {
		                            	                                
		                                App.models.user = new app.models.user();
		                                App.models.user.set(user);
		                                App.vent.trigger("closePopups");
		                                App.routers.mainRouter.navigate('#userHome', {
		                                    trigger : true
		                                });
		                            }
		                        });
								
								// Hooray! Let them use the app now.
							},
							error: function(user, error) {
								App.vent.trigger("closePopups");
								// Show the error message somewhere and let the user try again.
								if(error.code == 202){
									
									var options = {
											"title":App.settings.messageTitle.failureTitle,
											"message":"Email address already taken."
									}
									
								}else{
									var options = {
											"title":App.settings.messageTitle.failureTitle,
											"message":error.code + " " +error.message
									}
								}
								
								App.controller.showPopup('alert',options);
//								alert("Error: " + error.code + " " + error.message);
							}
						});
					}else{
						App.vent.trigger("closePopups");
						var options = {
								"title":App.settings.messageTitle.failureTitle,
								"message":App.settings.messageSubject.connectionLoss
						}
						App.controller.showPopup('alert',options);
					}
				}else{
					console.log("Error----- Parse not initialise.");
				}
			}else{
				var options = {
						"title":App.settings.messageTitle.failureTitle,
						"message":App.settings.messages.blankFieldLoginMsg
				}
				App.controller.showPopup('alert',options);
			}
		},
		onClose : function() {
			console.log("VIEW CLOSE");
		},
		cancelSignup : function(){
			App.routers.mainRouter.navigate('#userLogin/logout', {
				trigger : true
			})
		}
	});
})();
//alskdjhaskjdklasjdlkajs