;
(function() {
	app.pages.settings = Backbone.Marionette.Layout.extend({
		template: '#settingsLayout',
		events:{
			'click .upload_profile_h':'updateProfileImage',
			'click .save_user_settings_h':'saveSettings',
			'click .cancel_user_settings_h':'cancelSettings'
		},
		regions: {
			content: ".content_wrapper_h"
		},
		init: function() {
			console.log("settings LAYOUT INIT");
			_.bindAll(this,'updateProfileImage','saveSettings');
			
		},
		updateProfileImage : function(){
			var _this = this;
			console.log("upload popup clicked");
			App.controller.showPopup('uploadOptions',{
				'from':'userProfile',
				'title':'Profile photo'
			});
		},
		saveSettings: function(){
			var _this = this;
			var expiraryDays = this.$el.find('#daysToExpire').val();

			var daysValidation = /^\d{1,3}$/;

			if(!(daysValidation.exec(expiraryDays) && expiraryDays >0)){
				var options = {
						"title" : App.settings.messageTitle.failureTitle,
						"message" : "Share documents expiry days must be in positive number days within range 1 to 999"
				}
				App.controller.showPopup('alert', options);
				return false;
			}

			var checkNetworkConnection = PG_checkNetwork();
			if (checkNetworkConnection.result) {
				if(App.sessionVars.profileImageData && App.sessionVars.profileImageData.length > 0){

					App.controller.showPopup('loader');

					

					var parseFile = new Parse.File(
							"user_profile_pic.jpg", {
								base64 : App.sessionVars.profileImageData
							});

					parseFile.save().then(function(parseFile) {
						// The file has been saved to Parse. file's URL is only available 
						//after you save the file or after you get the file from a Parse.Object.
						//Get the function url() on the Parse.File object.
						var url = parseFile.url();
						console.log(parseFile.url());
						var CurrentUser = Parse.User.current();
						CurrentUser.set("userProfilePicture",url);
						CurrentUser.set("linkExpiryDays",expiraryDays);
						CurrentUser.save(null,
								{
							success : function(user) {
								console.log(JSON.stringify(user));
								App.vent.trigger("closePopups");
								if(App.models.user){
									App.models.user.set(user);
								}else{
									App.models.user = new app.models.user();
									App.models.user.set(user);	
								}
								var options = {
										"title":App.settings.messageTitle.successTitle,
										"message":"Settings updated successfully",
										"callBack": function(){
											App.controller.backButtonHandler({
												'from':'userSettings'
											});
										}
								}
								App.controller.showPopup('alert',options);
							},
							error : function(e) {
								App.vent.trigger("closePopups");
								console.log("Oh crap",e);
								var options = {
										"title":App.settings.messageTitle.failureTitle,
										"message":App.settings.messages.genericError
								}
								App.controller.showPopup('alert',options);
							}
								});

					}, 
					function(error) {
						// The file either could not be read, or could not be saved to Parse.
						console.log("Error: " + error.code + " " + error.message);

						App.vent.trigger("closePopups");
						console.log("Oh crap",error);
						var options = {
								"title":App.settings.messageTitle.failureTitle,
								"message":error.message
						}
						App.controller.showPopup('alert',options);

					});

				}else {
					var CurrentUser = Parse.User.current();
					CurrentUser.set("linkExpiryDays",expiraryDays);
					CurrentUser.save(null,
							{
						success : function(user) {
							console.log(JSON.stringify(user));
							App.vent.trigger("closePopups");
							if(App.models.user){
								App.models.user.set(user);
							}else{
								App.models.user = new app.models.user();
								App.models.user.set(user);	
							}
							
							var options = {
									"title":App.settings.messageTitle.successTitle,
									"message":"Settings updated successfully",
									"callBack": function(){
										App.controller.backButtonHandler({
											'from':'userSettings'
										});
									}
							}
							App.controller.showPopup('alert',options);
						},
						error : function(e) {
							App.vent.trigger("closePopups");
							console.log("Oh crap",e);
							var options = {
									"title":App.settings.messageTitle.failureTitle,
									"message":App.settings.messages.genericError
							}
							App.controller.showPopup('alert',options);
						}
							});
				}

			}else{
				App.vent.trigger("closePopups");
				var options = {
						"title":App.settings.messageTitle.failureTitle,
						"message":App.settings.messageSubject.connectionLoss
				}
				App.controller.showPopup('alert',options);
			}
		},
		cancelSettings: function(){
			var options = {
					"title" : 'Are you sure to discard changes.',
					"callBack":function(){
						App.controller.backButtonHandler({
							'from':'userSettings'
						});
					}
			}
			App.controller.showPopup('confirm', options);
		},
		onCloseLayout: function() {
			console.log("LAYOUT CLOSE");
			App.sessionVars.profileImageData ="";
		},
		onRender:function(){
			if(App.models.user && App.models.user.get('attributes.linkExpiryDays')){

				this.$el.find('#daysToExpire').val(App.models.user.get('attributes.linkExpiryDays'));

			}else{
				this.$el.find('#daysToExpire').val(5);
			}
			
			if(App.models.user && App.models.user.get('attributes.userProfilePicture')){
				this.$el.find('#imagesIds').attr('src', App.models.user.get('attributes.userProfilePicture'));
			}else if(App.models.user && App.models.user.get('attributes.userFBProfilePicture')){
        		this.$el.find('#imagesIds').attr('src', App.models.user.get('attributes.userFBProfilePicture'));
        	}else{
        		this.$el.find('#imagesIds').attr('src','imgs/user_img.jpg')
        	}
			
		}
	});
})();