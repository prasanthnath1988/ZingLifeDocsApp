function pushNotificationSetup(pushnotifications){
	if ( ((navigator.userAgent.toLocaleLowerCase().indexOf('android') != -1) )){
		pushnotifications.register(
				successHandler,
				errorHandler,
				{
					"senderID":"replace_with_sender_id",
					"ecb":"onNotification"
				});
	} else {
		pushnotifications.register(
				tokenHandler,
				errorHandler,
				{
					"badge":"true",
					"sound":"true",
					"alert":"true",
					"ecb":"onNotificationAPN"
				});
	}
}

//iOS
function onNotificationAPN (e) {
	if(e.foreground != 1){
		var image_url = e.image_url;
		var route =  e.action;
		if(Parse.User.current()){
			App.routers.mainRouter.navigate(route + '/' + image_url, {
				trigger : true
			});
		}else{
			App.vent.trigger("closePopups");
			App.controller.loadPage('userLogin', null, null);
		}
		pushNotification.setApplicationIconBadgeNumber(successHandler, errorHandler,0 );
	}else{
        var image_url = e.image_url;
		var route =  e.action;
		if(Parse.User.current()){
			App.routers.mainRouter.navigate(route + '/' + image_url, {
                                            trigger : true
                                            });
		}else{
			App.vent.trigger("closePopups");
			App.controller.loadPage('userLogin', null, null);
		}
    }
	if ( e.sound )
	{
		var snd = new Media(e.sound);
		snd.play();
	}
	if ( e.badge )
	{
		pushNotification.setApplicationIconBadgeNumber(successHandler, errorHandler, event.badge);
	}
}
//Android and Amazon Fire OS
function onNotification(e) {
	switch( e.event )
	{
	case 'registered':
		if ( e.regid.length > 0 )
		{
			// Your GCM push server needs to know the regID before it can push to this device
			// here is where you might want to send it the regID for later use.
			console.log("regID = " + e.regid);
		}
		break;
	case 'message':
		// if this flag is set, this notification happened while we were in the foreground.
		// you might want to play a sound to get the user's attention, throw up a dialog, etc.
		if ( e.foreground ){
//			var image_url = e.payload.data.image_url;
//			var route =  e.payload.data.action;
//			console.log("___________Image URL is: "+image_url+" _______________route: "+ route);
//			if(Parse.User.current()){
//				App.routers.mainRouter.navigate(route + '/' + image_url, {
//					trigger : true
//				});
//			}else{
//				App.vent.trigger("closePopups");
//				App.controller.loadPage('userLogin', null, null);
//			}
		}else{
			
			var image_url = e.payload.data.image_url;
			var route =  e.payload.data.action;
			console.log("___________Image URL is: "+image_url+" _______________route: "+ route);
			if(Parse.User.current()){
				App.routers.mainRouter.navigate(route + '/' + image_url, {
					trigger : true
				});
			}else{
				App.vent.trigger("closePopups");
				App.controller.loadPage('userLogin', null, null);
			}
		}

		break;

	case 'error':

		break;

	default:
		console.log("in default");

	var image_url = e.payload.data.image_url;
	var route =  e.payload.data.action;

	App.controller.showPopup('loader');
	if(Parse.User.current()){
		App.routers.mainRouter.navigate(route + '/' + image_url, {
			trigger : true
		});
	}else{
		App.vent.trigger("closePopups");
		App.controller.loadPage('userLogin', null, null);
	}
	break;
	}
}

//result contains any message sent from the plugin call
function successHandler (result) {
	console.log('result = ' + result);

}


//result contains any error description text returned from the plugin call
function errorHandler (error) {
	console.log('error = ' + error);
}

function tokenHandler (result) {
	// Your iOS push server needs to know the token before it can push to this device
	// here is where you might want to send it the token for later use.
	console.log('device token = ' + result);
}