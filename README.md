Zing LifeDocs
===============

Version: Version 0.1

APK File Location:
IPA File Location

Brief description of the application: Document vault for ZingHR, an application where you can upload and tag docs related to your life.

Release Date: 

Author: Nikhil

Platform: Android 2.3+, iOS 7+
				
Environment:	
 1. JVM-version 1.6.0_34
 2. JDK-version 1.6.0_65
 3. Android SDK- version 4.4.2
 4. OS- Version 10.9.2
 5. Xcode- version 5.1	

Framework and technology stack:
	
1. Marionette backbone.js
2. Cloudmailin
3. Parse.js backend
4. Node.js for cloudmailing integration

Build Type :

Native:NA

Hybrid : NA

Cross Platform native : Yes

Change Log: NA 

Bugs: NA

Device Permissions: NA

To be Implemented: NA
=================================================================================
Milestone for next Major Version :version 1.0

To Do:


Bugs fix: NA
===================================================================================
Milestone for next Major Version :version 2.0

To Do:

Bugs fixes: 
=====================================================================================
Note:
NA- Note applicable.
None of the fields in the template should be left empty.
