/**
 * APP objects JS
 *
 * Creating everything here to prevent addning conditionals each time an object needs to be created
 *
 * File contains the list of all global variables used in the application as well as abstractions for
 * objects/variables we may nullify by not including them in the mobile website code
 *
 */
/**
 * IMPORTANT:
 * 1. Any _foo objects/variables are not to be accessed directly under any circumstance.
 *    They have other internal functionals that use them for various purposed
 * 2. Any Values in app settings objects are not be modified on runtime in any case
 * 3. all variables/objects marked as FOO or BAR, are to used as read OLNY constant values in the application,
 *    they should not be modified at any cost.
 */
// CLASSES
window.app = window.app || {};
app.views = app.views || {};
app.models = app.models || {};
app.collections = app.collections || {};
app.pages = app.pages || {};
app.routers = app.routers || {};
app.regions = app.regions || {};
// INSTANCES
window.App = window.App || {};
App.views = App.views || {};
App.models = App.models || {};
App.collections = App.collections || {};
App.pages = App.pages || {};
App.regions = App.regions || {};
App.routers = App.routers || {};
App.templates = App.templates || {};
App.communication = App.communication || {};
App.sessionVars = App.sessionVars || {};
App.settings = App.settings || {};
App.session = App.session || {
    logged: false,
    device: {
        iOS: (navigator.userAgent.match(/iPhone/i) || navigator.userAgent.match(/iPod/i) || navigator.userAgent.match(/iPad/i)),
        android: (navigator.userAgent.match(/android/i))
    }
};