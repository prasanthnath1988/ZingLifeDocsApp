/**
 * Name    : AppRouter
 * Purpose : Maps application urls to specific methods in the controller.
 * Params  : --
 * Returns : --
 **/
window.app = window.app || {};
app.routers.AppRouter = Backbone.Marionette.AppRouter.extend({
    appRoutes : {
        '' : 'index',
        'userLogin/*options' : 'showUserLogin',
        'userHome' : 'showUserHome',
        'forgotPwd' : 'showForgotPwd',
        'signUp' : 'showSignUp',
        'dataList/:tagId' : 'showDataList',
        'imageDetails/*imageUrl' : 'showImageDetails',
        'settings' : 'showSettings',
        'imageZoom':'showImageZoomDetails',
        'contactus':'showContactUs',
        'howtouse' : 'showHowToUse'
    },
    /**
     * Name    : route
     * Purpose : Flexible customization of Backbone.Router.route in order to trigger a 'beforeroute' event before routing.
     * Params  : route - the url to navigate, name - the route name, callback - the callback method to be called after route.
     * Returns : --
     **/
    route : function(route, name, callback) {
        return Backbone.Router.prototype.route.call(this, route, name, function() {
            //  This event could be particularly useful for managing the regions, append classes and loaders and for the mobile version (ex. integration with jQuery mobile)
            this.trigger.apply(this, ['beforeroute', route, {
                'e' : event,
                'name' : name
            }].concat(_.toArray(arguments)));
            callback.apply(this, arguments);
        });
    }
});
