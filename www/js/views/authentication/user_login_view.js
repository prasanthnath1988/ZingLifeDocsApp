;
(function() {
    app.views.userLogin = Backbone.Marionette.ItemView.extend({
        template : '#userLoginView',
        events : {
            'click .login_h' : 'doLogin',
            'click .fb_login_h' : 'doFbLogin',
            'submit form':'doLogin'
        },
        initialize : function() {
        	_.bindAll(this,'doLogin');
        },
        doLogin : function() {
        	var _this = this;
            console.log("login button clicked");

            var checkNetworkConnection = PG_checkNetwork();
            if (checkNetworkConnection.result) {

                var uEmailAdd = this.$el.find("#userEmailAdd").val();
                var uPassword = this.$el.find("#userPassword").val();
                
                var emailPattern = /^\w+@[a-zA-Z_]+?\.[a-zA-Z]{2,3}$/;
                if (uEmailAdd && uPassword) {
                    if (emailPattern.exec(uEmailAdd)) {
                        if (Parse) {
                            App.controller.showPopup('loader');
                            Parse.User.logIn(uEmailAdd, uPassword, {
                                success : function(user) {  //setting the installation id against the user name
                                	console.log("______________Intalation_ID_:  "+App.sessionVars.pushInstallationId);
                                    user.set("Installation_Id", App.sessionVars.pushInstallationId);
                                    user.save(null, {
                                        success : function(user) {
                                        	window.localStorage.setItem("user_Password",uPassword);
                                        	window.localStorage.setItem("user_emailId",uEmailAdd);
                                            console.log("_____________Login Success_________");
                                            console.log(JSON.stringify(user));
                                            
                                            App.models.user = new app.models.user();
                                            App.models.user.set(user);
                                            App.vent.trigger("closePopups");
                                            App.routers.mainRouter.navigate('#userHome', {
                                                trigger : true
                                            });
                                        }
                                    });
                                },
                                error : function(user, error) {
                                    // The login failed. Check error to see why.
                                    App.vent.trigger("closePopups");
                                    if (error.code == 101) {
                                        console.log("invalid userid and password");
                                        var options = {
                                            "title" : App.settings.messageTitle.failureTitle,
                                            "message" : "Invalid userid or password"
                                        }
                                        App.controller.showPopup('alert', options);
                                        _this.$el.find("#userPassword").val("");
                                        
                                    }else{
                                    	var options = {
                                                "title" : App.settings.messageTitle.failureTitle,
                                                "message" : error.message
                                            }
                                            App.controller.showPopup('alert', options);
                                    	_this.$el.find("#userPassword").val("");
                                    	
                                    }
                                }
                            });
                        }

                    } else {
                        console.log("Email id is not valid");
                        var options = {
                            "title" : App.settings.messageTitle.failureTitle,
                            "message" : App.settings.messages.inValidEmailId
                        }
                        App.controller.showPopup('alert', options);
                    }
                } else {
                    console.log("enter valid email ID and password");

                    var options = {
                        "title" : App.settings.messageTitle.failureTitle,
                        "message" : App.settings.messages.blankFieldLoginMsg
                    }
                    App.controller.showPopup('alert', options);
                }

            } else {
                var options = {
                    "title" : App.settings.messageTitle.failureTitle,
                    "message" : App.settings.messageSubject.connectionLoss
                }
                App.controller.showPopup('alert', options);
            }

        },
        doFbLogin : function(e) {
            console.log("FB login intiate");
            var checkNetworkConnection = PG_checkNetwork();
            if (checkNetworkConnection.result) {
                PG_loginFacebook();
            } else {
                var options = {
                    "title" : App.settings.messageTitle.failureTitle,
                    "message" : App.settings.messageSubject.connectionLoss
                }
                App.controller.showPopup('alert', options);
            }
        },
        onRender : function() {
        	

        },
        onClose : function() {
            console.log("VIEW CLOSE");
        },
    });
})();
