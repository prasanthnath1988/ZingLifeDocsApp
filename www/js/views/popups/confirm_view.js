window.app = window.app || {};
app.views = app.views || {};
app.views.confirm = Backbone.Marionette.ItemView.extend({
    template: '#confirmView',
    events: {
        'click .close_h': 'closePopup',
        'click .ok_callback_h': 'okCallback',
        'click .cancel_callback_h': 'closePopup'
    },
    initialize: function() {
        App.vent.trigger("closePopups");
    },
    okCallback: function(e) {
        console.log("confirm ok callback");
        if (this.options.callBack) {
            this.options.callBack();
        }
        
        if(!(this.options.from && this.options.from=='logout')){
        	
        	App.vent.trigger("closePopups");
        }
        
        
    },
    onRender: function() {
        var _this = this;
        if (_this.options.title) {
            _this.$el.find('.confirmation_h').html(_this.options.title);
        }
        
        if(_this.options.from == "exitPop" || _this.options.from == "logout"){
        	_this.$el.find('.ok_callback_h').addClass('red');
        }
    },
    closePopup: function() {
        App.vent.trigger("closePopups");
        
    },
    onClose: function() {}
});