;
(function() {
    app.pages.userHome = Backbone.Marionette.Layout.extend({
        template: '#userHomeLayout',
        regions: {
            content: ".content_wrapper_h"
        },
        init: function() {
            console.log("USER HOME LAYOUT INIT");
            App.views.userHome = new app.views.userHome;
            this.content.show(App.views.userHome);
        },
        onCloseLayout: function() {
            console.log("LAYOUT CLOSE");
        }
    });
})();