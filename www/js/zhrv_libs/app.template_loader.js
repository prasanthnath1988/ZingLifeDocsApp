/**
 * Name    : --
 * Purpose : Method to load all the templates.
 * Params  : --
 * Returns : --
 **/
;
(function() {
    $.ajax({
        url: 'templates/templates.html',
        success: function(data) {
            $('head').append(data);
        },
        error: function() {
            console.log("ERROR LOADING TEMPLATE");
        }
    });
})();