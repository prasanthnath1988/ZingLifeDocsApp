;(function() {
	app.views.imageDetails = Backbone.Marionette.ItemView.extend({
		template : '#imageDetailsView',
		events : {

			"click .upload_document_h": "uploadImages",
			"click .cancel_image_upload_h":"cancelImageUpload"

		},
		initialize : function() {

			_.bindAll(this,'uploadImages','saveOffLinePic' );
			App.sessionVars.imageDetailsTags =  this.options.imageUrl;

		},
		onRender : function() {

			this.$el.find('#imagesIds').attr('src', "");
			if(this.options.imageUrl == 'editDocs'){
				this.$el.find('#imagesIds').attr('src', App.sessionVars.imageZoomOptions.imageUrl);
				this.$el.find("#documentName").val(App.sessionVars.imageZoomOptions.imageName);
				this.$el.find("#documentsTags").val(App.sessionVars.imageZoomOptions.imageTags);
				this.$el.find('.upload_document_h').text('Update');
				this.$el.find('.save_image_offline_h').css('display','none');
			}else{
				if(this.options.imageUrl && this.options.imageUrl != "editDocs"){
					this.$el.find('.doc_img').children("img").attr('src', this.options.imageUrl);
					this.$el.find('.upload_document_h').text('Upload');
					this.$el.find('.save_image_offline_h').css('display','');
					function convertImgToBase64(url, callback, outputFormat){
						var canvas = document.createElement('CANVAS'),
						ctx = canvas.getContext('2d'),
						img = new Image;
						img.crossOrigin = 'Anonymous';
						img.onload = function(){
							var dataURL;
							canvas.height = img.height;
							canvas.width = img.width;
							ctx.drawImage(img,0,0);
							dataURL = canvas.toDataURL(outputFormat || 'image/png');
							dataURL = dataURL.replace(/^data:image\/(png|jpg);base64,/, "");
							callback.call(this, dataURL);
							canvas = null; 
						};
						img.src = url;
					}
					convertImgToBase64(this.options.imageUrl, function(base64Img){
						// Base64DataURL
						console.log(base64Img);
						App.sessionVars.imageData = base64Img;
					});

				}else{
					this.$el.find('#imagesIds').attr('src', "data:image/jpeg;base64," + App.sessionVars.imageData);
					this.$el.find('.upload_document_h').text('Upload');
					this.$el.find('.save_image_offline_h').css('display','');
					if(!App.sessionVars.mainTagsFalse){
						this.$el.find("#documentsTags").val(App.sessionVars.mainTags);
						this.$el.find("#documentName").val("");
					}else{
						this.$el.find("#documentsTags").val("");
						this.$el.find("#documentName").val("");
					}
				}

			}


		},
		onClose : function() {
			console.log("VIEW CLOSE");
		},
		uploadImages : function(imagesData){
			var _this = this;
			var documentName = this.$el.find("#documentName").val().toLowerCase();
			var documentTags = this.$el.find("#documentsTags").val().toLowerCase();
			if(!documentName){
				var options = {
						"title":"Name Missing!!",
						"message":"Please provide valid name to document."
				}
				App.controller.showPopup('alert',options);
				return false;
			}

			if(!documentTags){
				var options = {
						"title":"Tags Missing!!",
						"message":"Please provide valid tags to document."
				}
				App.controller.showPopup('alert',options);
				return false;
			}



			App.controller.showPopup('loader');
			var checkNetworkConnection = PG_checkNetwork();
			if (checkNetworkConnection.result) {
				if(this.options.imageUrl == 'editDocs'){
					//Image Edit Section Start 
					// Create the object.
					var userUploadedImage = Parse.Object.extend("user_uploaded_images");
					var editImageData = new userUploadedImage();
					editImageData.id = App.sessionVars.imageZoomOptions.imageObjectId;

					editImageData.set("DocumentsTags", documentTags);
					editImageData.set("DocumentName", documentName);

					editImageData.save(null, {
						success: function(gameScore) {
							App.vent.trigger("closePopups");
							App.sessionVars.imageZoomOptions.imageName = documentName
							App.sessionVars.imageZoomOptions.imageTags = documentTags;
							var options = {
									"title":"Sucess!",
									"message":"Document updated successfully.",
									"callBack":function(){
										App.routers.mainRouter.navigate('#imageZoom', {
											trigger : true
										});
									}

							}
							App.controller.showPopup('alert',options);
						}
					});
					//Image Edit Section End
				}else{
					//Upload new image Start
					var parseFile = new Parse.File(
							documentName.replace(/\s+/g,'_').trim().toLowerCase()+".jpg", {
								base64 : App.sessionVars.imageData
							});
					var UserImages = Parse.Object.extend("user_uploaded_images");
					parseFile.save().then(function() {

						var user_images = new UserImages();
						if(documentName){
							user_images.set("DocumentName",documentName);
						}
						if(documentTags){
							user_images.set("DocumentsTags",documentTags);
						}

						user_images.set("user_images",parseFile);

						if(App.models.user.get("attributes.username")){
							user_images.set("username",App.models.user.get("attributes.username"));	
						}
						user_images.save(null,
								{
							success : function(ob) {
								App.vent.trigger("closePopups");
								App.controller.showPopup('uploadMoreDocumets');
								console.log("______________________________Parse loaded image details");
								console.log(JSON.stringify(ob));
								if(_this.$el.find('#offLineDocumentCheck').is(':checked')){
									console.log("_________________________________________here________________________")
									console.log(ob.attributes.user_images.url());

									var imageUrl = ob.attributes.user_images.url();
									_this.saveOffLinePic(documentName, imageUrl);
								}
							},
							error : function(e) {
								App.vent.trigger("closePopups");
								console.log("Oh crap",e);
							}
								});
					},
					function(error) {
						App.vent.trigger("closePopups");
						console.log("Error");
						console.log(error);
						var options = {
								"title":App.settings.messageTitle.failureTitle,
								"message":error.message
						}
						App.controller.showPopup('alert',options);
					});
					//Upload new image End
				}
			}else{
				App.vent.trigger("closePopups");
				var options = {
						"title":App.settings.messageTitle.failureTitle,
						"message":App.settings.messageSubject.connectionLoss
				}
				App.controller.showPopup('alert',options);
			}
		},
		saveOffLinePic:function(imageName, imageUrl){


			//Main Code Start
			var fileSystem_this=null;
			window.requestFileSystem(LocalFileSystem.PERSISTENT, 0, gotFS, fail);
			function gotFS(fileSystem) {
				fileSystem_this = fileSystem;
				var entry=fileSystem.root; 
				entry.getDirectory("zingLifeDocs", {create: true, exclusive: false}, onGetDirectorySuccess, fail); 
			}
			function onGetDirectorySuccess(dir){
				var url = imageUrl;
				url = encodeURI(url);
				var fileTransfer = new FileTransfer();
				if((navigator.userAgent.toLocaleLowerCase().indexOf('iphone') != -1) || (navigator.userAgent.toLocaleLowerCase().indexOf('ipod') != -1) || (navigator.userAgent.toLocaleLowerCase().indexOf('ipad') != -1)){
					var imagePath = dir.nativeURL +imageName+".png"; // full file path
					fileTransfer.download(url,imagePath, function (entry) {
						console.log(entry.fullPath); // entry is fileEntry object
					}, function (error) {
						console.log("In Some error");
					});
				}else{
					var imagePath = dir.nativeURL +"/"+imageName+".jpg"; // full file path
					fileTransfer.download(url,imagePath, function (entry) {
						console.log(entry.fullPath); // entry is fileEntry object
					}, function (error) {
						console.log("In Some error");
					});
				}		
			}
			function fail(error) {
				console.log(error.code);
				var options = {
						"title":App.settings.messageTitle.failureTitle,
						"message":"Got File system Error, Please try again."
				}
				App.controller.showPopup('alert',options);
			}
			//Main Code End

		},
		cancelImageUpload : function(){
			if(this.options.imageUrl == 'editDocs'){
				var options = {
						"title" : 'Are you sure to discard changes.',
						"callBack":function(){
							App.routers.mainRouter.navigate('#imageZoom', {
								trigger : true
							});
						}
				}
				App.controller.showPopup('confirm', options);
			}else{
				var options = {
						"title" : 'Are you sure to discard this image.',
						"callBack":function(){
							App.routers.mainRouter.navigate('#userHome', {
								trigger : true
							});
						}
				}
				App.controller.showPopup('confirm', options);
			}
		}
	});
})();
