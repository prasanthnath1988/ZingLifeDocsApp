;
(function() {
	app.views.imageDetails = Backbone.Marionette.ItemView.extend({
		template : '#imageDetailsView',
		events : {
			"click .upload_document_h" : "uploadImages",
			"click .cancel_image_upload_h" : "cancelImageUpload",
			"click .upload_more_icon_h" : "showUploadPopup",
			"click .delete_h" : "deleteImage",
			'focusout #copyrightText':'handlingKeyBoard',
			'focusout #documentName':'handlingKeyBoard',
			'focusout #documentsTags':'handlingKeyBoard',
			'focusout #userConfirmPassword':'handlingKeyBoard'
		},
		initialize : function() {

			_.bindAll(this, 'uploadImages', 'saveOffLinePic');
			App.sessionVars.imageDetailsTags = this.options.imageUrl;
		},
		handlingKeyBoard:function(e){
			if (App.settings.isDeviceReady && cordova && cordova.plugins && cordova.plugins.Keyboard) {
				cordova.plugins.Keyboard.close();
			}
		},
		onRender : function() {
			function convertImgToBase64(url, callback, outputFormat) {
				var canvas = document.createElement('CANVAS'), ctx = canvas.getContext('2d'), img = new Image;
				img.crossOrigin = 'Anonymous';
				img.onload = function() {
					var dataURL;
					canvas.height = img.height;
					canvas.width = img.width;
					ctx.drawImage(img, 0, 0);
					dataURL = canvas.toDataURL(outputFormat || 'image/png');
					alert(dataURL);
					dataURL = dataURL.replace(/^data:image\/(png|jpg);base64,/, "");
					callback.call(this, dataURL);
					canvas = null;
				};
				img.src = url;
			}

			if (App.models.user && App.models.user.get('attributes.signature')) {
				console.log('in if signature'+App.models.user.get('attributes.signature'));
				setTimeout(function(){
				console.log("generating signature");
				convertImgToBase64(App.models.user.get('attributes.signature'), function(base64Img) {
					App.sessionVars.signatureData = base64Img;
					console.log(App.sessionVars.signatureData);
					if(App.sessionVars.signatureData.indexOf('data:image')<0){
						App.sessionVars.imageType = 'png';
						App.sessionVars.signatureData = 'data:image/png;base64,'+App.sessionVars.signatureData;
						console.log('sessionVars.signatureData --------   '+App.sessionVars.signatureData);
					}
					else
					{
						App.sessionVars.imageType = 'jpeg';
					}
				}, 'image/jpg');
				},10000)
				


			}

			this.$el.find('.imagesIds').eq(0).attr('src', "");
			if (this.options.imageUrl == 'editDocs') {
				this.$el.find('.imagesIds').eq(0).attr('src', App.sessionVars.imageZoomOptions.imageUrl);
				this.$el.find("#documentName").val(App.sessionVars.imageZoomOptions.imageName);
				this.$el.find("#documentsTags").val(App.sessionVars.imageZoomOptions.imageTags);
				this.$el.find('.upload_document_h').text('Update');
				this.$el.find('.save_image_offline_h').css('display', 'none');
				this.$el.find('.signature_h').css('display', 'none');
				this.$el.find('.upload_more_icon_h').hide();
				this.$el.find('.delete_h').hide();
				this.$el.find('#copyrightDiv').hide();

			} else {
				if (this.options.imageUrl && this.options.imageUrl != "editDocs") {
					this.$el.find('.doc_img').children("img").attr('src', this.options.imageUrl);

					//pdf notification
					if (this.options.imageUrl.indexOf('zinghrpdf') > -1) {
						this.$el.find('.upload_document_h').text('Upload');
						this.$el.find('.upload_more_icon_h').hide();
						this.$el.find('.doc_img').append('<img src="imgs/pdf_default.png" class="imagesIds">');
						this.$el.find('.imagesIds').eq(0).hide();
						this.$el.find('.delete_h').hide();
						this.$el.find('#copyrightDiv').hide();
						this.$el.find('.signature_h').css('display', 'none');
						this.$el.find('.save_image_offline_h').css('display', 'none');
					} else {
						this.$el.find('.upload_document_h').text('Generate');
					}

					// hide for ios
					// if ((navigator.userAgent.toLocaleLowerCase().indexOf('iphone') != -1) || (navigator.userAgent.toLocaleLowerCase().indexOf('ipod') != -1) || (navigator.userAgent.toLocaleLowerCase().indexOf('ipad') != -1))
					// {
					// this.$el.find('.save_image_offline_h').css('display', 'none');
					// }
					// else
					// {
					//     this.$el.find('.save_image_offline_h').css('display', '');
					// }

					this.$el.find('.save_image_offline_h').css('display', '');
					this.$el.find('.signature_h').css('display', '');

					function convertImgToBase64(url, callback, outputFormat) {
						var canvas = document.createElement('CANVAS'), ctx = canvas.getContext('2d'), img = new Image;
						img.crossOrigin = 'Anonymous';
						img.onload = function() {
							var dataURL;
							canvas.height = img.height;
							canvas.width = img.width;
							ctx.drawImage(img, 0, 0);
							dataURL = canvas.toDataURL(outputFormat || 'image/png');
							dataURL = dataURL.replace(/^data:image\/(png|jpg);base64,/, "");
							callback.call(this, dataURL);
							canvas = null;
						};
						img.src = url;
					}

					convertImgToBase64(this.options.imageUrl, function(base64Img) {
						// Base64DataURL
						//console.log(base64Img);
						App.sessionVars.imageData = base64Img;
					});

				} else {
					this.$el.find('.imagesIds').eq(0).attr('src', "data:image/jpeg;base64," + App.sessionVars.imageData);
					//this.$el.find('.upload_document_h').text('Upload');
					this.$el.find('.upload_document_h').text('Generate');

					// hide for ios
					// if ((navigator.userAgent.toLocaleLowerCase().indexOf('iphone') != -1) || (navigator.userAgent.toLocaleLowerCase().indexOf('ipod') != -1) || (navigator.userAgent.toLocaleLowerCase().indexOf('ipad') != -1))
					// {
					// this.$el.find('.save_image_offline_h').css('display', 'none');
					// }
					// else
					// {
					//     this.$el.find('.save_image_offline_h').css('display', '');
					// }

					this.$el.find('.save_image_offline_h').css('display', '');
					this.$el.find('.signature_h').css('display', '');

					if (!App.sessionVars.mainTagsFalse) {
						this.$el.find("#documentsTags").val(App.sessionVars.mainTags);
						this.$el.find("#documentName").val("");
					} else {
						this.$el.find("#documentsTags").val("");
						this.$el.find("#documentName").val("");
					}
				}
			}
			//select tags from selectbox functionality #ZHRLD-2
			this.$el.find('#selectTags').change(function() {
				if ($('#documentsTags').val() == '')
					$('#documentsTags').val(this.value.toLowerCase());
				else {
					var duplicateTagFlag = true;
					var value = this.value.toLowerCase();
					_($('#documentsTags').val().split(' ')).find(function(tags) {
						if (tags == value) {
							duplicateTagFlag = false;
						}
					});
					if (duplicateTagFlag) {
						console.log($('#documentsTags').val() + ',' + this.value);
						if ((this[0].selectedIndex == 0) || ($('#documentsTags').val().indexOf(this.value) > -1) || (this.value == "0"))
							$(this)[0].selectedIndex = 0;
						else
							$('#documentsTags').val($('#documentsTags').val() + ' ' + this.value.toLowerCase());
					}

				}
				$(this)[0].selectedIndex = 0;
			});

			if (!(App.models.user && App.models.user.get('attributes.signature'))) {
				this.$el.find('.signature_h').hide();
			}

		},
		onClose : function() {
			console.log("VIEW CLOSE");
		},
		uploadImages : function(imagesData) {
			this.handlingKeyBoard();
			var _this = this;
			//var documentName = this.$el.find("#documentName").val().toLowerCase();
			var documentName = this.$el.find("#documentName").val();
			var documentTags = this.$el.find("#documentsTags").val().toLowerCase();
			if (!documentName) {
				var options = {
					"title" : "Name Missing!!",
					"message" : "Please provide valid name to document."
				};
				App.controller.showPopup('alert', options);
				return false;
			}
			if (!documentTags) {
				var options = {
					"title" : "Tags Missing!!",
					"message" : "Please provide valid tags to document."
				};
				App.controller.showPopup('alert', options);
				return false;
			}
			App.controller.showPopup('loader');
			var checkNetworkConnection = PG_checkNetwork();
			if (checkNetworkConnection.result) {
				if (this.options.imageUrl == 'editDocs') {
					//Image Edit Section Start
					// Create the object.
					var userUploadedImage = Parse.Object.extend("user_uploaded_images");
					var editImageData = new userUploadedImage();
					editImageData.id = App.sessionVars.imageZoomOptions.imageObjectId;
					editImageData.set("DocumentsTags", documentTags);
					editImageData.set("DocumentName", documentName);
					editImageData.set("objectId", App.sessionVars.imageZoomOptions.objectId);
					editImageData.save(null, {
						success : function(gameScore) {
							App.vent.trigger("closePopups");
							App.sessionVars.imageZoomOptions.imageName = documentName;
							App.sessionVars.imageZoomOptions.imageTags = documentTags;
							var options = {
								"title" : "Sucess!",
								"message" : "Document updated successfully.",
								"callBack" : function() {
									App.routers.mainRouter.navigate('#imageZoom', {
										trigger : true
									});
								}
							};
							App.controller.showPopup('alert', options);
						}
					});
					//Image Edit Section End
				}
				//Notification pdf
				else if (this.$el.find('.upload_document_h').text() == "Upload") {

					var UserImages = Parse.Object.extend("user_uploaded_images");
					var user_images = new UserImages();
					if (documentName) {
						user_images.set("DocumentName", documentName);
					}
					if (documentTags) {
						user_images.set("DocumentsTags", documentTags);
					}
					if (App.models.user.get("attributes.username")) {
						user_images.set("username", App.models.user.get("attributes.username"));
					}
					user_images.set("NotificationPdf", 'True');
					user_images.set("NotificationUrl", $(".imagesIds").eq(0).attr('src'));
					user_images.set("DefaultImage", 'imgs/pdf_default.png');
					user_images.save(null, {
						success : function(ob) {
							App.vent.trigger("closePopups");
							App.controller.showPopup('uploadMoreDocumets');
							console.log("______________________________Parse loaded image details");
							console.log(JSON.stringify(ob));
							if (_this.$el.find('#offLineDocumentCheck').is(':checked')) {
								console.log("_________________________________________here________________________");
								console.log(ob.attributes.user_images.url());
								var imageUrl = ob.attributes.user_images.url();
								_this.saveOffLinePic(documentName, imageUrl);
							}
							$('.upload_document_h').text('Generate');
							$('.upload_more_icon_h').show();
						},
						error : function(e) {
							App.vent.trigger("closePopups");
							console.log("Oh crap", e);
						}
					});
				} else {

					var copyrightText;
					if ($('#copyrightText').val().length > 50) {
						var options = {
							"title" : "Limit Exceeded!!",
							"message" : "Copyright text should not be greater than 50 alphabets."
						};
						App.controller.showPopup('alert', options);
						return false;
					} else {
						copyrightText = $('#copyrightText').val();
					}

					// get date
					var today = new Date();
					var dd = today.getDate();
					var mm = today.getMonth() + 1;
					//January is 0!

					var yyyy = today.getFullYear();
					if (dd < 10) {
						dd = '0' + dd;
					}
					if (mm < 10) {
						mm = '0' + mm;
					}
					var today = dd + '/' + mm + '/' + yyyy;

					//uploading images as pdf format
					doc = new jsPDF();
					var initialx = 10;
					var initialy = 25;
					var end = 190;

					if (App.models.user && App.models.user.get('attributes.signature') && $('#signatureCheck').is(':checked')) {
						//var signatureData = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAIAAAACACAYAAADDPmHLAAAABGdBTUEAAK/INwWK6QAAABl0RVh0U29mdHdhcmUAQWRvYmUgSW1hZ2VSZWFkeXHJZTwAAEx8SURBVHjaYvz//z/DKBi5ACCAmEaDYGQDgAAaTQAjHAAE0GgCGOEAIIBGE8AIBwABNJoARjgACCAWZA4jI+Ow82BgYKA4ExNTzt+/fwOAWBEo9Bfozzt//vw5+v37990PHjw4ev/+/Y8g8ZEW+aAeIEAAMSJ3A4dbAggKCjIDUiuAka0oLi7BIC0tzfDv3z+GJ08eM7x//57h9+9f/xkZmZ/+///vxK9fv3a+fft239mzZx8D9fwBhc9ISAAAATRsEwAw8mWA1EFmZmalgIAgBldXVwYODi6wpz9//sRw69YthqtXrwDpGwwvX75kAJYGDExMjF+A0meBiWHv169fd1wFgnfv3n0frokBFBYAATScE8AEYJGfHxUVzRASEgrM7X8YYN779+8/kM3E8PfvP4avX78wPHz4kOHatSsMN25cZ3j06CHDt29fQWp+A5XeBJpx6MePHzuBVcWxO3fuvB9OVQUo7gECaFgmgICAAEFgUX9WWlpGsbW1nYGbm4uBlZUZxX+gyP/z5y84MQB9Dhb79u07w6tXLxlu3rwBTAzXGO7du8vw4cMHYOL5/R/YjngBNPMYMDHsAlUVp0+ffjjUqwpQ3AMEEMtwLNqAEW0IavDp6ekx8PDwgPI8RuJmZmYCY1iJAEoMnJwcDLKycgzy8goM9vZODB8/fmC4e/cOKDEw3r59SxKYOIKBCSGYm5v7i4yMzFlg22IfsKrYdeLEiYvfvn37MRQTA0AADdcSoA/ol8L8/CIGGxsboL/+Aet34nu8oOoClCD+/PkH7in/+/cXWFV8BVYPjxju3LkFLCGuAxuST6BVxV9glcB4E5gYDgHbEduA1cmJGzduvBsKVQUo7gECaNglgNDQUFZgI+64qKiocVNTK4OQkBADOzv5BR0oIfz+/RdaXTDAq4q3b98w3L59E9iIvMnw4MFdhk+fPgHV/AYGJuMLYOlzFNSQfPPmzd7jx4+Dqorfg7F0AMU9QAANuyoAmBM1gbnSQFVVDRz5TBQOdbGwMIMxpKr4By4dmJg4gT0KaWC3UobBysoWHPkPHtwDlQ6Md+/elnz9+lUIKytLCDe33FdZWdnzwDbEXqCaHaCqAliSDKqqAiCAhl0CAJZifkCSWVdXF9jwYwGm8n9UMxtUjbCzswExJPeASgY2NkYgFgInNn19I3BV8ezZY3ADElhCcL948cwGKGbDzs5e5evrewuYQI8Aq4odwOrk2OXLl99BG5IDBgACaNhVAcD6fycfH59bQ0MTg4yMLDARMNGp5PnL8PPnb4Zfv0Dth/8MoGD9/v0bw/v374CJ4Q6wMXkb2IZ4ACwtPoJKKVCgvwbSh4Glw57Xr1/vO3LkyAN6VxWguAcIoGGVAIKDg+WBAXrZwMCAt7y8Clx0s7HRv5ADVRU/fkASAwiDupnANgG4qgAlgvv37wDxPYZ3794AE81PkJavQD3ngG4/8Pnz550XgeD58+ffwN0XGicAgAAablWAC9BTvAYGhsBimh3owYFpiIOqCi4udjAGBTIoEfz4AUqMrMCqQphBW1sfXDq8evUCmBDughqR3M+fP7UFdiVtOTg4qlxcnO8CE8N+YGNz59OnT4+ePXuWZlUFQAANqwQADGx3UL9fXV0THPCwfv4At0mAiZEVjKGNVGDk/wI2IlkYeHi4GRQUlIGJ4we4qnj48B4IMz958kjt8+dPasBEnC4oKPBSQ0PjGLCk2P/u3bud+/bto2pVARBAw6YKABb/osDu1yUlJWWJhoZmcIAPRPFPCgCNRv748QtcXYBoUDcTVFWAhqdBE1aPHz8AJwpQVfHrF6iqYPwGTECwqmLH+fPnLz579ozsqgIU9wABNGxKAGDiNQfWoxJ6evrAopdzwIp/UgCohOLm5gBjUGSAGpHfv/9k4ORkY+DnF2DQ0NBmAFYLDG/evAS2He6D2g9cr149s/n+/YcN0I8Vbm5ud4EJ4iBQzZ6HDx8eAlYVb0mtKgACaDhVAb6geh9YXIInfZiYmIZaAgZWC2xgLCjIAG43gBIDFxcbAy8vD4OcnBK4qvj06T2wdHgILB0esgC7m+rAqkId6O80QUHB19ra2kdAbQdgr2Lnnj17iKoqAAJoWFQBERER3MAi8qSkpJR2Y2MLMMB4gcU/87BJ2aCqApQYvn2D4L9/QQ3L3+Cq4sWLpwxPnz4CVxcfP74HVxXAKP0KLBkuAfH+jx8/bt+7d++5r1+/Ykxrg+IeIICGRQIICwuzANadx1xcXBnT0zPBVSJs9G64AVB8gdoLX79CEgOopPj9G9SWAI05vAWWDg/ACeL165dgMaD6f79//zl19+7dugMHDhwHdTlhCQFkFkAADYsqAFj3+wKLfEYdHV1gvcoI5DMyDFcAyqScnOxgDAKgBADK3F+/soN7FVJSsuCq4vPnjwzPnz8BthvuMT18eNdCQ0N9zffv3wtOnjy5CajtPazhCBBAwyUB2IP616qqquDikoVl5Kx1BfV02Nh4ge0GXrDfQbPSX758ByYGTgYRETEGTU09hmvXLjIcO7afT0tLq/nKlSuPgdXBKaDWTyD9AAE05EMKWP+rATOFiZKSItDDogwjYCkf3l4FLy8Xg6SkMIOysiSDvLwIeDBKW9sImBB0gaUGp7SRkVESUKksLPMDBBDTMMj97kDMrqdngLHqZyQDUDhwcXEwyMiIAEtERmCC0ACWFOwMAgICekBp0Opo0EoZBoAAGg4JwIOPj59BXV0dvLIH2BQYjX0kABsQ4+bmBSYEFlD3mAsoLAbEnCB5gAAa0gkAWPxLAylzWVlZ+JLv0RIAswsJaih++/YFHD5/IMucQC1IcDcJIICGdAIAdmOs//79Kwxq/XNwsI/mfiwA1EMAjRu8evUcPEbw9u1bUA/gFwN0yRpAALEM8QTgx8nJxaClpQ2v90aPO0BuBzAAu4M/wFPOoLGBv3///L13795DaDcQtDKJASCAhmwCiImJ4QN6zEJSUpJBQUEBvCAD1AoePfAC0QgElfagFWhfv34G5vyXoCXuH169evUIKP0CiEGTSAwAATRkE8Dv37/1gXWaMqjxx83NDUrd0DX+owBctzOBViT9AK9OevHiCXjn07Nnz19BI/81tBpgAAigoVwFBINataC1/yDPghZnD7XcDyqiGaE0yOnUTb+MDF++gBLAb3ACAGaQfw9BW6AYGJ4C8UfYgAlAAA3JBBAbGwta+m0jKirKoKSkDF6cCYn7wZcAYBEMnqEEEoxMUAEGSKyDiunvP/8xsLMxMbAwMzL8+v2fgRodGVCVCCr+QdPJoAYgkP765MmT+0Cp50D8BaYOIIBokwDijzKCuxr/fokw/P2tzPD/rxzDvz/sDD8/PWZ4fe0iw9HWNwwULHECdmW0gMW/ASjyQatxQUu1B0MEgzAzEyiSoVkbSPwHdcNAkzXAiH337S/Dw7fABtn7XwyP3/1ieABkPwLSD9/+YpARZGWo8ZFiMFfiZfgPLAr+U1SyMIJnC0EZA1T3g2YNgcU/aBEqKPJfwhqAIAAQQCwURDIzw8+PXAxMrJJAXyoDRRTAI0yMzMAW2Q9pBiZmWQZGFlEGFjY2YFKHdDfZhRgYeKTeMkga72N4dLCX4dTEy0BRknffAot6T1A/1sDAADz58+vXf7oU17B4ZQFFMjNUAJiL/wIj9zsokn/9Y3j5+TfDI2CEPgZG8kNg5D4CRvKT97/BEf784y+wmn+/QEt//kHKfJCBQHOuAZl3nv9g2FeuySAtwMrw++9/ihIAaD4A1CYCTQiB2kdPQFuZILkfPhEEAgABRDgBxB1hBOZeRYZ/v/WAtDIweSsyMLHIMfz9Kc3AxiMHtI4PiFkhyZ0BUpmBMMgDjP8hww1MzLBRCVBaEGbgEgtlUAvwBNKtDAeq50IdRUo2dhMUFIRO/vynSt3PiFQng9oUzEACvKYEFNFAO34CI+wbCAOL6ycfILkWErE/wexnwAh/AIzk91//gHP8f1gkww3/D0o5QMzMwMjGzMDGzcbAx83OICrAyQDsuzBcufeO4d6LnwxXn31jkBPgZQClaUi3lnS/QVYl/wIvPH3+/DGwG/jrx33QMmRI/f8ZWS1AAMqtZqVhIAwOWZts18Sm0EKlMS/hSewT+Ap5jd68+gS+kE9QEI/Fi9RWxUBLivSHRDvfZgs9VT0sH4EcsjPDzH6b3eMCyB5uSPqQBF5R8tqSeUgw7c2hBQm3hn9if1NqGTpAHAZoR5oT9e1k8mKNl9ccH++fKL9ViIvBHa5v24yEe2dNv4ogyzI5+n2Zpil6vXNrdX9d/cunuvS1ziF27blMriSLifqGtVhVtT2T2Olia6tY9cQ9LymCck+wbKztM9tDTXJDwdMKJtZoEYPWqUZkAnRiEn7GKiPy0Q2VvVjyNPnCePyM7XyJ0WOFftXEjwqQJAmMMf/qbgRn4kPSS7Z9cxTFgiJ4y7kGmLkOYHX4/k4ASs5mJ2EgisIHyxTGgTZI/Vu5g9fAB+ERiO/Bg7Bz58a1W1y4cKVBQrBoMJGARCryc+60TQiYECeZNLNp2vvdc+9M53b+doD6ncuw3qTSr2ylorykfEcmaKWU3WTI6TyMdhPIAjuPU0/h2GRw7mVwVgTKGggOAd+Nb/v6pdF6KOHmPkDn+YlGjBxcXDYwaHfRub1OlierPd5doyo8+fNHtn0jwkhVsjmrTvNxNoXMFqWhmn04+UXvg5Cp3MFoTuAx6D7H7+M5nWGJxXaolkeTiCDFJoqTNkI0Wlkn903OdoF7QtBlT2ArHOkDBLRJYACaCQVa3OfVqDiDyOE0j30HhsL8+f7Ey5tBVHUwnU0Z3RaoVKr/TlVSPSTYwrBnVwEhW6L+nZrBtQCMml8OwUAQxkdL1Vrqb4oH8UKCU7gAx5A4jnO4jXhtRIL0gYoIDaK+2baRCGGTeWjSppl+v/lmdtPPADzuU4g/xtNkZgU1m3VqNQpIzAS1GtmIOozflkQlEyJnwoiT+rZq+Aj9AfbwfpFmfofc5QLNyMhQezQBAPNoOj3/IHyYk5J63a66NrSA9CREhjDcDvzIqi8Qe3u4KmHZnrknr/YvkXds1aj44Iq2BCAUdvHBeDIUOQGRDZFSVs2gW4CcYa9acZhUQRWXhEZlgM6RB+wSATYorb/MgRe/6oTd9wE1uD5iEoMUG2QslUMI8gIP7eUGoBlaXU3wbONCZJWt/2f/pH4752Nf112zG9wdx+HpnyE4vt//FECYCSBqbww48oEtayExQQYbc02GSCM2BkspSASyoXkKpXUOtPzjT6AngfjzL4hnP/+E0D+BnhcCJhIToDnVTgwMV14JMHz/JM7w4SWwbSKoosYgbenE8PQ46HyeR7hKgcDoVDHmv29MpaQVGUQkZBkev/7GcBsYgo/e/QZH6pP3kGIbUnT/BiaEv8CAQCqqkSt70JYxYAnCxMnCwMHPwiAAzMWgUgxUZAtBIxmc4IExKszNDIxcRiAN9AMwknmBCZ2HFRLJzGiBAfLnN6B/3wCT8Vcg/f03kP8bwv4NSmv/IU4B1Z4gt4ASAbAwZeDh4QK3zUCJ9c/fv+BSDbSHAFTFcXD8J6oaAHkLNvnz5csn8HLyd+/ef3j37t1jaAMQI3MBBGDU6nUIBqPoRUiDNn4n6SLpYLAZDMLuGTyFV/EsFqNnsAmDpSSiJaSVos79vitak61N0/T2nnvOPSf50gMwWRpq5yNDmpUyDfodmg4KNLLTP8igXgRYBbQAfos0mZ7Sb645m5DmA6Z9cyIaO0SjNtHaxQAcua5cgapODwOwkD11Ry05Cj0DKaOBXOTAOdmreDtsFT17HXZpPnNp5/rkna/wp/jYPSnVYt7UPoZUg5KmMFmx2DKobn5Z3ASijRJYDKlmgCuGsDivVS2TSiCayQzq/qoBZhD5PngIuEmQ4+80x3Kt0gRKi6VE7lGtaqkHPproBy8MF9KNAjP6JJ+/rCwfLWcV0OYvZPfvSk85eke/b7wFYNQKUhCGgeBqbVMQcrCCRaiCehJEEHyCF//Ql/oL715FBMGDYJW2OJMmtfVkIeTSpNnM7uxs0rYD5NlOeuGaK58vJrJfVuAj0OR4qXoa6QAurB7sOKPsTnGpntRVUn3KxZ5OcziBTZA+KBgDpZD7wInj7Up0kkq8ScXvx6gyElF6isk0diuQstO9BTPJvSeieyTvO8Qs6ltjLUSXgh7RhqYrkAfIwQTZRDGAHiL5RqTqfoOqVQVy+PP/KNdPUNlQmZkemsrYTvBzq3/ZHKhNkEv5Or71gfo99wF3buXmiaD8OYAC9PYoREe+GcOLHEb1PwxArZNlL8Mc1+uZ+b+w9N86/Ws+HwHEgtbBtgO5hgPYyJOXEmIwl4AUZRtuQAKBBRbJ/yGplwlapP6Ddgr+/0ON7H/I7H8I/vefEOs4OdiADTlWYAn9FdiI0FUA4iy4RuReBqghB7RciOMXAy83J4OMpCi4wfWfQQTYmgZ1paCRzM0ExkLASBbkgDS6YMU1+iZhkPGgSAUV1+/+QEq2n9BIh5VeMCf8RxwjxIA83ghO4IxIieA/ooONkiiwJBBYxgElKJA9QkJ8YIHPwFT27stvBmURSNQAu3Dg3E9MCQAp/v+C5/5Bo39fvnz9AuwBwIZ/v2LTAxCAkLNnQRgGwvBJg9L6AQ4inbo6+v8nf4qTJeCmiEqaeG8uiWkqOISG0EC45z5De4UCDC0o4oPCVV35GHc6M7C3VHoR8giwHcO22XoUYprb8D5JTGSjJcVVhYCuPOwFKwV69cy5jGz4idi83dRcUVjauwt1uyUdD5131THZwmjUNB4DJKDeYMWDCBsDsE12polsg5LPQiRxmWeLHs1lVkwF/LRGX9hpXigRvArOseYcQLFAzMPQ9W7kCP5voZdPAP8pgNz+SRcTrXvfvobdv+a9faiunr/2fQRg5FpWEIaB4BrsLaKCFC+CZ///azwGvEqKiGDcpLNNah5F8FBSQhKG7mumIakdwPNaoKkkoa4QDbchfrTQRDX72tjctvOTxn44l46JDQuPACTdn2h7QETD6XZI25djR+e9on4TVYZISSkX9LzTyyITQAF0OvZJfXbJoBZryUnsdzK68zVWKqI4rHJYqvxbfhGxoSkLbbpf9Id6UycUC5Vzv+MoYpZ3cXYNxWUf0O2DA2Y/XW4hbJ7Z/0UA5VyhOIts/qANxpiZ/NlfxHoUgI8rWEEYhqGvehBR1osg6g/4I/7/yas3d9hBBd3E4haTroUYNwuF9vySl5cXWhMAXRBeDywkZEhxrCa4NyNAK3BbJfwE6LfKNNEwLomd5JHEGrxfAYeCsC48dt5jy4Buln1W2y5DaPlUPXB9Osy4XZq+TACOTNJIte6RqnMckAF84EyqVpOhbuCX9om+M5sGNhT4caSd9ITc58x4C9ZEt67F+RLSf0Qu1nMxdsR/+ccC+ceS/tl5ibpusvtX6uGPXR8B6LiCFQZhGBr1IKLgae7i//+XFxmyw2AXMdgsr6asLVXwYA9ifXkvLyGaBQCvQAuz5WiYLJ+aKjZwJWUxmyFCBGMjtQEMn9AriPNINA3X+TQ2o3fwGK5SsgkGwl7KbiZr/f4NGJiMuaVD8+B72zVLaIlWtdRGgUhxPs4BLbCa5MaQZYAn4FMq904K97T1sOYi2S8piJj8h+ftOox09/4CJezpxO8PoDIf3ivdBQAIBsKiD4Kvg/AnVM39myrCq9T9i4+fAISczQ5AMBCEVx04SIijxPu/lrur/yB2um2sajg40VRMO/abpg0oYO0oY/7kETfNC/WmoHkQIfAyqblncsXW3LLISPwgsBe7KYWVU/MMhQ6HTvZ/7NgY4trLCxqKQ4JLG7Y4HTvlRWk3faL/2IwLxX/dj4ivzfWkd9szqOJ0v7oWeDiDsn2KPOcHCL5rotrWIAGU64xKI2Mt3BILOX9ZgMS/chIJ8M8t/vj07/PIuksAoSaAvz/ugVqC//7+Z/4CrKQVgNlVDRipIjyICAZFuCQ/pIWNvgEHVCKAGozvvkFb1L+hDa6/SFXGX0R4sgBtB7UBYQEAXxyBVsT+/P4NLMvKwYNomGGrj7FEPnpDDVuxj03tP6TqArnxhp7bGbBUCyhVwH+kkgHJwaCSExQusBlGkJygAGQs4BOwxQqaOpbmZ2b48esvuArAPxYAKf5B/X7Q6B+w5/Dr9u3bd6HF/2d8bQeAAEJNAD8+3mPg/QdsdfzjevPhG4OtojBDtBamJlDX6fVnRDEN8sxftG4frGhCzn3gRhcTUjeIETMC/iPnTNDIFmg07Od3BmZWYJeRjQM+5oBRVP/HbRZysY01ov5jFvvo9Tx60Y9eavxDKu7/YWsDoCUkUNiB2iWM0Mbpf/hg0H9wAngL7ApK8zGCG3WgriC+sQBQwgDJgXYHg04aef369dvPnz8/xTX6hwwAAlBy7ToAgjCwoIlGE/7/95wcTGQ0hIBSXsGCUTcWWNrrXa+kJAHkBkZLMN10OB2wE+ZArl/24GWnFbtpupbOnBVqu4EOW5RExmsFne/ENzQaIQ4B4yxwhJerztfg35DH/gUfoC79liYGpQDbbhfL0PlOxeRuM1cAIdBr7+F02SFdK8jZEN099egFBP43nhpR/WutcKPpGtH/+vHmEkCohfiTQ18Y/v15BIrFH8AE8OIzpDvFAK2n7wK7hW8hpTG4joetiYAN9TLg6h79Q8ol0FICNKHGyIhaaqDnGEjx/4UBtD6GlZMbUvwzYe+SIedC9AjAVWxjK7IZGFCLbHQ3MeBIsOg9gn/I3T80daDhY+S4BIfDPwbw+AcPsJcDqhtefPwDH6OAde9giQAdg2YNQWpgkz/A1v8DpMkfvAMIAAGEmgAeH/wLTAAPQSH96wcwAXz6D06pIADshTF8/A6p9wktWUMJoH+IiIEFCrg7yIKaW/5haUn/BQYEqAHIyMQKLP454aty/v3HnsNQGno4cjYsgv8xYO/6YXTzGLDnZPTWPnpiYsBSBfyHlqIgjFz9MUHNAY8F8HCCA+bZB9hYwH94Avj37z8GhiyIAfbzvnxmePPmFegous8vXrx4CE0AXwmNHwAEYORqVgAEYbCWRNL7v6BBdKxDGaXYZiaaK1K8eZHJvp9Ny2qgPRS+G10XDYzdsnkT3kpFYvf74U1C6ny2SGCB3UGkpJHLZRke3MBq2o5VAjgAACaH+ZRhhYxzpQTMMDs1hRzBPQj9T15uwhNwb9ksBFmb0nnkIUtKWfuv7XHjMO0R87Gmj0SQkoJXmbjy1i+SQKX6MTTXxNbvr3EKIMwE8O/3Y1Dy/A3senz59pPhzXcWBmleBvD6ckYseZ8RZSHGf3ggMWI0hBhRAgIlB/7DHFOHlEJfwR4EFf+QhMSIc7QOdiIs1vocS3sBW1sAazcPNnDEgFry4BsF/M+AGPJGVvsL2huCLQNHrgL+Q7u8gvw8YIF3wDYAaHUSMLlDD6z+A2wCsWBNAKD1A6DuH2jf38OHDx9Bc/9HYtZaAgQQZgL48/0+aFnq/78MjO8/fWd484MbLMzK8B9lPBV59Q0iu2PJQeCAY0RtHf/H3lBDjhTwkWng4p8ZUvyjdf+Qx93/Y+uSYRsAYsQ+OfMfx8AOintwNDSxtVvQJ34Y/iMmn/CtSQQlYgF+6KQQsCH47us/BlEeRnAdDyoB2Nk5MHoCIDtA4/6gI2G+fPn67S4QEBr9QwYAARi5ghUAQRgqCnXt4P9/WV8QBEUXIZq5qWxsBh12ExGcb+/t4ezn0HSs5SQJBft5Jbc1EUEjd8jfz9XqNFKE7WEZoBzCPIBX0F00zMO7wH+BtDDNzpcIzZ15ZHcNVK03icc8xEgxtR5GL3qADiCJbRZn/+AMdPmu9kQ0F5GlsiNmjAtBDrqCOykBToC6L5O/+hva0xg5dACb+dO7f+lPArwCCEsCePcK6LsvoATwA9gOeAvtCnKwEmj0wQL1H2akMmCLFCyRgTym/gu6Jo6VnQscPKBNE8j1CraE9B/XBA2WyR1spREssSK7Bb1lD4v8f8jtCbTZvv/Qobd/SA0/YtZ1gswS4OcFj5D9+fGP4cO3P8C0AGnoQbqC6D2Bf+ASGDL695fh8ePHT6G5/x0DkYdHAgQQZgL4/OQzsOX1GDxVCUwAoAEfkNvZWBixVigY079YGkN/0fjILWf4WgIGpHUD0OIf5DsWYAKAnPuHNA2N3t9G7gpi6f6h9xAwEiGWBMXwD3vdjh75KAkXy1D2L+hIKEb7Aq26ga2rAK2o5uLmADW6GF59+g1POJCeAGoPAFQe//jxHVz8fweCu3fv3COl+AcBgADCTADXl/xh+P/nESjUf0K7gqCBC3AJ8B97zkfvIiFHFCTg/mP09/+h5+T/iMYgqNX7Bzz6Byz62SCr0ZmQI/sfjgkb9PF2BuxTsPBxCeQS6z/alDdS4oYnNjyRj9EA/A/t8v3D3lNBmauALp37Dx0L4AatDwRyXnz8DT3x9B/KugAYBhX/Hz68Y/j48R3DuzfP3379+u0pockfdAAQQNhXBf/98wQU6qAS4P23vwyff7IwcLMwoNy+8f8/9gYftghBKZaxNOSQExAjePTvB3galJOXB9wIZGaCyMAigoEB9/Qt1kYesjuREs8/9NE7fJM9/3As8MDR+v+NtA71P475hv9ojUCQGZygk0GBCeA1sEh/CV4XgD4YhOg5gLbDg3b+MAHL3VOXHz1nYBP4wCAbqMoobqMP3qnFyCwPDHBZYD3yk+HDlb7/V3tOMiCdEwgCAAGEIwH8BFcBv77/YPj2/TfDy28sDJoCjOBuHqyjB+vWYatP0eth5DFyhv+ogYje5/4Prf9BHFYOLsj6TiZGlEYi1u4enqlfbBNBDDgGgTBG/HCo+YdlEOs/WuSj28OA1ujEllhBA2R8fDxgwVef/oCn3UG5BtYVBO2IBuV+0NAvaJr4zevnDJ++//v/8r+BLoOyqz4DpyDr///skPoatLuJFRLF/3nVtBlYeKIY/ny5grw8HCCAcCWAe7A+5odP34ANQU4GJmHIAA5saPj/f9y5/h8jWsMIbRwefeAFOdeBLuH6Daz/mVhYgVUAJwNsdxa2yRxscwBYc/1/HKt6kBPoP+wR/g9tDoABV+RD2z9/sCwWxVZKYUwdI5VKgoL8YE9/+fGX4cP3vwz8HAzwngAzMzP8KHzQtXYfP75lePGegfETAy8Hu5AIA7uoIgObkAwDOzcnAxsPN3js5tGpM8BeFasEAzO7FTABvIDOEIKtBAhAt9msIAzEQHhWKm0XQSziQT2Lz+D7v4UHL70UfyiiiOvauAlCl256zy2zw+RLVhcAswBQGAWpaO8vuQbGn8O7r7Lu1JI1pXAn4eYmndv9h5c/DrkN9s8imPQD/FhTtRl+CIM0wqeKeGTpo7lE/PLlFJx6cQAp2h5Co1i8Jvp8VFVz8XgWwO3psSiz4AReslHX5VLEUKhpagnLl+kK28MOs+UG5XqPzFqBSqEE52Md8pSDeZxael/5uK6IW/0TgK8r2GEQhKHURZNt8f9/bddlBz14cTGS6dau2DLEkh2IIeGg0pbXxyuUD4mauju7og/wdZ44AmhG2dSQrNv8QEgZLYH09Un7utE4Hgr0aogwfpas49xq/QYYAiei5r3My/APLgd0JmenfIMq4w+OHD8l78TCmr9qQ7TvcTQ2/IOZYlTYIgB/9/L6uJExWAX0E4aI/FvuMux5/ffvyg3Yssdf3SlQ5s2Fp44NcmGDDFVIj05EGM9br9rArBr7KwBf17KDMAgEx0SkDy/+/0/5CV7VxFo1sdiyyBKo0Gw9wmkv+5jZGZArwOfxBI1+olQHw3/kxZUCS7nyzA4stA+UJWDJDCFlFf1pGQWvHxggC61b1G3Dm1HUel2qlZZDJnoV8mGOFlM2CQyf2Pc3Ja3sljRwdmfpJz4FVjgDJ+8OIMDVhKCC99KXcNO/g0KYaBdgX4KC/O4fe/7v3RX9VKEbtlB7X+xVNQfOFWAaRrzOF38g627HU9QHFPawrwDCngAuz/jFIG3/ENiC0PkJbAg++wiZw+ZgQeq+ASl2NkaGV8AuyP4TOxhuPb3M8OPXd3DXBNskzH8sizUxVuSA7uX78QV8wAIbOw9kcyczZnEJa4YyM7IwyAopMRgpWDPIiCoBiz528DLz//+wj+//w8HG1tpn+I9ZpcHEkHc+4ZrG/o9rYQieHhMsAXCAuoKgBPD+C3ha+B+0yAMlAPCwPLBhB5ry//n9K8Ozb7wMf5mBCQZY3jOxcUIHhyCHVHx9847h52dgeP54/vn/57tYZwgBAogF5+De31+PGJi4GH58/QZOhV9/swCrAIQjOYENzcNXDzG0rCoGFkXXIMUMAwXHDjIiLQKEtbQIb4UE7yfgFZBhiLDIZLDT8mTgExVj4BMQRIwV4Aho9BFIrLN6SHr+IO2G+ofWvfv3H/vCUAb0DTK41gogJ/B/kB1ToK7gO2BZDh4MgqY00FgAaFs7KIJfvngK2bn0jRXYYGYHBhsLmP4PKveZWYFqmBk+PX8J5P8BthCBxf//v7DDoX4ihyBAAOE+H+D/nyeg8vU7sAr4BrQJNCsozw2JH1Zga/DkrTMM5XPiGL5+fgMsCkB7nxmhZ0/SEYDnV4FN2k8vGebsbwNWGZwMRr+twY7k4eOHJAI84/8M/7GvG4TtJfiDtPT9H1qvA1d18g9tMOw/ljYH+rIx5DYHKHFxgPYm8kIm4d58/g2sFf8B5f9Dp4RZwEu/3719yfD5DwvD659sDCzARiITKyfU4D/gxvPfX/8ZPoOLf8b//z9cfQJdIPoefTgPIAAp146CMBBEX4qEkPhJaWdjEARv6SUsvUOuYSHYBQUrtciSj9loWHedTRRCZG2cEwzs2zfz5mcGwLM+aK8lxeS80ErARRi0RxX0yPg6WqHMCVDDMazFANP5DCPHJ0flF9UDhjHsP0z/gko8cIpjyH0GRUCNthuEkyVwPcP1KInUyZJhTNx6l6i7lC66i60d/60+Sylz0asf942NIvV7yygI2q5gzgWKu4Rny0YKavmXJAzlLcWFO6iVDZ9YsKF/YoxmL5OkImcZKkbvLdIKbHdE7zjUx14CCHcC+PP9AdAloI1pbO8/fmN4+VUQsp8P2OA7d/cCw6X7pxmAlS4DgyoHQ5pfPEODbgqwwGBCSQC0BKD+LegYl96rKxg6fk5gYLjyl+HhmzsMd19eY9Bjt2D49vUzAw8/MHH+RUQCKMJBbZnffxERDsvl6ItYsC5qxbZ/AFcXF6mo/4c2T/HvP4FRU1BXUFgQMhbw8x94hTCPABN4IA40NgNe+gV09JPPQDFWYCsZWPwzsrIBC+3f4DMNgFyGzy9eANvxQP6nm6////sJWySCMUMIEEC4E8DPDw8ZeP/9AHZu2b58/grvCoISwKsPLxl+g64xA0aAiKIkQ65GCAMPCwfDD2Dfg4mRfscPswF9mqLmw7BIZRPDs5uXQAfjMrz6/AJYZDIyfPjyi+E30K0/fkHGLjBG55AWsTIyoEY4LNdjNFz/YS52YcAy///vH/YBJYyZR2xrC6GrpYSEBcAO+gFMAB+/ARt2Aszg3A+a9n0D2vnzG9gA/8bEwMINjHxQGwA0Yf4PNFDEDaoFgPX/K4ibPt14hjRDiLE/ACAAL1eSg0AIBJslwpiMiX7D/3/DL3hyLsYYFVBntZpJlJkoR+FOCF2hu6qX3wBwhwtttogi1Orp77EkjJfRKecfQAEtMNKS6wIe+b/j2hrsQfS0NiVVYpzVw/fcg+3CLVLJrKX7GFomHc3iCzt500CK1dmT6qac/JxLP8+zjROhaK5TJC5gCRagraE2NFEMknIRAcBDH27XM4yvyCMGKBR//3ZMGuEAoeEW3IP8EbFZH2o67bL9AS8BhDsBXJv7g0HaEdh4YFYFjQW8+AQZ6eKEbeSAhhSoO/brHzC3AVMfvRMAsEAE5u5fkGrnP+TEhd/A3A/K7exImz8Z0VYOw5Z4wQ7vguV8RqQY/4c0O/WPATPBYN1k8h/HYlFc8wdYqgLG/7CeACe4Ifjh0xtgV/s3uFsHugjryeMH4BHBxx+BIQ9MFIjW/29wdcDMzsbw/vFDhj8/fjAwfn3w7v/PN3j3BwAEEAv+8P3zALSE9zuwK/ji0x9gEc+CsTAEVC/9/v8b2BgBJoL/9E0A//4zg+39j22eGmlwiREtEcDLL9jGDOjACSO24h/apMF2DxXeBaVYloqjRz7WJeywJeJcHMCuIDAB/H8N7oaDNoeC6v/Xr58zfAU2ZIA9e2BvD1j8M7OC2wD/gZmQmZUP7K5Pz16ADfn/8dozQvsDAAIQc3YrAIIwFN66KCTo/Z+x6CrLhGSdZT8i5W3gvYhD952drTwmTg2iqh1aRcFA4wIU7OLI0/vkVQyAv14A3VOSG//Kvi8/ASeFKs4kBeHHmydpr0NmJ5KXGkZJQxD5dkPlwzT4/H4aw9QiAFRfGaZwiD8z8G91yMccchzPVJv4/4PxsRAQwPHNB7I96IyBb1H9K/YH7AIIfwL4++sBZEzoN8NnYFfw1Xd2cAIA7+z9j1gcOuAlwH+kfM3IiJHzmBgwi3FGpDofNhfPhBRpjP9R5ysY0LppOBfFYFl9/I8ByzwFll4BfL4B6hAB8FYx6CFbQL9+/vQBHPaP3v0BtuY4wGslQAkAtHEWVA0wc/AAG3+vGX4CG+2M3198+v/lPsH9AQABRCgBPAK6EJjF/rF8/PSN4dVXAfisIHKuAEY/0IG/wI1BeoK/wEAB2f0frWOOMTGEtA0Nvo3tH+rmDMb/iO1uWLt+WMa0se5nxLFz6B++gSKk1UbIA1aCoJ4AaEgX2Nd/9xU0Mfce2N5iZHj8HjQgxAdJAKCl4n9+MjBy8TMwsTKCu3/glu+n60TtDwAIIPwJ4Oe7uww8Mt+B/Sfejx++MLz7gegJQLIH6Dy7/0DpP+Aq4M8/epcAf8H2/kOq2dF35aJPt/5HquPBiYAJdaEow3+kNsN/nHGPcx8BtkkkjMkw9FXK/7APFIG7gsBW/ve/bAwv3n5m4Pz5leEjMA7efP3HwMzHDM714IrwL7D7x8kPngH89uYN0E///0FH/wjuDwAIIAIJ4ONrhr9/3jL8ZeT9+RUxLczNyohaAoDaAOASgL43dv8Dlo8gu5FDF6NxxYA4EQSW45nQIhK5X49cNSDLo689YMCxDgFXdfAfx1Iy+MwichUAGw0U5GFgZBdl+PuTheHhyzcM2rx/GR6/A2U20BkJTMDczwYu/kFDvyzAEuDHp8/AKPvEwPTnw/e/7y8StT8AIIDwJ4DLk74xiBo/YmDiUAB3BT9ChLnYESUAuBfAAGkD0DsBAPMA0N7f0EYgZLfCf2gW+/f/P6L4R2vw/fuH1GTAtkMHdQ8M1iVnDAy4zyL4j2d08D+eyEeehwBmfIY3b34ChUHLw/4wPH7xlkGLl5nh0Rtgd54ZNCnDDO4B/Adme2YOPgYWDg6G9/cegyd/GD7dfAVswMOqgJ/4whAggAidFg7qCj4GhS2oBHj+8R94Wwg3GyPqvDOoBwAshuheAgAtB1UByG0A8Amb/xnBR6z8ga5Rh90mhjyKhzLGjyzBiBgrwFX/o+w0wjKbyIBWBTBgiXSss4awRisw8t++/c6wc8t5yDg1EzCXA1Mt6HDOF59+ASObH9L9A8/ZAfncguBEDSr+wQtlPsJH/94z4DkdBAQAAojwcfH/fj8EzfJ9A7Ysv3z/zfDxDzt4oQb81gukcYCBKAFAVQ885oC54vvXrwxfP35gYOHlY2CHnm7G+Pc/eFUN+GBzRkbE9XLIk0NIEcmINPSLMgmEXv//w36+EM6dxf9xbyf7h9T9BA1Z79l+geHDi/fgqUFG0PbvP2wMD998Yvj2+z8DJxcTMJGA1j38AQ8EsfAKM/z++pPh5/v3wITy/df/N2fuQgd/PhMKQ4AAIpwAQF1BYI7//RN0Zfkvhlff2FF2CcMHgoA58e9AlQCMiLXS33/8AOae1ww/gHKg08HZ2DkY2DlA91oAA5KZGZpw/kHOO2KG3AsAK/oZ/qPW/+hjBPDE8R/LcjZs6xWxjAwyYBklRB6zAF2vcfbEXYarZ+8xMHFxgEvc/8BewMdfHAy3XzwDD/pAFvyxgnM/EzcPAyu3EMPHR4/BB2n8/3z37f+f72HHw34jFIYAAURMCfAIvFQHGNrvgT2B1994gU6CLchjRBkHoFcCAM0EggajmEHnBjAxQgd3IKMzv1iAjSE2EWAkszF8+vaTgenzR6A6ZgZWYMiyARMCCDOzcYG7Twx/gDkJGLuw8wpADStmRuwrkBj+YR0Twjoc/I+IMQJsQ8KgUv3p4w8Mx/deAK/oVNczZnh07zawRPsJ7IKzMPz78huyVY4ZckcyqPvHwiMMXofx/c1rcOPm37uryKeDEIwQgAC0XT0PwiAQfSlBQx0cdHHQxMHRxJ/v6h/Q1cmYmA4mRWttC21p8KAOHTR1kYWFgRx3j/t4B/0KUGUnhFbRTkePJEes378Odxwl46OA/yOAO3juYl/XPVsVuMgYx3sEqVIP/06aTTCE4hNYMYUly2B1Cm4yQrACWt9IcSTJlhMyhL7vgNFckjeNmrXKwFruoyOjtuybL28Q4PfyMPCZrNpNFDmj1mWD3XaPMnlivt5gsVriGp2R0+LMCARWQPibjvvav5PIYDyjKMF6+IetTCMPvdm/7ngJQNuVrCAMQ8HRpC5IK3gQ9Kg3/f+zN39AUEQ8WMEqWqTS2MUuvhcXVES9GMg5l8mbeclk8h0AynZgdQ9IUAtJB/BZgExwl9E3CjhlXAH+4wVgtJdoR0SkeKfuGqPtAuOdjY3yEJHqFbST2fxxKZfQvjh+kh4X+J68gYimICUtUwUj9QkUinBN7ZLvEZcKSKNMwqqmwRDLKlRBXMMtc535z5pHiuLjZ19PFrAXtvjYCbzTA5qWaI3hYAJntoTZaqPT6xN1sTu6qhdRaR2mwYHSvk5M0eW/YkJaTYR7F0kQID+uDpk3/zkdhMdZABFOADfmfmUQs3kKDALJn6AryIDNCt4/iEFxUMoFVwEM1E8AoIgH5fivv38ynHn5iOHU81sM9z68ZPjx5zew1APlWCYGDmDR/u/XX/iiDoxVuLCABiaGv6z8DL+AmIkdmNP/fQcm5K/AxPCZ4c+P7wyM37+A1zIwgWfUQNUEN8NvVk6Gb0yQ9XXMTP+AiRCYIFhAiZER5Z5ifLuccDYOkdoGrMAC6Pa15wznjlwB1vtcDPI6xgzu+qwMn4DhfB50ZhD47iBgFcDIDWR+Qyr+FYFdQFaGH69fAbuDfxn+vLkM2xpO1OkgIAAQQMTcGgbsZQHbAUxsJj++fGV4/oGBQZodMVMCGnuDjQNQKwHAivofwK7l6ZcPGE48A+aML+8hh0sBI4OLlQ2xBp8R9Z499IOrGf+jnfcPxsDCHtiX/sXCDUwMogzM/34CS5Ev4ATB9Ad0yCGwqmB4C96dBFpqxcjGA0wMXAzfmdnBZTUz0FDQIY4crIzgXdOwdgg5G05AXb5PwDr+yO5zDP+BDW1ZQ3MGQ01hhkInBoYZJ4F28PFCByj+AQthyOwfaO4fdLUcm5AssIkGOtztHXhJ9Z+3l0GngzxlIPJ0EBAACMDaFasgDEPBS9XWwaUdHQsKfoU/5Ce6Co7ddBJEwcE6SKhW20QviS1BHBQcmrlJLvcuL7mX756N0/UG7OyVAKAMQNE+DuCAYPIA3I3+5TpYlyOiOEzr0xbLwwo7mXMSXQjwV7a1i+v2/MeCxjegKO9KlmjSRC/N4p8MKjY66KMi34qI4lHXFgwdA4a7hLjwY6gxeXezk6DkhiI7lAwV8uZKnYUBLDNEPaMfgraOoX58zgm829wW8wznfY4kTTGajDGbOje2KYAdJwM0hQS1IBsY+ufCCPgPYTxEJalvpIQqj0WdZz/bw58CkHbtKggDQXCWGEwj3AeIWNsK+iv+pF8SsQhYRMVGxCgKFhoU8nD2EuF8FAGL7VLdzs1kn9cQAKiHRR8Ms0ocFPGvShp1UrNxSsn/AMBT+qVtbyeESUyq39stWe0Px7vd4iJ4X6/xowdPnEN33zoQtxfAyQIWlIrMN3S0gQQMF/OUMkF5UOOPp0qFsoAtw5IVSgIipV2lGp/2Jbe1EmUHBYVKheB7sKSoQ74o3GA5X/E2G/QGQ0zGglEfmEbAbKfp4IDO5uXSXcCtKgNYUhI904XfMbjEa7tMMzsvjvxIIwDtBbs3PfenACIuAfz9fJ/hv/i//3/+M33+9IPhHbDfxAw6yfofxHPgXgAFCQBUrH/+853h8tt7DFffPGD4DqzjWZhB9S4L9khHSwCIbiBIFHTHCwekV8CMNg+MvDAEeZ4AaUgYdRqYCRjowBwIxEwgI//+gCaGz0D8lYHx11ugeW8Z/jOxQhIDuGTgBpaQ7ODEwAydOOMEJwbI1bCwhAY6Jvfls08MJ/ZdAKuV0zJisNPlYkgwB7YHgDX48ouQtgEzIxsDJw+wYfoOdF4iaASDBVxNcYgoQg7ZfvcWsm/h7VXYyh+iTwcBAYAAwp4AXJS5Gf79NQOWo/wMf7//Yfi/TY2B0Rk0Lcx2/w4Lww/BJ8BAQh0JZPpP+opgUP8cFBoPvzxnuPj6DsMbYHeOGRhx8FyPK9KRF3cgL9wA5sJvX+8zfHh7jIHlpwADy2choPP+oR5mBQwbVmBDkIdXBTwWwAw9gAplwA9tRhC+np8ZWFUAMQOwqmACVnugUgHUowAlCMYfwNbx9/dgNzCxcIBLhj+sPAw/mDgZPoC7rv/B3UtOYKnABexaMANz9JFdZ4Hdt08MEpp6DJqasgy59pCqbdpxBoZvwF4eDwfoNiU2Bm5QAnjzCVj0AxPkX2bwsblsIvLg9YI/P34ENoI//vz98hTJxT8IAAQQC5bI5wWG7jpg68QFck8q6ARrUNdyPjiIbpxZyHADHJqs4MhngvYCmP4zgidgiGvkMYAj+sPPLwzX3z9gePj5JVgvOwsL7qIejY18OhkjI3RgClhHvnu+HYzxtWlBQFTKi0FOKQlYjIoycAjLoiQkRrQqg5ERcy7gHyOwLmYTBJbjguBxelCuhJQMIAysKn6CqorXwEQObLACS4V/wMTwlYWL4TMwR7MA2w4PzlxheHbzEbjLJ6+lw5BgycCgIsbAMOsEA8M10HYL6CYcHm7QyiAOsIP+/2cGz29wCQgDrZVm+PL4CcNf0FmK7269/f/rM9aLIQkBgADCck7g/3hgZewC9jQzeCIKqgzqe9hNUPBuwH9wFcD0jxE6L48fQMbe/jE8/fKU4fbHx+AuHqgkYPqPeXYe8QkAz6oNzMYG6AQUhjfPtjIICFsw8DLqA7tewNKCixeczZEXiCInAtSlROgrg5jAuR18lck/CXBVAS4dfkGqCobfwKri21tgAxdYOnDyMrx4+Ifh7vkbQHs5GeS0jRjcDdgYggwYGE4B2/BbboBODIWM+ILGH0D7MfkFuOCLGP/+BVYnIgrgGg40+gc6RBrY+kcu/klalAEQQFgSAIM4ZNstMJc4sDNwKwJz5U/MOhTiHmB3iJcRvHcdtCkU30ggbPj2PTBA7gMj//X3D8C0wwQsFpmx5np4K59AAgD3CIDdUg4HYB38G2lS5T/mFS7gSvk+sHt8D9SvBhafvz4wcIMC9c9vSAJkQCwHRx73/48s8A9NngH9FhKg2cBin4GDk+E/sIvJCFquBa4qPjGw/P0C7LJ9YLh98j6wq/mLQUbfmEFXQ4Qhx44BvNBj1knIcjBQbwLUzWSHjkqKivIiLqpm4GRgF1cHj/6Bun//f3///efVedjNYJ9IbX8BBBBmAvj59woDFxMDaJLt/ZnfDNzyLAxCxqzAegatboSFALBMevz9CYMwmxADB7ClxARdbgFbqAnpnv1n+Ak04NWPtwwvvr8FX4wI6uf/x5PrcUU27JRNOA0SB43UybLCL7SCn00I3bMPO1+Y4RowJT8C3VTxk4GVU4yBk0sF0pYB9vP/QbsOf5EaA9jaBrjKGPS9g/BVPsA2wX82YK8CiEEzkvfOH2X49u4rg5C8EoOSpgZDug3kFpXO/QwMTz5Din5m6OgjOzQRiIKuQQOfKP0X2L4VZ2AXlmf4/vY1w++v34Dt80ef/n4Gnw5K8Gh4bAAggDATwPGHWxkMpacyCHBl/n71j+nBnG8Mv/w4GMTt2CGnU/1FDRUQ9ebPa4Z3f94B0w0XAyczJ7BfzAqu48H32AD71d/+fmf4DKwXQUO5wHIAPKxKKNdjSwA4aehZdP+hR3jB1tbD1vyDSgaG08Asdv0nuIfEwiHKIKWcx8DGLc/AIiQNTD1sDH+RShvG/wQrEqzrC/CtEAJN9Ly4+ZDh9Z1HDGz8Agzy2oYMAUaMDA6qDAw7bzIwHADmYW5o0Q+LeFAPAnTdnYgwF7BtCQyz33+BJQRo8Iqd4dvLl5Bzg95cekrq6B8yAAggbL2A7wznn3YwqIl+Z5DkzwFGOMfTdd8Zfn/4xyDjwwH2CKhdwAgNffAePWiu//7/G7BO/wIVhQ3IIA7CY8GS63FFOvISLmw01tKAEU0M5LvPwIRxFNguegi6veo7AxuvGoO0ci4Dj4g+A6ugJDBHcSIiHykiGRlR9wcwYmlhMGLL+WjrDBigEz1fX39ieHLqHJivoGPEYKrBw5BmzcDw+AMDw4KzwBzPDkkk4JwPxaDiHzTIxM/DxsDHy8nw8c1vYKPvD8PPD1+BHY53oAGhv3/eXHpM6ugfMgAIIGwJ4C84Rd16PYnh55+XDLJC1cAySeDVzl8Mv9//Z1CO4Gbg4AH2R38xgot7RqSQApcQoONMQGN5oFMtQcO0//8jpkn/4RrzJ7HYZ8SfGCCdFCDxGuiS/d8Z/r8CRj7zDwYuIXMGScVMBk5BNQYWYOSD7qv59xe5TYMayxi7grEUAVgXDqGtHQMN4jw6fQ5YXAO7fBo6DKpqkC4faJfVtGPAmAPmW9DRgOB6nwmpBIBcUs7ACUwAAvzABABMRKBDtL4Ac//PT1+A3cBXX/++vQTb+fuVgQwAEEC4BoL+gPuUD98vZvj2+zmDsmg7Awer7PuTvxluvP/KoB0vxMAvzcHw78d/aG6HNBr/QY8yBdF/GP+AG4UgGiwOze2kdPNwlQJ4cz1IDbBR+Of+b4bfB34w/P8MuvL0NwO/qDeDmFwisCqWBUa+BLii/f8HsWAQfBY6I2pf9T8D9tXBWNsBKKuEESkDdOPps0vXGUDVNI+YJIOcpj5DtDkDg44UA8Oy8wwM515A631YjofR0CoAXAIA22QC/BwMD0E7mX//Znh//x6k9f/u6ivour+XpIz+IQOAAMI3EvgXXK+8/rKd4fvvVwwaYl0MvCwGX27/Z7g05R2DYYIkg6Q2H8Of7//gDT1QRIMgOOKB9QQIgwaIgCxIqQA7ZfA/gcEdqBisesAb4ci5Hnop8+/Lvxh+HQXW9z9/g++xE5KMYRCRCmFgFZBkYOETBx+v/v/Pf8RWcHC9/x9RAqBce8OIu2BlRK34/yPXGaDGJ7AU+vDkFcOLy1cYmDg4GBR0jRkcdVgZIo0ZGC4B8+yaK8AuHwck8llhRT804jmgCQFUKoBOjhUD3aAJngX8y/Dt7TvwKap/3xG39BsfAAggQnu5/4EXFn75eYLh0os0hq9qd0Au/P7yJ8Ppqc8YXhz/xiDEww9MFzwMPExAzMzDwA1spHAxc8EbhOxM7AxsoMEP0AgZIyM8YkEYFnHoGD3HEyUGPU/214lfDD/3/wBdusvAyMoFzPU5DMJSkQwsAvIMTDwSkNY5MPf8h97LB2rUQjfWws87homB8b//OK9q+Y+k/z90eS9MDOSwX19+Mjw9C6zgf/5gkNXQZdBUg3T5QEe7gGb6fkKngsEzi8yokc/BAqkiuNkhI4LKcvzQHTmMkNz/9fX3f6/P4jz4gVgAEEDETQeDFhf++n2F4bfLEYY/z1UYWLcDuyA/GI7NvA9sFzAxmPkqAuum/+DxAFBOB63T+8X4i4HlHwt4ggd0YgXjX0hVAZo1gJUExLTyCeV6MJsVfKohw7e9Pxl+XwVWqIzAlj6nFIOobCYDD7DeZxGQYGDi4odEPMog0n+MHA9u2IES6j+0up8RR9n/HzFqhDIQCjTj2cWLDD9evWQQklNiUNPWYMgGRr4MsEc48QhkCzvoQFDQtXmgU9bAEc0OiXQWUK8BvCD7H8PbT98ZHj/4wfDk+Wf4jVz//v5h+P/h5htgKiBr9A8ZAAQQCwlqQUeA3Wb4awL0NB/QlZuAxdE7hlPL7jD8fPuXwTVGj4GJk5Hh+68fDL8ZfzOw/mdl+Mn4E9I+AOUqpv+QXAeO/j8463l8xT26PLgIA63mev+P4fP2nwx/HvwFN/bYeXQZRKXTGDgFdBiYgZHPyMYFLjpRO/aMiHoe7cxCxJG4uBuB6KPeyAtAWIAtt7f37jO8u3Mb2OUTZNAwMmaQF2dk+Ap0wrRTDAyHgQU36GYY8PHvwFb9x4/fGV5+AbbsP30Fn8/8GdjK//jhG8M7YHvr+/ff4DsBQZs/ITdmQe4OYnx9BnYrOMGbwfABgABiIUn1tyc7gT4qZ/irysfKmgBsUO1g+Pr+MsPFbXcZvr37wxCUYc4gLCDE8OX7V4afoEUW4DlD6C0PTJDBIVgigC0eQa//sUUyVjaoKuFgZPj5+C/Dp80/GP6+BkX+TwZOfltgfZ/CwM6nwMDMLwndPfMXJaIh08H/US6TZkQa5vz/D0eko6UH6IApeFEpMxSzAyP/26dPwHr/IliBsrYOAxcwe1+7+5bh6HnI7l6mv9/BaytAS+1BO69B/fm/4KPFoRcwMiFfx4Y+z/2HgfHZjpv/XxwA1i0Md6H1P9kAIIAYke+gYWQkeCsUB4NOdSsDj3IRA7B4l1YTB8btBoYXdzczgM5jkVSTYIjMs2MQU+Bj+PD1E8O3f98Yvv/7zvDtL4T+DvI4kAYlDsiuXsRo3X8sI3g42aBwADawvl0G5p7tQHO+ABt7rH8ZeIX8GATFIoANPWkGVn4JcKpjhJ4cgpg+ZsTsfSCXAEhVASxBMiNdkwe5OpcRQjNDDs1iRmrT/P75l+HMvsMMrx89BQ/YcHBxM/z6+QN8wBO4FPrzB7Uhw4i+1hg6igWOcNDoFujq0M/AAHv/jeE3sIh4f+Exw+ujoAGFE0B8FNoIJGs1LijuAQKI1AQA7PyzSTLoN29kYBMxAR1bomtnyfDpwwqG+5dmA33PxMAnIcgQnmnHoGYqwfDuyweGL3++Aou+r+DEAKJhCeAn+Pihf8RFODIbOvH36eQfhk+7Qeeh/gYfkMQvEsnAJ+wNjHxJBmYeUcQ2UUZG1L1haA1JWMMUdi8y/H5kZsgmEjANXeXDBN0VDcqxoNPTQEfa//gKysVfwPjn9+/AsPjM8O7FG8QVa7Ax5v9oi8Rgw5agUx9AU+6/PwHrTmAk//rwheEXsJP/8/VHhh8v3zN8f/GO4S8wN/39+ZEBstEDRIOK/xvQ+v83ubkfFPcAAURqAoCsuJDysgXi1cCOihDoQmdjdy+G189XMtw62wWe5GDl5mHwT7ZkMHZTYPjw/SPDp1+fGb78/YKSCH6AE8AfkiIfNJkDWgP3btdvhi/HQSN7v4BdLX4GAbFUBm4BawZmPikGZk4B8MERsFFGWM5kguZUUISywJd9g1rgsFzMCF6fBxly/w9eZQM+ufzXL3Dk/vj6BbzrCLQw9tvnz+CIB0U4SB2oigHnbFBQgk7tYoFdivgP0o349xsYyd9+Mfz++AOYk78CI/kzGP988wkcyT+Akfzv5ydgqvoEbdF/gQ7sfIFG+hfoOP8PaIMPxqdoHT4o7gECiJwEAALcDCppmQxCpl1AjzPyiUkxmHr6Mbx+tpPh6vEKYIJ+D4wsTgbHYD0G2zB1hi//vjB8+PGR4es/RGkAqg7QqwG8kQ/ax/H5P8PrDb8ZvgOLfgbm7wzM7AoMAuLpDFx8+sCWvhQDGwcPMFL/QSOZEbLZg5kRygdtp2KEJAhw6foPfAAz6HJK0OXUP4AR+/3LZ/AeyB/fvkBz9mfw1bWghACO5L9/UQcpGJHWF/0DVuJ/gf2+3x9BuRgYye+BOfndJ2Akf2T4/vwdUOw9w78fn4AJ4jOeSAZF7k/omD4y/gONbGocsYiSAAACiNwEAFIozKBe2MnAr50EbKYyiCqqMOg7ujC8f30WmAhKGX58BLZP/nIw6LooMzgnaDD85vjB8P7bR3BiAEU+KBH8gu4lwBbhyHzQ2pMfL/4zvFoF7F4+hEQ+j6Ahg4R0CjDnqzNwCUtCdvwwQTaCMkFXfYLqXUhR/RsYqV/BkQrKyWD6G5QNbIyBpoP/gSP4D+JiP3gdAbus+C+kyf4L2GT/DcKgovo9MCe/B+biVx8Yvj97C+yWfQItzwEq/oIjkr9CczFyJP9Gov9TO5IJJQCAACI3AcBm16UYdOoWMHDKOoHqYikNHQZtK1uGLx8fMlw+XsLw5TWwz/Obg0HGQIzBOVOdgUkUdCnyR3DkgxqGoLYALAFgjXxoH//zjX8Mr1b/ZvgD7G4ysP1hEBV3ZpBXiAe29GUYWIH1PfhKlR/fwcUzpKgGVqHfvkL5n8Hsv8i5GDxHzARtaTMh1reDIvnvt9/gXAyK5F8fIcX0r3fAMv/VO4afwOL6D7BO+49SVKNHMqyoRs/Fv6E5mW4RTEwCAAggShIApD3AJaPOoJyxjIFdTBeUg6Q19RnUTa0YfgLbLDfO1DG8fbQB6G0OBn55fgabNHkGHjUGhg/AohWcAP4D2wH/cCQARkix/+HkX4aX64H17A9QN+8fAzeHDYOokBcwNDkZ/jBygk/M/gEuqn8gFdV/ENNwyLn5P3j3PbBIALWqwTn5KyQXA4vqX28/QYrqd5D6GFJUf0WL6C9IRfUPHLn472CKZEIJACCAKE0AIMAOrAaMGeRjFwHbhMqgxpCkui6wF2ADjMg/DHcv9TA8uzMPGDyMDOyCnAyGSeIMwqZMDJ9BxfA/7AngP/SG8Ne7/jK82QGt/kArb7/pMTD+VmP4DzoggYkLqZWP1KUCbWQFTZyDc/CHbwx/PkEiGZSTf74FtqyBRfVvYDH0D96q/oqWk2Fi33EU1X8YUK8pHLIAFPcAAUSNBAACXAwi1vYM0v5zGFj4pUA5UEJNl0HVyJqBhY2J4fGtVQx3r7QBc/F38C2gqqF8DBJuzAzff4EGQVATAKhi+QPMW8/X/WH4cBQY8aygSOYDRocpMPjFQRszQOeh/QcX1bC+MTgnv/sMbnD9fP2e4ecrYFH9DVRUf0SL3K9orervWIrpX4OtqKZlAgAIIGolAJBGHgYhMwcGmeAZ4ETw7y+DsJwqsJEmDGxZ/2R493IPsK0IrA7+A8P9LwuDlCc7g3QAsOJl+c/w5yck8hnZgFnu7X+Gx0v+MHy9DLo5AXQxLrDF+FLoE8MXYASz878FlvGQbtPPtx+AreqPaK3qYVlU0zIBAAQQtRIALBHwMvCqWzLIRU9i4BBXA/eNwYf1MkFOFmd6AaR3AJW9BcY0I4OAGTOw5mBhYOaBlABfHgAjf+Efhh+PQCclgvLr99cMV++fAlbbN6CzXu+hAyGf0SL5J1Lk/hpuRTUtEwBAAFEzASBKAh5lEwaZkHYGTmlDYD/sP8Pv99/BfePfwA7271ufGEReyDFwcYgz/GBk4FJjZFBIYmb48YaB4dH8Pwx/3jNAIv/Nh8cMdx8fAnYFQGPeoKHPJ9Bi+ydShP9hIGEXzCjATAAAAUTtBABLBKAjOFQZWAUsgQ0yeYY/X/6hFNWsLGwMavLpDLzchqBoZBGELNz8C5JlATro+etbDI9eHAbygP1IhuNAfB8a+aO5mcoJACCAaJEAEBNHDAwiQMwPXXiCXBeDLJNiUFNoYBDk80EMaAK7DY+eXWJ4+fY4NOJBCeARA4GjzkYB+QkAIIBomQCwzab/R0sg8gxyUpkMfDx+wPYCM8OzV9cZPn25ABQH3XF7Flrv/x6NKtolAIAAokcCwAeA7X4GSSDWAmI5aJ1+G4hvQlqKDH9Go4m2CQAggAY6AcCGlHkZIGdP/UUaTh1t3NEhAQAEEEoCGAUjDwAEGAAqc+XUBj0CJwAAAABJRU5ErkJggugB";
						var signatureData = "//CapturedImagesCache/WP_20150109_001.jpg"
						App.sessionVars.imageType = 'jpg';
						alert(1);
						$(".imagesIds").each(function() {
							imgsrc = this.src;
							height = ((this.height / this.width) * 180) + 25;
							height = (height > 250) ? 250 : height;
							doc.addImage(imgsrc, 'JPEG', initialx, initialy, end, height);
							// adding signature
							// fix aspect ratio
							//debugger
							alert(App.sessionVars.imageType);
							if(App.sessionVars.imageType && App.sessionVars.imageType == 'png')
							{
								doc.addImage(signatureData, 'PNG', 10, 10, 20, 20);
							}
							else
							{
								doc.addImage(signatureData, 'JPEG', 10, 10, 20, 20);
							}
							
							alert(3);
							doc.setTextColor(100);
							//color
							doc.setFont("helvetica");

							doc.setFontSize(10); 
							doc.text(10, 10, 'Document Securely Stored and Shared by');
							doc.setFontSize(13);
							doc.text(77, 10, 'www.zinglifedocs.com');
							doc.setFontSize(10); 
							doc.text(122, 10, '. Document Originally Saved on '+ today + '.');


							doc.setFontSize(14);

							// intialx for copyrightText depends on copyrigth text size
							doc.text(190 - (($('#copyrightText').val().length) * 2), 290, copyrightText);

							if (($(".imagesIds").length - 1) != $('.imagesIds').index($(this)))
								doc.addPage();
						});
					} else {
						$(".imagesIds").each(function() {
							imgsrc = this.src;
							height = ((this.height / this.width) * 180) + 25;
							height = (height > 250) ? 250 : height;
							doc.addImage(imgsrc, 'JPEG', initialx, initialy, end, height);
							doc.setTextColor(100);
							//color
							doc.setFont("helvetica");

							doc.setFontSize(10); 
							doc.text(10, 10, 'Document Securely Stored and Shared by');
							doc.setFontSize(13);
							doc.text(77, 10, 'www.zinglifedocs.com');
							doc.setFontSize(10); 
							doc.text(122, 10, '. Document Originally Saved on '+ today + '.');

							doc.setFontSize(14);

							// intialx for copyrightText depends on copyrigth text size
							doc.text(190 - (($('#copyrightText').val().length) * 2), 290, copyrightText);

							if (($(".imagesIds").length - 1) != $('.imagesIds').index($(this)))
								doc.addPage();

						});

					}

					file = doc.output('datauristring');
					//Upload new pdf
					var parseFile = new Parse.File(documentName.replace(/\s+/g, '_').trim().toLowerCase() + ".pdf", {
						base64 : file
					});
					var UserImages = Parse.Object.extend("user_uploaded_images");
					parseFile.save().then(function() {
						var user_images = new UserImages();
						if (documentName) {
							user_images.set("DocumentName", documentName);
						}
						if (documentTags) {
							user_images.set("DocumentsTags", documentTags);
						}
						if (copyrightText) {
							user_images.set("CopyrightText", copyrightText);
						}
						user_images.set("pdf_doc", parseFile);
						if (App.models.user.get("attributes.username")) {
							user_images.set("username", App.models.user.get("attributes.username"));
						}
						user_images.save(null, {
							success : function(ob) {
								//App.vent.trigger("closePopups");
								//App.controller.showPopup('uploadMoreDocumets');
								console.log("______________________________Parse loaded pdf details");
								console.log(JSON.stringify(ob));
								if (_this.$el.find('#offLineDocumentCheck').is(':checked')) {
									console.log("_________________________________________here________________________")
									console.log(ob.attributes.pdf_doc.url());
									var imageUrl = ob.attributes.pdf_doc.url();
									_this.saveOffLinePic(documentName, imageUrl);
								}
								// Now upload thumnail ie First Image
								var parseFile = new Parse.File(documentName.replace(/\s+/g, '_').trim().toLowerCase() + ".jpg", {
									base64 : $(".imagesIds").eq(0).attr('src').replace(/^data:image\/(png|jpg|jpeg);base64,/, "")
								});
								var UserImages = Parse.Object.extend("user_uploaded_images");
								parseFile.save().then(function() {
									var user_images = new UserImages();
									user_images.id = ob.id;
									if (documentName) {
										user_images.set("DocumentName", documentName);
									}
									if (documentTags) {
										user_images.set("DocumentsTags", documentTags);
									}
									user_images.set("user_images", parseFile);
									if (App.models.user.get("attributes.username")) {
										user_images.set("username", App.models.user.get("attributes.username"));
									}
									user_images.save(null, {
										success : function(ob) {
											App.vent.trigger("closePopups");
											App.controller.showPopup('uploadMoreDocumets');
											console.log("______________________________Parse loaded image details");
											console.log(JSON.stringify(ob));
											if (_this.$el.find('#offLineDocumentCheck').is(':checked')) {
												console.log("_________________________________________here________________________")
												console.log(ob.attributes.user_images.url());
												var imageUrl = ob.attributes.user_images.url();
												//_this.saveOffLinePic(documentName, imageUrl);
											}
										},
										error : function(e) {
											App.vent.trigger("closePopups");
											console.log("Oh crap", e);
										}
									});
								}, function(error) {
									App.vent.trigger("closePopups");
									console.log("Error");
									console.log(error);
									var options = {
										"title" : App.settings.messageTitle.failureTitle,
										"message" : "No Internet Connection"
									}
									App.controller.showPopup('alert', options);
								});
								// upload thumnail
							},
							error : function(e) {
								App.vent.trigger("closePopups");
								console.log("Oh crap", e);
							}
						});
					}, function(error) {
						App.vent.trigger("closePopups");
						console.log("Error");
						console.log(error);
						var options = {
							"title" : App.settings.messageTitle.failureTitle,
							"message" : "No Internet Connection"
						}
						App.controller.showPopup('alert', options);
					});
					//Upload new image End
				}
			} else {
				App.vent.trigger("closePopups");
				var options = {
					"title" : App.settings.messageTitle.failureTitle,
					"message" : App.settings.messageSubject.connectionLoss
				}
				App.controller.showPopup('alert', options);
			}
		},
		saveOffLinePic : function(imageName, imageUrl) {
			//Main Code Start
			var fileSystem_this = null;
			window.requestFileSystem(LocalFileSystem.PERSISTENT, 0, gotFS, fail);

			function gotFS(fileSystem) {
				fileSystem_this = fileSystem;
				var entry = fileSystem.root;
				entry.getDirectory("zingLifeDocs", {
					create : true,
					exclusive : false
				}, onGetDirectorySuccess, fail);
			}

			function onGetDirectorySuccess(dir) {
				var url = imageUrl;
				url = encodeURI(url);
				var fileTransfer = new FileTransfer();
				if ((navigator.userAgent.toLocaleLowerCase().indexOf('iphone') != -1) || (navigator.userAgent.toLocaleLowerCase().indexOf('ipod') != -1) || (navigator.userAgent.toLocaleLowerCase().indexOf('ipad') != -1)) {
					var imagePath = dir.nativeURL + imageName + ".pdf";
					// full file path
					fileTransfer.download(url, imagePath, function(entry) {
						console.log(entry.fullPath);
						// entry is fileEntry object
					}, function(error) {
						console.log("In Some error");
					});
				} else {
					var imagePath = dir.nativeURL + "/" + imageName + ".pdf";
					// full file path
					fileTransfer.download(url, imagePath, function(entry) {
						console.log(entry.fullPath);
						// entry is fileEntry object
					}, function(error) {
						console.log("In Some error");
					});
				}
			}

			function fail(error) {
				console.log(error.code);
				var options = {
					"title" : App.settings.messageTitle.failureTitle,
					"message" : "Got File system Error, Please try again."
				}
				App.controller.showPopup('alert', options);
			}

			//Main Code End
		},
		cancelImageUpload : function() {
			if (this.options.imageUrl == 'editDocs') {
				var options = {
					"title" : 'Are you sure to discard changes.',
					"callBack" : function() {
						App.routers.mainRouter.navigate('#imageZoom', {
							trigger : true
						});
					}
				}
				App.controller.showPopup('confirm', options);
			} else {
				var options = {
					"title" : 'Are you sure to discard this image.',
					"callBack" : function() {
						App.routers.mainRouter.navigate('#userHome', {
							trigger : true
						});
					}
				}
				App.controller.showPopup('confirm', options);
			}
		},
		showUploadPopup : function() {
			console.log("upload popup clicked");
			App.sessionVars.mainTagsFalse = true;
			App.controller.showPopup('addMoreDocumets');

		},
		deleteImage : function(e) {
			var _this = this;
			console.log('delete image clicked');
			console.log(e.currentTarget);
			if ($('.doc_img_con').length == 1) {
				_this.cancelImageUpload();
			} else
				$(e.currentTarget).closest('div[class^="doc_img_con"]').remove();
		}
	});
})();
