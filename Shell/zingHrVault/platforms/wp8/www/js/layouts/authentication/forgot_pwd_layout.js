;
(function() {
	app.pages.forgotPwd = Backbone.Marionette.Layout.extend({
		template : '#forgotPwdLayout',
		events : {
			'click .submit_h' : 'submitPassword',
			'click .cancel_h' : 'cancelSignup',
			'keyup #emailIdForPassword' : 'doneHandling',
			'focusout #emailIdForPassword' : 'handlingKeyBoard',
		},
		init : function() {
			console.log("forgot password LAYOUT INIT");

		},
		doneHandling : function(e) {
			if (e.keyCode == 13) {
				this.submitPassword();
			}
		},
		handlingKeyBoard : function(e) {
			if (App.settings.isDeviceReady && cordova && cordova.plugins && cordova.plugins.Keyboard) {
				cordova.plugins.Keyboard.close();
			}

		},
		submitPassword : function() {
			this.handlingKeyBoard();
			console.log("submit pwd clicked");

			var emailAddress = this.$el.find('#emailIdForPassword').val();
			var emailPattern = /^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$/;
			if (!emailPattern.exec(emailAddress)) {
				var options = {
					"title" : App.settings.messageTitle.failureTitle,
					"message" : App.settings.messages.inValidEmailId
				};
				App.controller.showPopup('alert', options);

				return false;
			}

			var checkNetworkConnection = PG_checkNetwork();
			if (checkNetworkConnection.result) {

				App.controller.showPopup('loader');
				Parse.User.requestPasswordReset(emailAddress, {
					success : function(data) {
						App.vent.trigger("closePopups");
						// Password reset request was sent successfully
						var options = {
							"title" : App.settings.messageTitle.resetPassword,
							"message" : App.settings.messageSubject.resetPasswordLink,
							"callBack" : function() {
								App.routers.mainRouter.navigate('#userLogin/logout', {
									trigger : true
								});
							}
						};
						App.controller.showPopup('alert', options);

					},
					error : function(error) {
						App.vent.trigger("closePopups");
						// Show the error message somewhere
						console.log("Error: " + error.code + " " + error.message);
						var options = {
							"title" : App.settings.messageTitle.failureTitle,
							"message" : (error.message) ? error.message : "No Internet Connection"
						};
						App.controller.showPopup('alert', options);
					}
				});

			} else {
				App.controller.showPopup('alert', {
					"title" : App.settings.messageTitle.failureTitle,
					"message" : App.settings.messageSubject.connectionLoss
				});
			}

		},
		onCloseLayout : function() {
			App.vent.trigger("closePopups");
			console.log("LAYOUT CLOSE");
		},
		cancelSignup : function() {
			App.routers.mainRouter.navigate('#userLogin/logout', {
				trigger : true
			});
		}
	});
})();
