/*!
 * Backbone Overrides to sync/layout/itemsviews etc in one place for easy code keeping
 */

(function(Backbone, Marionette, _, $) {

  //Override Backbone layout to include initialize and render methods by default
  Backbone.Marionette.Layout = Backbone.Marionette.Layout.extend({

    initialize : function() {
    },
    onRender : function() {
      if (this.init) {
        this.init();
      };
    },
    onClose : function() {
      if (this.onCloseLayout) {
        this.onCloseLayout();
      }
      delete this;
    }
  });

  Backbone.Marionette.ItemView = Backbone.Marionette.ItemView.extend({
    render : function() {

      this.isClosed = false;

      this.triggerMethod("before:render", this);
      this.triggerMethod("item:before:render", this);

      var data = this.serializeData();
      data = this.mixinTemplateHelpers(data);

      var template = this.getTemplate();
      var html = Marionette.Renderer.render(template, data);
      this.$el.html(html);
      this.bindUIElements();
      if (this.model && this.follow_model_changes) {
        this.listenTo(this.model, "change", this.render);
      }

      this.triggerMethod("render", this);
      this.triggerMethod("item:rendered", this);

      return this;
    }
  });
  

  //Override Backbone Model to include network failure error in fetch and add loader.

  Backbone.Model = Backbone.Model.extend({

    fetch : function(options) {
      if (!(options && options.suppressloader)) {
       App.controller.showPopup('loader');
      }

      options = options ? _.clone(options) : {};
      if (options.parse ===
      void 0) {
        options.parse = true;
      }

      options.timeout = options.timeout || 20000;

      var model = this;
      var success = options.success;
      options.success = function(resp) {
        if (!(options && options.suppressloader)) {
         App.vent.trigger("closePopups");
        }
        if (!model.set(model.parse(resp, options), options))
          return false;
        if (success)
          success(model, resp, options);
        model.trigger('sync', model, resp, options);
      };
      wrapError(this, options);
      return this.sync('read', this, options);
    }
  });

  //Override Backbone collection to include network failure error in fetch and add loader.
  Backbone.Collection = Backbone.Collection.extend({
    fetch : function(options) {
      if (!(options && options.suppressloader)) {
       App.controller.showPopup('loader');
      }

      options = options ? _.clone(options) : {};
      if (options.parse ===
      void 0) {
        options.parse = true;
      }

      options.timeout = options.timeout || 20000;
      var success = options.success;
      var collection = this;
      options.success = function(resp) {
        var method = options.reset ? 'reset' : 'set';
        collection[method](resp, options);
        if (!(options && options.suppressloader)) {
         App.vent.trigger("closePopups");
        }

        if (success)
          success(collection, resp, options);
        collection.trigger('sync', collection, resp, options);
      };

      wrapError(this, options);
      return this.sync('read', this, options);
    }
  });

  var wrapError = function(model, options) {
    var error = options.error;
    options.error = function(resp) {
      if (!(options && options.suppressloader)) {
       App.vent.trigger("closePopups");
      }
      if (resp.status == 0 && !options.suppressNetworkCheck) {
        App.vent.trigger("checkNetworkStatus");
      } else {
        if (error)
          error(model, resp, options);
      }
      model.trigger('error', model, resp, options);
    };
  };

})(Backbone, Backbone.Marionette, _, window.jQuery);
