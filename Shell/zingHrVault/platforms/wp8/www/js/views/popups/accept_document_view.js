window.app = window.app || {};
app.views = app.views || {};
app.views.acceptDocument = Backbone.Marionette.ItemView.extend({
	template: '#acceptDocument',
	events: {
		
		'click .cancel_callback_h': 'closePopup'
	},
	initialize: function() {
		
	},
	onRender: function() {
		
	},
	closePopup: function() {
		App.vent.trigger("closePopups");
		// App.routers.mainRouter.navigate('#userHome', {
		// 	trigger : true
		// });
	},
	onClose: function() {
		
	}
});