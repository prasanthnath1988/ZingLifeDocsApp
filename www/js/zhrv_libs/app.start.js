/**
 * APP START FILE
 *
 * This is the startup file of the Application
 *
 * This is the last file to be loaded & processed after everything is in place(deviceready or documentready).
 * If we have an autologin procedure, we will pull it here
 */
$(document).ready(function() {
	App.settings.screenHeight = $(window).height();
	//included fastclick to avoid 300ms tap delay
	FastClick.attach(document.body);
	if (!((navigator.userAgent.toLocaleLowerCase().indexOf('android') != -1) || (navigator.userAgent.toLocaleLowerCase().indexOf('iphone') != -1) || (navigator.userAgent.toLocaleLowerCase().indexOf('ipod') != -1) || (navigator.userAgent.toLocaleLowerCase().indexOf('ipad') != -1))) {
		App.settings.deviceHeight = $(window).height();
		App.settings.deviceWidth = $(window).width();
		var parseAPPID = "BJYBrQf1wjRwvAb4pcpnuPxRr205QjmhXjNSPc1D";
		var parseJSID = "mmvhYZF8zIbahPjDvd7KaOA49CqJ88mdn8lAqND0";
		// Initialize Parse
		Parse.initialize(parseAPPID, parseJSID);
		App.start();
	}
});
document.addEventListener("deviceready", function() {
	console.log("Device Ready called");
	App.settings.isDeviceReady = true;
	App.settings.deviceHeight = $(window).height();
	App.settings.deviceWidth = $(window).width();
	onDeviceReady();
	if ((navigator.userAgent).toLowerCase().indexOf('android') != -1) {
		document.addEventListener("backbutton", App.controller.backButtonHandler, false);
	}
}, false);
document.addEventListener("pause", function() {
	App.settings.appPaused = true;
	App.settings.deviceHeight = $(window).height();
	App.settings.deviceWidth = $(window).width();
}, false);
document.addEventListener("resume", function() {
	setTimeout(function() {
		setTimeout(function() {
			App.settings.appPaused = false;
			App.settings.deviceHeight = $(window).height();
			App.settings.deviceWidth = $(window).width();
			if ($("#popupRegionMask").hasClass("mask_active_h")) {
				App.controller.setMaskHeight();
			} else {
				App.controller.setBodyHeight();
			}
		}, 400);
	}, 0);
}, false);

function onDeviceReady() {
	console.log("device ready fire.");
	var pushNotification;

	if (App.settings.production_mode) {
		if (window.console && window.console.log) window.console.log = function() {};
		if (window.alert) window.alert = function() {};
	}
	if ((navigator.userAgent).toLowerCase().indexOf('android') != -1) {
		document.addEventListener("backbutton", App.controller.backButtonHandler, false);

	}
	var parseAPPID = "BJYBrQf1wjRwvAb4pcpnuPxRr205QjmhXjNSPc1D";
	var parseJSID = "mmvhYZF8zIbahPjDvd7KaOA49CqJ88mdn8lAqND0";
	Parse.initialize(parseAPPID, parseJSID);
	// Initialize Parse

	App.start();
	pushNotification = window.plugins.pushNotification;
	pushNotificationSetup(pushNotification);
}