;
(function() {
    app.pages.userLogin = Backbone.Marionette.Layout.extend({
        template: '#userLoginLayout',
        regions: {
            content: ".content_wrapper_h"
        },
        init: function() {
            console.log("login LAYOUT INIT");
            App.views.userLogin = new app.views.userLogin;
            this.content.show(App.views.userLogin);
        },
        onCloseLayout: function() {
            console.log("LAYOUT CLOSE");
        }
    });
})();