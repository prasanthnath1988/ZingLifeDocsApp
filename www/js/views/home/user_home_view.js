;
(function() {
	app.views.userHome = Backbone.Marionette.ItemView.extend({
		template: '#userHomeView',
		events: {

			'click .upload_icon_h': 'showUploadPopup',
			'click #myCanvas': 'showTaggedImages'
		},
		canVasrender : true,

		initialize: function() {
			_.bindAll(this, 'drawTextAlongArc','renderMenuArc','onRender');
			if(!App.models.user && Parse.User.current()){
				App.models.user = new app.models.user();
				App.models.user.set(Parse.User.current());
			}
		},
		onRender: function() {
			var _this = this;
			if($('body').attr('id')=="userHome_Body"){
				$('.outer').css('padding','0px');
				$('.outer').css('padding-top','20px');
			}
			if(App.models.user && App.models.user.get('attributes.userProfilePicture')){
				var userPic = App.models.user.get('attributes.userProfilePicture');
				this.$el.find('.user_image').css({'background':'url('+userPic+')','background-size':'cover'});
			}else if(App.models.user && App.models.user.get('attributes.userFBProfilePicture')){
				var userPic = App.models.user.get('attributes.userFBProfilePicture');
				this.$el.find('.user_image').css({'background':'url('+userPic+')','background-size':'cover'});
			}

			var checkNetworkConnection = PG_checkNetwork();
            if (checkNetworkConnection.result) {
            	this.renderMenuArc();
    			if(_this.canVasrender){
    				setTimeout(function() {
    					App.views.userHome.render();
    					_this.canVasrender = false;
    				}, 420);
    			}
            }else{
            		App.controller.showPopup('alert', {
						"title" : App.settings.messageTitle.failureTitle,
						"message" : App.settings.messageSubject.connectionLoss
					});
            }
		},
		onClose: function() {
			console.log("VIEW CLOSE");
		},
		showUploadPopup: function() {
			console.log("upload popup clicked");
			App.sessionVars.mainTagsFalse=true;
			App.controller.showPopup('uploadOptions');
		},
		drawTextAlongArc: function(context, str, centerX, centerY, radius, angle, position, adjustFactor) {

			var strCharAdjust = 0;
			for (var n = 0; n < str.length; n++) {
				var charAdjust = 1;

				if (str[n] == 'O' || str[n-1] == 'O' || str[n] == 'M' || str[n-1] == 'M' || str[n] == 'W' || str[n-1] == 'W' ) 
				{
					charAdjust = 1.2;
				}
				if (str[n] == 'I' || str[n-1] == 'I' || str[n] == 'J' || str[n-1] == 'J' ) 
				{
					charAdjust = 0.9;
				}
				if (str[n] == 'W' || str[n-1] == 'W' ) 
				{
					charAdjust = 1.3;
				}
				strCharAdjust = strCharAdjust + charAdjust;
			}

			strCharAdjust = strCharAdjust/str.length;
			adjustFactor = adjustFactor / strCharAdjust;


			context.save();
			context.translate(centerX, centerY);
			context.rotate((position - 1) * 45 * Math.PI / 180);
			context.rotate(-1 * (angle / str.length) / 2);
			context.rotate((adjustFactor - str.length) / (2 * adjustFactor) * 45 * Math.PI / 180);
			for (var n = 0; n < str.length; n++) {
				var charAdjust = 1;

				if (str[n] == 'O' || str[n-1] == 'O' || str[n] == 'M' || str[n-1] == 'M' || str[n] == 'W' || str[n-1] == 'W' ) 
				{
					charAdjust = 1.2;
				}
				if (str[n] == 'I' || str[n-1] == 'I' || str[n] == 'J' || str[n-1] == 'J' ) 
				{
					charAdjust = 0.9;
				}
				if (str[n] == 'W' || str[n-1] == 'W' ) 
				{
					charAdjust = 1.3;
				}
				context.rotate(angle / str.length * charAdjust);
				context.save();
				context.translate(0, -1 * radius);
				var char = str[n];
				context.fillText(char, 0, 0);
				context.restore();
			}
			context.restore();
		},
		renderMenuArc: function() {
			var _this = this;
			var canvas = _this.$el.find('#myCanvas')[0];
			var context = canvas.getContext("2d");
			var tagsList = Parse.Object.extend("tags");
			var query = new Parse.Query(tagsList);
			query.ascending("tag_id");
			var inputText = [];
			App.controller.showPopup('loader');
			if (window.sessionStorage.getItem('inputTags')) {
				App.vent.trigger('closePopups');
				var inputText = window.sessionStorage.getItem('inputTags').split(',');
				window.sessionStorage.setItem('inputTags', inputText);
				var adjustFactor = 10;
				context.font = "22pt OpenSans";
				context.fillStyle = "white";
				context.textAlign = "center";
				var centerX = canvas.width / 2;
				var centerY = canvas.height / 2;



				//var angle = 45*Math.PI/180 // radians
				var angle = inputText.length / adjustFactor * 45 * Math.PI / 180;
				var radius = 220;
				for (var i = 0; i < 8; i++) {
					var position = i + 1;
					var angle = inputText[i].length / adjustFactor * 45 * Math.PI / 180
					//var adjustFactor=Math.round(inputText[i].length*3.33);
					console.log(adjustFactor);
					_this.drawTextAlongArc(context, inputText[i].toUpperCase(), centerX, centerY, radius, angle, position, adjustFactor);
				}
			} else {
				query.find({
					success: function(results) {
						App.vent.trigger('closePopups');
						var i = 0;
						while (i < results.length) {
							inputText.push(results[i].get('tag_name'));
							i++;
						}
						if (inputText) {
							window.sessionStorage.setItem('inputTags', inputText);
							var adjustFactor = 10;
							context.font = "24pt OpenSans";
							context.fillStyle = "silver";
							context.textAlign = "center";
							var centerX = canvas.width / 2;
							var centerY = canvas.height / 2;
							//var angle = 45*Math.PI/180 // radians
							var angle = inputText.length / adjustFactor * 45 * Math.PI / 180;
							var radius = 220;
							for (var i = 0; i < 8; i++) {
								var position = i + 1;
								var angle = inputText[i].length / adjustFactor * 45 * Math.PI / 180
								//var adjustFactor=Math.round(inputText[i].length*3.33);
								console.log(adjustFactor);
								_this.drawTextAlongArc(context, inputText[i].toUpperCase(), centerX, centerY, radius, angle, position, adjustFactor);

							}
							App.views.userHome.render();
						} else {
							App.vent.trigger('closePopups');
							console.log("unable to fetch data from parse");
						}
					},
					error: function(e) {}
				});
			}
		},
		showTaggedImages: function(e) {
			var center = new Array();
			var width;
			var tagId = 1;
			width = $('.circle').width();
			center[0] = $(window).width() / 2;
			center[1] = 268;
			var tan = (center[1] - e.pageY) / (e.pageX - center[0]);
			var inv_tan = ((e.pageX - center[0]) >= 0) ? (Math.atan(tan) * 180 / 3.14) : (180 + Math.atan(tan) * 180 / 3.14);
			console.log(tan + ";" + inv_tan);
			if ((inv_tan >= 0) && (inv_tan <= 45)) {
				console.log("TAB 2");
				tagId = 2;
			} else if ((inv_tan > 45) && (inv_tan <= 90)) {
				console.log("TAB 1");
				tagId = 1;
			} else if ((inv_tan > 91) && (inv_tan <= 135)) {
				tagId = 8;
				console.log("TAB 8");
			} else if ((inv_tan > 135) && (inv_tan <= 180)) {
				tagId = 7;
				console.log("TAB 7");
			} else if ((inv_tan > 181) && (inv_tan <= 225)) {
				tagId = 6;
				console.log("TAB 6");
			} else if ((inv_tan > 225) && (inv_tan <= 270)) {
				tagId = 5;
				console.log("TAB 5");
			} else if ((inv_tan > -90) && (inv_tan <= -45)) {
				tagId = 4;
				console.log("TAB 4");
			} else {
				tagId = 3;
				console.log("TAB 3");
			}
			var baseTagArray = window.sessionStorage.inputTags.split(',')
			var tagName = baseTagArray[tagId - 1].toLowerCase();
			console.log("_____________main Tags_________ "+tagName);
			var checkNetworkConnection = PG_checkNetwork();
			if (checkNetworkConnection.result) {
				App.sessionVars.mainTags=tagName;
				App.sessionVars.mainTagsFalse=false;
				App.routers.mainRouter.navigate('#dataList/' + tagName, {
					trigger: true
				});
			}else{
				App.controller.showPopup('alert', {
					"title" : App.settings.messageTitle.failureTitle,
					"message" : App.settings.messageSubject.connectionLoss
					});
			}
		}
	});
})();