;
(function() {
    app.pages.footer = Backbone.Marionette.Layout.extend({
        template: '#footerLayout',
        events: {},
        init: function() {
            console.log("footer LAYOUT INIT");
            this.updateFooter();
        },
        updateFooter: function() {
            console.log('updateFooter');
            var bodyId = $('body').attr('id');
            $(App.regions.footer.el).hide();
        },
        onCloseLayout: function() {
            console.log("LAYOUT CLOSE");
        }
    });
})();