;
(function() {
	app.pages.header = Backbone.Marionette.Layout.extend({
		template : '#headerLayout',
		events : {
			'click .menu_h' : 'showMenu',
			'click .search_icon_h' : 'showSearch',
			//'form submit':'showSearchData',
			'click .search_h' : 'showSearchData',
			'click .search_clear_h' : 'clearSearch',
			'keydown .search_text_h' : 'createSearchTags',
			'click .header_title_h' : 'showHomeScreen',
			'click .header_back_h' : 'goBack',
			'focusout .search_text_h':'handlingKeyBoard'

		},
		menuOpened : false,
		handlingKeyBoard:function(e){
			if (App.settings.isDeviceReady && cordova && cordova.plugins && cordova.plugins.Keyboard) {
				cordova.plugins.Keyboard.close();
			}
		},
		searchState : false,
		init : function() {
			console.log("header LAYOUT INIT");
			_.bindAll(this, 'showSearchData', 'clearSearch', 'updateHeader', 'createSearchTags');
			this.updateHeader();
		},
		showSearch : function() {
			console.log("search button clicked");
			this.$el.find('.header_title_h').hide();
			this.$el.find('.header_back_h').hide();
			$(App.regions.header.el).find('.menu_h').removeClass("i_menu");
			this.$el.find('.second_menu_h').hide();
			this.$el.find('.search_icon_h').hide();

			App.pages.header.$el.find('.search_clear_h').show();
			this.$el.find('.search_box_h').addClass('search_white_icon').show();
			this.$el.find('.search_text_h').focus();
			if (this.menuOpened) {
				this.showMenu();
			}
			this.searchState = true;

		},
		hideSearch : function() {
			console.log("Removed focus from search button");
			App.pages.header.$el.find('.header_title_h').show();
			App.pages.header.$el.find('.second_menu_h').show();
			App.pages.header.$el.find('.search_icon_h').show();
			App.pages.header.$el.find('.search_clear_h').hide();
			App.pages.header.$el.find('.search_box_h').addClass('search_white_icon').hide();
			this.searchState = false;
			this.updateHeader();

		},
		showMenu : function() {
			console.log("menu button clicked");
			this.hideSearch();
			if (!this.menuOpened) {
				$('#menuRegion').css('display', '');
				App.controller.showMenu();
				//				$(App.regions.header.$el).css({
				//				'position' : 'relative'
				//				});
				var maskHeight = App.settings.deviceHeight;
				if (navigator.userAgent.match(/(iPad|iPhone|iPod touch);.*CPU.*OS 7_\d/i)) {
					$('#menuRegion').css({
						'height' : (maskHeight - ($('#headerRegion').height() + 20))
					});
					$('#menuRegion').css({
						'top' : '68px'
					});
				} else {
					$('#menuRegion').css({
						'height' : (maskHeight - ($('#headerRegion').height()))
					});
				}
				this.menuOpened = true;
				var currentPage = App.sessionVars.currentPage;
				console.log('currentPage is ' + currentPage);
				switch (currentPage) {
					case "userHome":
						$('#menuRegion .m_home_h').addClass('selected');
						break;

					case "settings":
						$('#menuRegion .m_settings_h').addClass('selected');
						break;

					case "contactus":
						$('#menuRegion .m_contactus_h').addClass('selected');
						break;

					case "imageDetails":
						$('#menuRegion .m_upload_h').addClass('selected');
						break;

					case "imageZoom":
					case "dataList":
						window.onscroll = function() {
							window.scrollTo(0, 0);
						};
						break;
					
					default :
						$('#menuRegion .menu_item').removeClass('selected');
						window.onscroll = function() {
							window.scrollTo(0, 0);
						};
						$("#menuRegion").bind('touchmove', function(e) {

							e.preventDefault()
						});

						break;
				}
				//this.hideSearch();
			} else {
				$('#menuRegion').css('display', 'none');
				//				$(App.regions.body.$el).removeClass('body_transition');
				//				$(App.regions.header.$el).css({
				//				'position' : 'fixed'
				//				});
				window.onscroll = function() {
				};
				$("#menuRegion").unbind('touchmove');

				$(App.regions.menu.$el).hide();
				var maskHeight = App.settings.deviceHeight;
				$('#bodyHeightRegion').css({
					'height' : maskHeight
				});
				this.menuOpened = false;
			}
		},
		updateHeader : function() {
			var _this = this;
			var bodyId = $('body').attr('id');
			if (this.searchState) {
				_this.hideSearch();
			}
			console.log('--------------------'+bodyId);
			if (bodyId === "userLogin_Body") {
				$(App.regions.header.el).hide();

			} else if (bodyId === "signUp_Body") {
				$(App.regions.header.el).show();
				$(App.regions.header.el).find('.user_name').hide();
				$(App.regions.header.el).find('.header_title_h').show();
				$(App.regions.header.el).find('.header_title_h').text("SignUp");
				if ((navigator.userAgent.toLocaleLowerCase().indexOf('iphone') != -1) || (navigator.userAgent.toLocaleLowerCase().indexOf('ipod') != -1) || (navigator.userAgent.toLocaleLowerCase().indexOf('ipad') != -1)) {
					$(App.regions.header.el).find('.header_title_h').hide();
					$(App.regions.header.el).find('.header_back_h').show();
					$(App.regions.header.el).find('.menu_h').addClass("i_menu");

				}
				$(App.regions.header.el).find('.search_icon').hide();
				$(App.regions.header.el).find('.second_menu').hide();
				$(App.regions.header.el).find('.menu_h').hide();
				$(App.regions.header.el).find('.header_title_h').css('margin-left', '8px');
				$(App.regions.header.el).css("position", "fixed");
			}
				else if (bodyId === "terms_Body") {
				$(App.regions.header.el).show();
				$(App.regions.header.el).find('.user_name').hide();
				$(App.regions.header.el).find('.header_title_h').show();
				$(App.regions.header.el).find('.header_title_h').text("Terms and Conditions");
				if ((navigator.userAgent.toLocaleLowerCase().indexOf('iphone') != -1) || (navigator.userAgent.toLocaleLowerCase().indexOf('ipod') != -1) || (navigator.userAgent.toLocaleLowerCase().indexOf('ipad') != -1)) {
					$(App.regions.header.el).find('.header_title_h').hide();
					$(App.regions.header.el).find('.header_back_h').show();
					$(App.regions.header.el).find('.menu_h').addClass("i_menu");

				}
				$(App.regions.header.el).find('.search_icon').hide();
				$(App.regions.header.el).find('.second_menu').hide();
				$(App.regions.header.el).find('.menu_h').hide();
				$(App.regions.header.el).find('.header_title_h').css('margin-left', '8px');
				$(App.regions.header.el).css("position", "fixed");
			} 
			 else if (bodyId === "userHome_Body" || bodyId === "imageDetails_Body") {
				$(App.regions.header.el).show();
				$(App.regions.header.el).find('.user_name').show();
				if (App.models.user && App.models.user.get('attributes.name')) {
					$(App.regions.header.el).find('.user_name').text("Hi " + App.models.user.get('attributes.name'));
				} else {
					$(App.regions.header.el).find('.user_name').text("Hi");
				}

				$(App.regions.header.el).find('.header_title_h').show();
				$(App.regions.header.el).find('.header_title_h').text("ZING-LIFEDOCS");
				if ((navigator.userAgent.toLocaleLowerCase().indexOf('iphone') != -1) || (navigator.userAgent.toLocaleLowerCase().indexOf('ipod') != -1) || (navigator.userAgent.toLocaleLowerCase().indexOf('ipad') != -1)) {
					if (bodyId === "imageDetails_Body") {
						$(App.regions.header.el).find('.header_title_h').hide();
						$(App.regions.header.el).find('.header_back_h').show();
						$(App.regions.header.el).find('.menu_h').addClass("i_menu");
					} else {
						$(App.regions.header.el).find('.header_title_h').show();
						$(App.regions.header.el).find('.header_back_h').hide();
						$(App.regions.header.el).find('.menu_h').removeClass("i_menu");
					}

				}
				$(App.regions.header.el).find('.search_icon').show();
				$(App.regions.header.el).find('.second_menu').show();
				$(App.regions.header.el).find('.menu_h').show();
				$(App.regions.header.el).find('.header_title_h').css('margin-left', '-12px');
				$(App.regions.header.el).css("position", "fixed");
			} else if (bodyId === "forgotPwd_Body") {
				$(App.regions.header.el).show();
				$(App.regions.header.el).find('.user_name').hide();
				$(App.regions.header.el).find('.header_title_h').show();
				$(App.regions.header.el).find('.header_title_h').text("Forgot Password");
				$(App.regions.header.el).find('.header_title_h').css('margin-left', '8px');
				if ((navigator.userAgent.toLocaleLowerCase().indexOf('iphone') != -1) || (navigator.userAgent.toLocaleLowerCase().indexOf('ipod') != -1) || (navigator.userAgent.toLocaleLowerCase().indexOf('ipad') != -1)) {
					$(App.regions.header.el).find('.header_title_h').hide();
					$(App.regions.header.el).find('.header_back_h').show();
					$(App.regions.header.el).find('.menu_h').addClass("i_menu");
				}
				$(App.regions.header.el).find('.search_icon').hide();
				$(App.regions.header.el).find('.second_menu').hide();
				$(App.regions.header.el).find('.menu_h').hide();
				$(App.regions.header.el).css("position", "fixed");
			} else if (bodyId === "imageZoom_Body") {
				$(App.regions.header.el).show();
				$(App.regions.header.el).find('.user_name').show();
				if (App.models.user && App.models.user.get('attributes.name')) {
					$(App.regions.header.el).find('.user_name').text("Hi " + App.models.user.get('attributes.name'));
				} else {
					$(App.regions.header.el).find('.user_name').text("Hi");
				}
				$(App.regions.header.el).find('.header_title_h').show();
				$(App.regions.header.el).find('.header_title_h').text("ZING-LIFEDOCS");
				if ((navigator.userAgent.toLocaleLowerCase().indexOf('iphone') != -1) || (navigator.userAgent.toLocaleLowerCase().indexOf('ipod') != -1) || (navigator.userAgent.toLocaleLowerCase().indexOf('ipad') != -1)) {
					$(App.regions.header.el).find('.header_title_h').hide();
					$(App.regions.header.el).find('.header_back_h').show();
					$(App.regions.header.el).find('.menu_h').addClass("i_menu");
				}
				$(App.regions.header.el).find('.search_icon').show();
				$(App.regions.header.el).find('.second_menu').show();
				$(App.regions.header.el).find('.menu_h').show();
				$(App.regions.header.el).find('.header_title_h').css('margin-left', '-12px');
				$(App.regions.header.el).css("position", "absolute");
			} else if (bodyId === "dataList_Body") {
				$(App.regions.header.el).show();
				$(App.regions.header.el).find('.user_name').show();
				if (App.models.user && App.models.user.get('attributes.name')) {
					$(App.regions.header.el).find('.user_name').text("Hi " + App.models.user.get('attributes.name'));
				} else {
					$(App.regions.header.el).find('.user_name').text("Hi");
				}
				$(App.regions.header.el).find('.header_title_h').show();
				if ((navigator.userAgent.toLocaleLowerCase().indexOf('iphone') != -1) || (navigator.userAgent.toLocaleLowerCase().indexOf('ipod') != -1) || (navigator.userAgent.toLocaleLowerCase().indexOf('ipad') != -1)) {
					// $(App.regions.header.el).find('.header_title_h').hide();
					$(App.regions.header.el).find('.header_back_h').show();
					$(App.regions.header.el).find('.menu_h').addClass("i_menu");
				}
				$(App.regions.header.el).find('.menu_h').show();
				$(App.regions.header.el).find('.header_title_h').css('margin-left', '0px');
				//$(App.regions.header.el).find('.header_title_h').css('margin-left', '-12px');
				$(App.regions.header.el).css("position", "absolute");

			} else {
				$(App.regions.header.el).show();
				$(App.regions.header.el).find('.menu_h').show();
				$(App.regions.header.el).find('.header_title_h').css('margin-left', '-12px');
				$(App.regions.header.el).find('.header_title_h').text("ZING-LIFEDOCS");
				$(App.regions.header.el).css("position", "fixed");
				if ((navigator.userAgent.toLocaleLowerCase().indexOf('iphone') != -1) || (navigator.userAgent.toLocaleLowerCase().indexOf('ipod') != -1) || (navigator.userAgent.toLocaleLowerCase().indexOf('ipad') != -1)) {
					$(App.regions.header.el).find('.header_title_h').hide();
					$(App.regions.header.el).find('.header_back_h').show();
					$(App.regions.header.el).find('.menu_h').addClass("i_menu");
				}
				if(bodyId === "contactus_Body")
                {
                    $(App.regions.header.el).find('.header_title_h').text("About Us");
                }


			}
		},
		onCloseLayout : function() {
			console.log("LAYOUT CLOSE");
		},
		showSearchData : function() {

			var user_name = null;
			if (App.models.user && App.models.user.get("attributes.username")) {
				user_name = App.models.user.get("attributes.username");
			} else {
				App.routers.mainRouter.navigate('#userLogin/logout', {
					trigger : true
				});
			}

			var _this = this;
			//var newString = this.$el.find('.search_text_h').val().replace(/\s+/g, ' ').trim().toLowerCase();
			var newString = this.$el.find('.search_text_h').val().replace(/\s+/g, ' ').trim();
			if (!newString) {
				var options = {
					"title" : App.settings.messageTitle.failureTitle,
					"message" : "Please provide valid tags to search."
				};
				App.controller.showPopup('alert', options);
				return false;
			}

			var tags = newString.split(' ');
			App.sessionVars.tagName = tags;
			if (tags[0] && tags.length > 0) {
				var searchString = new Array();
				var searchTable = Parse.Object.extend('user_uploaded_images');

				for ( i = 0; i < tags.length; i++) {
					console.log(tags[i]);
					searchString[i] = new Parse.Query(searchTable);
					searchString[i].matches('DocumentsTags', tags[i]);
					searchString[i].equalTo("username", user_name);

				}
				for ( i = 0; i < tags.length; i++) {
					console.log(tags[i]);
					searchString[searchString.length] = new Parse.Query(searchTable);
					searchString[searchString.length - 1].matches('DocumentName', tags[i]);
					searchString[searchString.length - 1].equalTo("username", user_name);

				}

				var checkNetworkConnection = PG_checkNetwork();
				if (checkNetworkConnection.result) {
					var MainQueryParse = Parse.Query.or.apply(Parse.Query, searchString);
					App.controller.showPopup('loader');
					MainQueryParse.find({
						success : function(result) {
							App.vent.trigger("closePopups");
							if (result.length > 0) {
								console.log(JSON.stringify(result))
								App.sessionVars.searchResult = new Array();
								var i = 0;
								while (i < result.length) {
									App.sessionVars.searchResult.push(result[i].attributes);
									App.sessionVars.searchResult[i].objectId = result[i].id;
									App.sessionVars.searchResult[i].noResult = "";
									i++;
								}
								_this.hideSearch();
								App.sessionVars.listType = 'search';


								if (App.sessionVars.currentPage == "dataList") {
									App.collections.tagsList.reset(App.sessionVars.searchResult);
								} else {
									App.routers.mainRouter.navigate('#dataList/' + "searchData", {
										trigger : true
									});
								}

							} else {
								var options = {
									"title" : App.settings.messageTitle.failureTitle,
									"message" : "No Document Found"
								};
								App.controller.showPopup('alert', options);
							}

						},
						fail : function(error) {
							App.vent.trigger("closePopups");
							console.log(JSON.stringify(error))
						}
					});
				} else {
					App.controller.showPopup('alert', {
						"title" : App.settings.messageTitle.failureTitle,
						"message" : App.settings.messageSubject.connectionLoss
					});
				}
			}
		},
		clearSearch : function() {
			var _this = this;
			var searchBoxValue = this.$el.find('.search_text_h').val().trim();
			if (searchBoxValue) {
				this.$el.find('.search_text_h').val("");
				this.$el.find('.search_text_h').focus();
			} else {
				_this.hideSearch();
			}
		},
		existingString : {

		},
		createSearchTags : function(e) {
			var _this = this;
			arrow = {
				space : 32,
				enter : 13
			};
			switch (e.which) {
				case arrow.space:
					var newTagString = e.currentTarget.value.replace(/\s+/g, ' ').trim();
					if (_this.existingString.tagsText) {
						if (_this.existingString.tagsText != newTagString) {
							console.log(newTagString);
							_this.existingString.tagsText = newTagString;
						}
					} else {
						_this.existingString.tagsText = newTagString;
						console.log(newTagString);
					}
					break;
				case arrow.enter:
					$(e.currentTarget).blur();
					this.showSearchData();
					this.handlingKeyBoard();
					break;
			}
		},
		showHomeScreen : function() {
			var bodyId = $('body').attr('id');
			if (!((bodyId === "userLogin_Body") || (bodyId === "forgotPwd_Body") || (bodyId === "terms_Body") || (bodyId === "signUp_Body"))) {

				if (App.sessionVars.currentPage == "imageDetails") {
					App.controller.backButtonHandler();
				} else if (App.sessionVars.currentPage == "dataList") {
					return false;
				} else {
					App.routers.mainRouter.navigate('#userHome', {
						trigger : true
					});
				}
			}
		},
		goBack : function() {
			App.controller.backButtonHandler();
		},
		onRender : function() {
			if (navigator.userAgent.match(/(iPad|iPhone|iPod touch);.*CPU.*OS 7_\d/i)) {
				$(App.regions.header.el).css({
					'padding-top' : '20px',
					'height' : '68px'
				});

			}
		}
	});
})();
