window.app = window.app || {};
app.views = app.views || {};
app.views.shareDocument = Backbone.Marionette.ItemView.extend({
	template : '#shareDocument',
	events : {
		'click .close_h' : 'closePopup',
		'click .share_h' : 'shareOptions',
		//'click #url ' : 'hideShowExpiry',
		'click #attachment ' : 'hideShowExpiry',
		// 'click #whatsapp_share ' : 'shareOptions'
		'focusout #expiry' : 'handlingKeyBoard',
		'focusout #attachment' : 'handlingKeyBoard',
		'focusout #url' : 'handlingKeyBoard',

	},
	initialize : function() {
	},
	handlingKeyBoard : function(e) {
		if (App.settings.isDeviceReady && cordova && cordova.plugins && cordova.plugins.Keyboard) {
				cordova.plugins.Keyboard.close();
			}
	},
	onRender : function() {
		var user = App.models.user.get('attributes.name') ? App.models.user.get('attributes.name') : "Anonymous";
		//var text = encodeURI('whatsapp://send?text=' + user.toUpperCase() + ' has shared a document with you using Zing-Lifedocs. You can access this document on this link: ' + App.sessionVars.imageZoomOptions.pdfUrl + ' . Zing-Lifedocs mobile is a secure and easy way to save important documents in your life and share them. Click to download - iOS, Android, Windows 8.');
		//this.$el.find('#whatsapp_share').attr('href', text);

		// hide for ios
		if ((navigator.userAgent.toLocaleLowerCase().indexOf('ipod') != -1) || (navigator.userAgent.toLocaleLowerCase().indexOf('ipad') != -1)) {
			this.$el.find('#whatsapp_share').hide();
		}
	},
	closePopup : function() {
		App.vent.trigger("closePopups");
		// App.routers.mainRouter.navigate('#userHome', {
		// 	trigger : true
		// });
	},
	onClose : function() {

	},
	hideShowExpiry : function(e) {

		$("input:checkbox").click(function() {
			if ($(this).is(":checked")) {
				var group = "input:checkbox[name='" + $(this).attr("name") + "']";
				$(group).prop("checked", false);
				$(this).prop("checked", true);
			} else {
				$(this).prop("checked", false);
			}
		});

		if ($('#url').is(':checked') || !($('#attachment').is(':checked'))) {
			$('.chosse_options').show();
			$('#whatsapp_share').show();
			//$('#whatsapp_share').hide();
			// hide for ios
			if ((navigator.userAgent.toLocaleLowerCase().indexOf('ipod') != -1) || (navigator.userAgent.toLocaleLowerCase().indexOf('ipad') != -1)) {
				this.$el.find('#whatsapp_share').hide();
			}

		} else {
			$('.chosse_options').hide();
			$('#whatsapp_share').hide();
		}
	},
	shareOptions : function(e) {
		// Share via url
		if ($('#url').is(':checked')) {
			if (!($.isNumeric($('#expiry').val())) || ($('#expiry').val() == 0)) {
				var options = {
					"title" : App.settings.messageTitle.failureTitle,
					"message" : App.settings.messages.inValidExpiry
				};
				App.controller.showPopup('expiryAlert', options);
				return false;
			}
			App.sessionVars.shareType = e.currentTarget.getAttribute('id');
			App.sessionVars.expiryDays = $('#expiry').val();
			App.vent.trigger("closePopups");
			App.views.imageZoom.shareUrl();
			App.sessionVars.shareVia = "email";
		} else if ($('#attachment').is(':checked')) {
			// Share via attachment
			App.sessionVars.shareType = e.currentTarget.getAttribute('id');
			App.sessionVars.expiryDays = 5;
			App.vent.trigger("closePopups");
			App.views.imageZoom.shareUrl();
			App.sessionVars.shareVia = "attachment";
		} else {
			var options = {
				"title" : App.settings.messageTitle.failureTitle,
				"message" : App.settings.messages.selectOption
			};
			App.controller.showPopup('expiryAlert', options);
			return false;
		}

	}
});
