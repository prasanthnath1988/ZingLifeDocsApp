;
(function() {
    app.pages.signUp = Backbone.Marionette.Layout.extend({
        template : '#signUpLayout',
        regions : {
            content : ".content_wrapper_h"
        },
        init : function() {
            console.log("signUp LAYOUT INIT");
            App.views.signUp = new app.views.signUp;
            this.content.show(App.views.signUp);
        },
        onCloseLayout : function() {
            console.log("LAYOUT CLOSE");
        }
    });
})();
