/**
 * Network helper.
 * Provdes information about whether the device is connected to a network connetion or not
 * call PG_checkNetwork()
 * return result object:{reult:true/false,state_info:{}}
 *
 */
var PG_checkNetwork = function() {
	if (((navigator.userAgent.toLocaleLowerCase().indexOf('android') != -1) || (navigator.userAgent.toLocaleLowerCase().indexOf('iphone') != -1) || (navigator.userAgent.toLocaleLowerCase().indexOf('ipod') != -1) || (navigator.userAgent.toLocaleLowerCase().indexOf('ipad') != -1))) {
		if (App.settings.isDeviceReady) {
			var networkState = navigator.network.connection.type;
			var isConnected;
			// value returned to signify internet connection.
			var states = {};
			states[Connection.UNKNOWN] = 'Unknown connection';
			states[Connection.ETHERNET] = 'Ethernet connection';
			states[Connection.WIFI] = 'WiFi connection';
			states[Connection.CELL_2G] = 'Cell 2G connection';
			states[Connection.CELL_3G] = 'Cell 3G connection';
			states[Connection.CELL_4G] = 'Cell 4G connection';
			states[Connection.NONE] = 'No network connection';
			console.log("Network State is: " + states[networkState]);
			if (states[networkState] == states[Connection.NONE] || states[networkState] == states[Connection.UNKNOWN]) {
				isConnected = false;
			} else {
				isConnected = true;
			}
			var result = {
					"result": isConnected,
					"state_info": states[networkState]
			}
			return result;
		} else {
			console.log("Network Helper LOG: Deivce is not ready yet, can't check state yet");
			return {
				"result": false
			}
		}
	}else{
		
		return {
            "result": true
        }
	}
}