window.app = window.app || {};
app.views = app.views || {};
app.views.emailFacebook = Backbone.Marionette.ItemView.extend({
	template: '#emailFacebook',
	events: {
		'click .close_h': 'closePopup',
		'click #fb_submit': 'fbEmailSubmit'
	},
	initialize: function() {
		
	},
	onRender: function() {
		
	},
	closePopup: function() {
		App.vent.trigger("closePopups");
		// App.routers.mainRouter.navigate('#userHome', {
		// 	trigger : true
		// });
	},
	fbEmailSubmit: function() {
		var uFbEmail = $("#facebookEmail").val();
		var emailPattern = /^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$/;

		if(!emailPattern.exec(uFbEmail)){
			var options = {
					"title":App.settings.messageTitle.failureTitle,
					"message":App.settings.messages.inValidEmailId
			}
			App.controller.showPopup('fbAlert',options);
			return false;
		}
		App.sessionVars.fbEmail = uFbEmail;
		App.vent.trigger("closePopups");
		$('.fb_login_h').trigger('click');
	}
});