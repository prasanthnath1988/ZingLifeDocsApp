window.app = window.app || {};
app.views = app.views || {};
app.views.expiryAlert = Backbone.Marionette.ItemView.extend({
    template: '#ExpiryAlertView',
    events: {
        'click .close_h': 'closePopup'
    },
    initialize: function() {
        App.vent.trigger("closePopups");
    },
    onRender: function() {

        var _this = this;
        if (_this.options.title) {
            _this.$el.find('.title_h').html(_this.options.title);
        }
        if (_this.options.subject) {
            _this.$el.find('.subject_h').html(_this.options.subject);
        }
        if (_this.options.message) {
            _this.$el.find('.message_h').html(_this.options.message);
        }

    
    },
    closePopup: function() {
    	var _this = this;
    	if(_this.options.callBack){
    		App.vent.trigger("closePopups");
    		_this.options.callBack();
    	}
        App.vent.trigger("closePopups");
        App.controller.showPopup('shareDocument');      
    },
    onClose: function() {
    	
    }
});