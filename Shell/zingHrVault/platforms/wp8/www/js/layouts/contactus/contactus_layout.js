;
(function() {
	app.pages.contactus = Backbone.Marionette.Layout.extend({
		template : '#contactusLayout',
		events : {
			'click .contact_prasad_h' : 'contactUs',
			'click .contact_support_h' : 'contactUs'
		},
		contactUs : function(e) {

			//debugger;
			var contactEmail = e.currentTarget.getAttribute('contactEmail');
			if (App.settings.isDeviceReady) {
				var checkNetworkConnection = PG_checkNetwork();
				if (checkNetworkConnection.result) {
					window.plugin.email.open({
						to : [contactEmail],
						//cc:      ['erika.mustermann@appplant.de'],
						//bcc:     ['john.doe@appplant.com', 'jane.doe@appplant.com'],
						//subject : 'Feedback from' + userEmail,
						body : '',
						isHtml : true
						//attachments : ['www://imgs/fb.pdf']
					});
				}
			}

		},
		regions : {
			content : ".content_wrapper_h"
		},
		init : function() {
			console.log("CONTACT US LAYOUT INIT");
		},
		onCloseLayout : function() {
			console.log("CONTACT US LAYOUT CLOSE");
		}
	});
})();
