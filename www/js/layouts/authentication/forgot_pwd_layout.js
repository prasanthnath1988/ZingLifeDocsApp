;
(function() {
	app.pages.forgotPwd = Backbone.Marionette.Layout.extend({
		template : '#forgotPwdLayout',
		events : {
			'click .submit_h' : 'submitPassword'
		},
		init : function() {
			console.log("forgot password LAYOUT INIT");

		},
		submitPassword : function() {
			console.log("submit pwd clicked");

			var emailAddress = this.$el.find('#emailIdForPassword').val();
			var emailPattern = /^\w+@[a-zA-Z_]+?\.[a-zA-Z]{2,3}$/;
			if (!emailPattern.exec(emailAddress)) {
				var options = {
						"title" : App.settings.messageTitle.failureTitle,
						"message" : App.settings.messages.inValidEmailId
				}
				App.controller.showPopup('alert', options);

				return false;
			}

			var checkNetworkConnection = PG_checkNetwork();
			if (checkNetworkConnection.result) {

				App.controller.showPopup('loader');
				Parse.User.requestPasswordReset(emailAddress, {
					success: function() {
						App.vent.trigger("closePopups");
						// Password reset request was sent successfully
						var options = {
								"title" : App.settings.messageTitle.resetPassword,
								"message" : App.settings.messageSubject.resetPasswordLink,
								"callBack":function(){
									App.routers.mainRouter.navigate('#userLogin/logout', {
										trigger : true
									})
								}
						}
						App.controller.showPopup('alert', options);



					},
					error: function(error) {
						App.vent.trigger("closePopups");
						// Show the error message somewhere
						console.log("Error: " + error.code + " " + error.message);
						var options = {
								"title" : App.settings.messageTitle.failureTitle,
								"message" : error.message
						}
						App.controller.showPopup('alert', options);
					}
				});

			}else{
				App.controller.showPopup('alert', {
					"title" : App.settings.messageTitle.failureTitle,
					"message" : App.settings.messageSubject.connectionLoss
				});
			}

		},
		onCloseLayout : function() {
			App.vent.trigger("closePopups");
			console.log("LAYOUT CLOSE");
		}
	});
})();
