 ;
 (function() {
     app.pages.howtouse = Backbone.Marionette.Layout.extend({
         template: '#howtouseLayout',
         init: function() {
             console.log("HOW TO USE LAYOUT INIT");
         },
         events: {
             'click .how_to_use_h': 'showDashboard'
         },
         onRender: function() {
             $(App.regions.header.el).hide();
             $('#bodyHeightRegion').css('height', '100%');
             setTimeout(function() {
                 $('#bodyRegion').css('padding-top', '0px');
                 $('#mySwipe').css('height', $(window).height() + 'px');
                 $('.swipe-wrap > div').css('height', $(window).height() + 'px');
             }, 10);
             // pure JS
             var prev = 0;
             var a = this.$el.find(".swipe-wrap")[0].children.length;
             for (var i = 0; i < a; i++) {
                 var iDiv = document.createElement('div');
                 iDiv.className = 'Circle';
                 this.$el.find('.slider-circles')[0].appendChild(iDiv);
             }
             this.$el.find('.Circle')[0].className += ' selected';
             var elem = this.$el.find('#mySwipe');
             setTimeout(function() {
                 window.mySwipe = Swipe(elem[0], {
                     startSlide: 0,
                     auto: 0,
                     continuous: false,
                     callback: function(index, element) {
                         if (prev != undefined) {
                             $('.Circle')[prev].className = 'Circle';
                         }
                         $('.Circle')[index].className = $('.Circle')[index].className + ' selected ';
                         prev = index;
                     },
                 });
             }, 10);
         },
         showDashboard: function() {
             App.pages.header.showMenu();
             App.routers.mainRouter.navigate('#userHome', {
                 trigger: true
             });
             $('.menu_h').trigger('click');
         },
         onCloseLayout: function() {
             console.log("HOW TO USE LAYOUT CLOSE");
         }
     });
 })();