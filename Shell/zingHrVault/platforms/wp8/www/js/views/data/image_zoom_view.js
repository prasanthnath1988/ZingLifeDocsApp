;(function() {
	app.views.imageZoom = Backbone.Marionette.ItemView.extend({
		template : '#imageZoomView',
		events : {

			"click .edit_document_h" : "showDocumentEditScreen",
			"click .share_document_h" : "showSharePopup",
			"click .open_pdf_h" : "openPdf",
			"click .delete_doc_h" : "deleteDoc"

		},
		initialize : function() {

			_.bindAll(this, 'onRender', 'showSharePopup');

		},
		onRender : function() {
			var _this = this;
			this.$el.find('.imagesIds').eq(0).attr('src', "");
			this.options.imageUrl ? this.$el.find('.doc_img').children("img").attr('src', this.options.imageUrl) : this.$el.find('.imagesIds').eq(0).attr('src', App.sessionVars.imageZoomOptions.imageUrl);
			this.options.pdfUrl ? this.$el.find('.doc_img').children("img").attr('pdfUrl', this.options.pdfUrl) : this.$el.find('.imagesIds').eq(0).attr('pdfUrl', App.sessionVars.imageZoomOptions.pdfUrl);
			this.$el.find('.image_name_h').html(App.sessionVars.imageZoomOptions.imageName);

			var tagsNewString = App.sessionVars.imageZoomOptions.imageTags.replace(/\s+/g, ' ').trim().toLowerCase();
			var tagsArray = tagsNewString.split(' ');
			this.$el.find('.image_tags_h').html('');
			for (var i = 0; i < tagsArray.length; i++) {
				this.$el.find('.image_tags_h').append('<span class="tag">' + tagsArray[i] + '</span>');
			}
			setTimeout(function() {
				if (_this.imageZoomView) {
					_this.imageZoomView.refresh();
				} else {
					_this.imageZoomView = new iScroll("imageToUpload", {
						hideScrollbar : true,
						zoom : true,
						// So Swiper will not swipe/slide when zooming is enabled
						//					      onZoomEnd: function(e) {
						//					        var slide = $(this.wrapper);
						//
						//					        if(parseInt(this.scale) == 1) {
						//					          slide.removeClass('swiper-no-swiping');
						//					        } else {
						//					          slide.addClass('swiper-no-swiping');
						//					        }
						//					      }
					});
					_this.imageZoomView.refresh();
				}

			}, 200);

		},
		onClose : function() {
			console.log("VIEW CLOSE");
		},
		showSharePopup : function() {
			App.controller.showPopup('shareDocument');
		},
		shareUrl : function() {
			var _this = this;
			var userName = App.models.user.get("attributes.username");
			var expiryDays = App.sessionVars.expiryDays;
			if (App.models.user && App.models.user.get("attributes.linkExpiryDays")) {
				expiryDays = App.models.user.get("attributes.linkExpiryDays");
			}
			var checkNetworkConnection = PG_checkNetwork();
			if (checkNetworkConnection.result) {
				_this.createShareURL(App.sessionVars.imageZoomOptions.pdfUrl, userName, expiryDays);
				//_this.storeOffline(App.sessionVars.imageZoomOptions.pdfUrl, userName, expiryDays); // RVG RND
			} else {
				App.controller.showPopup('alert', {
					"title" : App.settings.messageTitle.failureTitle,
					"message" : App.settings.messageSubject.connectionLoss
				});
			}
		},
		//RVG RND STart
		storeOffline : function(url, username, date){
			window.requestFileSystem = window.requestFileSystem || window.webkitRequestFileSystem;
			window.requestFileSystem(LocalFileSystem.PERSISTENT, 0, function(fileSystem){
				alert(fileSystem.root.toURL());
				fileTransfer.download(encodeURI("http://www.hallaminternet.com/assets/URL-tagging-image.png"), fileSystem.root.toURL() + App.sessionVars.imageZoomOptions.imageName + '.pdf', function(entry) {
				alert("Download Complete");
				console.log("download complete: " + entry.toURL());
				App.vent.trigger("closePopups");
				window.plugin.email.isServiceAvailable(function(isAvailable) {
						window.plugin.email.open({
						to : [],
						//cc:      ['erika.mustermann@appplant.de'],
						//bcc:     ['john.doe@appplant.com', 'jane.doe@appplant.com'],
						subject : user.toUpperCase() + ' has shared a document -' + App.sessionVars.imageZoomOptions.imageName,
						body : user.toUpperCase() + ' has shared a document -' + App.sessionVars.imageZoomOptions.imageName + ' with you using Zing-Lifedocs.You can access this document on this link:-' + data.url + ' .Zing-Lifedocs mobile is a secure and easy way to save important documents in your life and share them. Click to download - iOS, Android, Windows 8. ',
						isHtml : true,
						attachments : [entry.toURL()]
					});
				});
			}, function(error) {
							alert(JSON.stringify(error));
							alert("error in download");
								console.log("download error source " + error.source);
								console.log("download error target " + error.target);
								console.log("upload error code" + error.code);
							});

						}, function(error){
							alert("Error in file system");
							console.log(error);
						});
		},
		//RVG RND End
		openPdf : function(e) {
			console.log(e.currentTarget);
			var imageUrl = $(e.currentTarget).attr('pdfUrl');

			if ((navigator.userAgent.toLocaleLowerCase().indexOf('iphone') != -1) || (navigator.userAgent.toLocaleLowerCase().indexOf('ipod') != -1) || (navigator.userAgent.toLocaleLowerCase().indexOf('ipad') != -1)) {
				window.open(imageUrl, '_blank', 'location=yes,closebuttoncaption=Close,enableViewportScale=yes');
			} else {
				window.open(imageUrl, '_system');
			}
		},
		showDocumentEditScreen : function() {
			App.routers.mainRouter.navigate('#imageDetails/editDocs', {
				trigger : true
			});

		},
		createShareURL : function(imageUrl, userName, expiryDays) {
			var paramasData = {
				"userid" : userName,
				"image_url" : imageUrl,
				"expires_in" : expiryDays
			};

			App.controller.showPopup('loader');
			App.communication.GET({
				data : paramasData,
				url : App.settings.urls.createUrl,
				successCallback : function(data) {
					console.log('success');
					console.log(data);
					// setting false
					if ((App.sessionVars.shareType == "whatsapp_share") && data && data.url) {
						App.vent.trigger("closePopups");
						var user = App.models.user.get('attributes.name') ? App.models.user.get('attributes.name') : "Anonymous";
						//$('#whatsapp_share').attr('href','whatsapp://send?text='+user.toUpperCase() + ' has shared a document -'+App.sessionVars.imageZoomOptions.imageName+' with you using Zing-Lifedocs. <br/><br/> You can access this document on this link:- <a href="http://'+data.url+'" style="border:none;color:#0084b4;text-decoration:none" target="_blank">Click Here</a> <br/><br/> Zing-Lifedocs mobile is a secure and easy way to save important documents in your life and share them. Click to download - iOS, Android, Windows 8.');
						//window.location.href = 'whatsapp://send?text='+user.toUpperCase() + ' has shared a document -'+App.sessionVars.imageZoomOptions.imageName+' with you using Zing-Lifedocs. <br/><br/> You can access this document on this link:- <a href="http://'+data.url+'" style="border:none;color:#0084b4;text-decoration:none" target="_blank">Click Here</a> <br/><br/> Zing-Lifedocs mobile is a secure and easy way to save important documents in your life and share them. Click to download - iOS, Android, Windows 8. ';
						//$('#whatsapp_share').trigger('click');

						if (1 || (navigator.userAgent.toLocaleLowerCase().indexOf('iphone') != -1) || (navigator.userAgent.toLocaleLowerCase().indexOf('ipod') != -1) || (navigator.userAgent.toLocaleLowerCase().indexOf('ipad') != -1)) {
							$('#whatsapp_share').attr('href', 'whatsapp://send?text=' + user.toUpperCase() + ' has shared a document -' + App.sessionVars.imageZoomOptions.imageName + ' with you using Zing-Lifedocs. <br/><br/> You can access this document on this link:- ' + data.url + ' Zing-Lifedocs mobile is a secure and easy way to save important documents in your life and share them. Click to download - iOS, Android, Windows 8.');
							$('#whatsapp_share').trigger('click');
						} else {
							window.open('whatsapp://send?text=' + user.toUpperCase() + ' has shared a document -' + App.sessionVars.imageZoomOptions.imageName + ' with you using Zing-Lifedocs.  You can access this document on this link:- ' + data.url + ' Zing-Lifedocs mobile is a secure and easy way to save important documents in your life and share them. Click to download - iOS, Android, Windows 8.', '_system');
						}

					} else if (data && data.url) {
						App.vent.trigger("closePopups");
						var user = App.models.user.get('attributes.name') ? App.models.user.get('attributes.name') : "Anonymous";
						if (App.sessionVars.shareVia == "email") {
							window.plugin.email.isServiceAvailable(function(isAvailable) {
								window.plugin.email.open({
									to : [],
									//cc:      ['erika.mustermann@appplant.de'],
									//bcc:     ['john.doe@appplant.com', 'jane.doe@appplant.com'],
									subject : user.toUpperCase() + ' has shared a document -' + App.sessionVars.imageZoomOptions.imageName,
									body : user.toUpperCase() + ' has shared a document -' + App.sessionVars.imageZoomOptions.imageName + ' with you using Zing-Lifedocs.You can access this document on this link:-' + data.url + ' .Zing-Lifedocs mobile is a secure and easy way to save important documents in your life and share them. Click to download - iOS, Android, Windows 8. ',
									isHtml : true
									//attachments : ['www://imgs/fb.pdf']
								});

							});
						} else {

							var fileTransfer = new FileTransfer();
							console.log(encodeURI(App.sessionVars.imageZoomOptions.pdfUrl));
							alert(encodeURI(App.sessionVars.imageZoomOptions.pdfUrl));
							fileTransfer.download(encodeURI(App.sessionVars.imageZoomOptions.pdfUrl), 'cdvfile://localhost/persistent/path/to/downloads/zingLifeDocs/' + App.sessionVars.imageZoomOptions.imageName + '.pdf', function(entry) {
								alert("Download Complete");
								console.log("download complete: " + entry.toURL());
								App.vent.trigger("closePopups");
								window.plugin.email.isServiceAvailable(function(isAvailable) {
									window.plugin.email.open({
										to : [],
										//cc:      ['erika.mustermann@appplant.de'],
										//bcc:     ['john.doe@appplant.com', 'jane.doe@appplant.com'],
										subject : user.toUpperCase() + ' has shared a document -' + App.sessionVars.imageZoomOptions.imageName,
										body : user.toUpperCase() + ' has shared a document -' + App.sessionVars.imageZoomOptions.imageName + ' with you using Zing-Lifedocs.You can access this document on this link:-' + data.url + ' .Zing-Lifedocs mobile is a secure and easy way to save important documents in your life and share them. Click to download - iOS, Android, Windows 8. ',
										isHtml : true,
										attachments : [entry.toURL()]
									});

								});
							}, function(error) {
							alert("error in download");
								console.log("download error source " + error.source);
								console.log("download error target " + error.target);
								console.log("upload error code" + error.code);
							});
						}

					} else {
						return "error";
					}
					
				},
				errorCallback : function(options) {
					if (options.textStatus == "timeout") {
						App.controller.showPopup('alert', {
							"title" : App.settings.messageTitle.failureTitle,
							"message" : App.settings.messages.timeOutMessage
						});
					}else{
						App.vent.trigger("closePopups");
					}
					
					return "error";

				}
			});
		},
		deleteDoc : function(){
			var deletedImage = Parse.Object.extend("user_uploaded_images");
			var query = new Parse.Query(deletedImage);
            query.equalTo("objectId", App.sessionVars.imageZoomOptions.objectId);
			query.find({
			  success: function(myObj) {
			    // The object was retrieved successfully.
			    console.log(myObj);
                myObj[0].destroy();
                App.routers.mainRouter.navigate('#dataList/'+App.sessionVars.mainTags, {
					trigger : true
				});
			  },
			  error: function(object, error) {
                  console.log(error);
					App.controller.showPopup('alert', {
					"title" : App.settings.messageTitle.failureTitle,
					"message" : App.settings.messageSubject.connectionLoss
				});                  		
				// The object was not retrieved successfully.
			    // error is a Parse.Error with an error code and description.
			  }
			});
		}
	});
})();
