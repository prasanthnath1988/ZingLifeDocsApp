/**
 * Name    : regions
 * Purpose : Regions used in the app.
 * Params  : --
 * Returns : --
 **/
window.app = window.app || {};
app.regions = app.regions || {};
app.regions.header = Backbone.Marionette.Region.extend({
    el : '#headerRegion',
    permanent : true
});
app.regions.body = Backbone.Marionette.Region.extend({
    el : '#bodyRegion',
    permanent : false
});
app.regions.popup = Backbone.Marionette.Region.extend({
    el : '#popupRegion',
    permanent : false
});
app.regions.footer = Backbone.Marionette.Region.extend({
    el : '#footerRegion',
    permanent : true
});
app.regions.menu = Backbone.Marionette.Region.extend({
    el : '#menuRegion',
    permanent : true
}); 