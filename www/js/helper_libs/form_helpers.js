(function() {
    $.fn.wordHighlight = function(textbox, jsonStr) {
        var _this = this;
        var txt = textbox;
        var content = '';
        var flag;
        var arr = txt.split(" ");
        for (i = 0; i < arr.length; i++) {
            flag = 0;
            _.each(jsonStr.corrections, function(val, key) {
                if (arr[i] == key) {
                    content += '<span class="incorrect_word_h ' + key + '" style="background: #ffe58f;color: #666666;padding: 2px 5px;border-radius: 1px;border-radius: 2px;font: italic 15px Helvetica, Arial, sans-serif;">' + arr[i] + '</span> ';
                    flag = 1;
                }
            });
            if (flag == 0) {
                content += arr[i] + ' ';
            }
        }
        _this.html(content);
    };
    //phonenumber formater
    App.formatPhone = function(phonenum) {
        var regexObj = /^(?:\+?1[-. ]?)?(?:\(?([0-9]{3})\)?[-. ]?)?([0-9]{3})[-. ]?([0-9]{4})$/;
        if (regexObj.test(phonenum)) {
            var parts = phonenum.match(regexObj);
            var phone = "";
            if (parts[1]) {
                phone += "(" + parts[1] + ") ";
            }
            phone += parts[2] + "-" + parts[3];
            return phone;
        } else {
            //invalid phone number
            return phonenum;
        }
    }
    App.actionsToLoading = function($_actions) {
        var $_loading_msg = $_actions.next('p.loading_msg');
        if (!$_loading_msg.length) $_loading_msg = $('<p class="loading_msg"><span class="msg">Submitting...</span></p>').insertAfter($_actions);
        $_actions.hide();
        $_loading_msg.show();
    }
    App.loadingComplete = function($_actions) {
        var $_loading_msg = $_actions.next('p.loading_msg');
        $_loading_msg.hide();
        $_actions.show();
    }
    var updateForm = function($_form) {
        $_form.bind('submit', function(e) {
            var $_this = $(this);
            $_this.find('input, select, textarea').trigger('validate');
            var $_error_fields = $_this.find('p.field.error');
            if ($_error_fields.length) {
                $_error_fields.first().find('input, select, textarea').first().focus();
                return;
            }
            this.$_actions = $_this.find('p.actions');
            App.actionsToLoading(this.$_actions);
        });
        $_form.bind('form:error form:submit_complete form:saved', function() {
            if (this.$_actions && this.$_actions.length) App.loadingComplete(this.$_actions);
        });
    }
    var updatePlaceholders = function($_view) {
        var $$_placeholders = $_view.find('[placeholder]');
        _.each($$_placeholders, function(placeholder) {
            var $_placeholder = $(placeholder);
            placeholder.setStyle = function(focus) {
                if (focus || (placeholder.value != $_placeholder.attr('placeholder') && placeholder.value != '')) {
                    $_placeholder.removeClass('placeholder');
                } else {
                    $_placeholder.addClass('placeholder');
                }
            }
            placeholder.setStyle();
            $_placeholder.bind('focus', function() {
                this.setStyle(true);
            });
            $_placeholder.bind('blur change', function() {
                this.setStyle();
            });
        });
    }
    var updateCheckboxes = function($_form) {
        var $$_checkboxes = $_form.find('input[type=checkbox]');
        _.each($$_checkboxes, function(checkbox) {
            var $_checkbox = $(checkbox);
            var label = $_checkbox.parent('label');
            // var ignore = $_checkbox.attr('class');
            if (!$_checkbox.hasClass('ignore')) {
                if (label.length) {
                    $(label).addClass('checkbox');
                } else {
                    $_checkbox.wrap('<label class="checkbox"/>');
                }
                $('<span />').insertAfter($_checkbox);
            }
            if ($.browser.msie && $.browser.version < 9) {
                checkbox.set_checked = function() {
                    if (this.checked) {
                        $_form.find('input[name="' + this.name + '"] + span').removeClass('checked');
                        $(this).next('span').addClass('checked').blur();
                    }
                };
                checkbox.set_checked();
                $_checkbox.bind('change', function() {
                    this.set_checked();
                });
            }
            if (App.session.device.iOS) {
                var $_label = $_checkbox.parent('label.radio');
                $_label.click(function() {
                    $_checkbox.focus();
                });
            }
            if (checkbox.value == 'on') checkbox.value = true;
        });
    }
    var updateRadios = function($_form) {
        var $$_radios = $_form.find('input[type=radio]');
        _.each($$_radios, function(radio) {
            var $_radio = $(radio);
            var label = $_radio.parent('label');
            if (label.length) {
                $(label).addClass('radio');
            } else {
                $_radio.wrap('<label class="radio"/>');
            }
            $('<span />').insertAfter($_radio);
            if ($.browser.msie && $.browser.version < 9) {
                radio.set_checked = function() {
                    if (this.checked) {
                        $_form.find('input[name="' + this.name + '"] + span').removeClass('checked');
                        $(this).next('span').addClass('checked').blur();
                    }
                };
                radio.set_checked();
                $_radio.bind('change', function() {
                    this.set_checked();
                });
            }
            if (App.session.device.iOS) {
                var $_label = $_radio.parent('label.radio');
                $_label.click(function() {
                    $_radio.focus();
                });
            }
            if (radio.value == 'on') radio.value = true;
        });
    }
    var updateSelects = function($_form) {
        var $$_selects = $_form.find('select');
        _.each($$_selects, function(select) {
            var $_select = $(select);
            // $_select.wrap('<div class="select" />');
        });
    }
    App.vent.bind('view:rendered', function(args) {
        updatePlaceholders(args.view.$el);
        var $$_forms = args.view.$el.find('form');
        if ($$_forms.length) {
            _.each($$_forms, function(form) {
                var $_form = $(form);
                updateForm($_form);
                updateCheckboxes($_form);
                updateRadios($_form);
                updateSelects($_form);
            });
        }
    });
    $.fn.multipartSubmit = function(args) {
        var url = args.url || '',
            method = args.method || 'POST',
            success_callback = args.success || function() {}, error_callback = args.error || function() {}, $_form = $(this),
            _form = this,
            $_iframe = $('<iframe id="upload_iframe" name="upload_iframe" class="hidden" src="' + App.settings.server + '/pages/blank" />'),
            _method = false,
            $_auth_token = false,
            $_json_submit = $_form.find('input[name="json_submit"]').length ? null : $('<input type="hidden" name="json_submit" value="true" />').appendTo($_form);
        if (method === 'PUT') {
            _method = $('<input type="hidden" name="_method" value="put" />').appendTo($_form);
            method = 'POST';
        }
        if (App.user.get('authenticity_token')) {
            $_form.find('input[name="authenticity_token"]').remove();
            $_authenticity_token = $('<input type="hidden" name="authenticity_token" value="' + App.user.get('authenticity_token') + '" />').appendTo($_form);
        }
        if (App.session.token) {
            $_form.find('input[name="auth_token"]').remove();
            $_auth_token = $('<input type="hidden" name="auth_token" value="' + App.session.token + '" />').appendTo($_form);
        }
        $_iframe.bind('load', function() {
            var response = this.contentWindow.document.body.textContent || this.contentWindow.document.body.innerText || false;
            if (response && !$.parseJSON(response).blank) {
                $_form.attr('target', '');
                $_form.attr('action', '');
                $_form.attr('method', '');
                if (_method) _method.remove();
                if ($_auth_token) $_auth_token.remove();
                if ($_json_submit) $_json_submit.remove();
                if (response) success_callback(response);
                setTimeout(function() {
                    $('#upload_iframe').remove();
                }, 250);
            }
        });
        $_iframe.bind('error', function() {
            var response = this.contentWindow.document.body.textContent || this.contentWindow.document.body.innerText || false;
            if (response) error_callback(response);
        });
        $_form.attr({
            'enctype': 'multipart/form-data',
            'target': 'upload_iframe',
            'action': url,
            'method': method
        });
        $_iframe.appendTo(document.body);
        this.submit();
    }
    $.fn.toModel = function(args) {
        if (this.find('p.field.error').length) return;
        var _this = this,
            modelName = $.isPlainObject(args) ? args.model : args;
        if (modelName && !this.busy_submitting) {
            this.busy_submitting = true;
            if (args.create_new_model) {
                var model = new ht.models[modelName];
            } else {
                var model = App.models[modelName] = App.models[modelName] || new ht.models[modelName];
            }
            if (args.url) {
                model.url = args.url;
                if (model.url.indexOf(App.settings.server) == -1) {
                    model.url = model.url.indexOf('/') == 0 ? App.settings.server + model.url : App.settings.server + '/' + model.url;
                }
            }
            model.save($(this).toJSON(), {
                success: function(model, response) {
                    if (response.result) {
                        _this.trigger('form:saved', {
                            model: model,
                            response: response
                        });
                        _this.busy_submitting = false;
                        if (args.collection) {
                            var collection = args.collection.models ? args.collection : (App.collections[args.collection] || new App.collections[args.collection]);
                            collection.add(model.attributes);
                        }
                    } else {
                        _this.trigger('form:error', {
                            model: model,
                            response: response,
                            errors: response.errors
                        });
                        _this.busy_submitting = false;
                    }
                },
                error: function(model, response) {
                    _this.trigger('form:error', {
                        model: model,
                        response: response,
                        errors: response.errors
                    });
                    _this.busy_submitting = false;
                },
                wait: true
            });
            return model;
        }
    };
    $.fn.toJSON = function() {
        var o = {}, a = this.serializeArray(),
            current_o = o,
            $_form = this;
        $.each(a, function() {
            var nameArr = _.flatten([this.name.match(/[^[.]+/), this.name.match(/\[(.*?)\]/g)]);
            if (nameArr[0] && !nameArr[1]) {
                o[nameArr[0]] = this.value;
            } else {
                nameArr = _.map(nameArr, function(item) {
                    return item.replace(/[\[\]]+/g, '')
                });
                var root_key = nameArr.shift();
                if (!o[root_key]) {
                    var next_key = nameArr[0];
                    o[root_key] = next_key != undefined ? next_key.length ? {} : [] : '';
                }
                current_o = o[root_key];
                for (i = 0; i < nameArr.length; i++) {
                    var key = nameArr[i];
                    if (!current_o[key]) {
                        if (key.length) {
                            var next_key = nameArr[i + 1];
                            current_o[key] = next_key != undefined ? next_key.length ? {} : [this.value] : this.value;
                            current_o = current_o[key];
                        } else {
                            if (!$.isArray(current_o)) current_o = [this.value];
                            else if ($.isArray(current_o)) current_o.push(this.value);
                        }
                    } else {
                        if ($.isArray(current_o[key])) current_o[key].push(this.value);
                    }
                }
            }
        });
        return o;
    };
    //      args.that:           this element,
    //      args.countSpan:      @string = elemnt to alter with length
    $.fn.charCount = function(args) {
        var _this = this;
        var length = _this.val().length;
        args.that.$el.find(args.countSpan).html(length);
        return length;
    };
    $.fn.charCountContentEditable = function(args) {
        var _this = this;
        var length = _this.text().replace(/<[\/]{0,1}(span|SPAN)[^><]*>/g, "").length;
        args.that.$el.find(args.countSpan).html(length);
        return length;
    };
    $.fn.setCursor = function(pos) {
        return this.each(function() {
            var _this = this;
            if (_this.createTextRange) {
                var textRange = _this.createTextRange();
                textRange.collapse(true);
                textRange.moveEnd(pos);
                textRange.moveStart(pos);
                textRange.select();
            } else if (_this.setSelectionRange) {
                _this.setSelectionRange(pos, pos);
            }
        });
    };
    App.usStates = function() {
        var states = ["AL", "AK", "AZ", "AR", "CA", "CO", "CT", "DE", "DC", "FL", "FM", "GA", "HI", "ID", "IL", "IN", "IA", "KS", "KY", "LA", "ME", "MD", "MA", "MI", "MN", "MS", "MO", "MT", "NE", "NV", "NH", "NJ", "NM", "NY", "NC", "ND", "OH", "OK", "OR", "PA", "PR", "RI", "SC", "SD", "TN", "TX", "UT", "VT", "VA", "WA", "WV", "WI", "WY"].sort();
        return states;
    }
    App.countries = function() {
        var countries = {
            "Afghanistan": "AF",
            "Albania": "AL",
            "Algeria": "DZ",
            "American Samoa": "AS",
            "Andorra": "AD",
            "Angola": "AO",
            "Anguilla": "AI",
            "Antarctica": "AQ",
            "Antigua and Barbuda": "AG",
            "Argentina": "AR",
            "Armenia": "AM",
            "Aruba": "AW",
            "Australia": "AU",
            "Austria": "AT",
            "Azerbaijan": "AZ",
            "Bahamas": "BS",
            "Bahrain": "BH",
            "Bangladesh": "BD",
            "Barbados": "BB",
            "Belarus": "BY",
            "Belgium": "BE",
            "Belize": "BZ",
            "Benin": "BJ",
            "Bermuda": "BM",
            "Bhutan": "BT",
            "Bolivia": "BO",
            "Bosnia and Herzegovina": "BA",
            "Botswana": "BW",
            "Bouvet Island": "BV",
            "Brazil": "BR",
            "British Indian Ocean Territory": "IO",
            "British Virgin Islands": "VG",
            "Brunei": "BN",
            "Bulgaria": "BG",
            "Burkina Faso": "BF",
            "Burma": "MM",
            "Burundi": "BI",
            "Cambodia": "KH",
            "Cameroon": "CM",
            "Canada": "CA",
            "Cape Verde": "CV",
            "Cayman Islands": "KY",
            "Central African Republic": "CF",
            "Chad": "TD",
            "Chile": "CL",
            "China": "CN",
            "Christmas Island": "CX",
            "Cocos (Keeling) Islands": "CC",
            "Colombia": "CO",
            "Comoros": "KM",
            "Congo Democratic Republic": "CD",
            "Congo Republic (Middle Congo)": "CG",
            "Cook Islands": "CK",
            "Costa Rica": "CR",
            "Croatia": "HR",
            "Cuba": "CU",
            "Cyprus": "CY",
            "Czech Republic": "CZ",
            "Denmark": "DK",
            "Djibouti": "DJ",
            "Dominica": "DM",
            "Dominican Republic": "DO",
            "East Timor": "TP",
            "Ecuador": "EC",
            "Egypt": "EG",
            "El Salvador": "SV",
            "Equitorial Guinea": "GQ",
            "Eritrea": "ER",
            "Estonia": "EE",
            "Ethiopia": "ET",
            "Falkland Islands": "FK",
            "Faroe Islands": "FO",
            "Fiji": "FJ",
            "Finland": "FI",
            "France": "FR",
            "French Guiana": "GF",
            "French Polinesia": "PF",
            "French Southern and Antarctic Territories": "TF",
            "Gabon": "GA",
            "Gambia": "GM",
            "Georgia": "GE",
            "Germany": "DE",
            "Ghana": "GH",
            "Gibraltar": "GI",
            "Great Britain": "UK",
            "Greece": "GR",
            "Greenland": "GL",
            "Grenada": "GD",
            "Guadeloupe": "GP",
            "Guam": "GU",
            "Guatemala": "GT",
            "Guernsey": "GG",
            "Guinea": "GN",
            "Guinea Bissau": "GW",
            "Guyana": "GY",
            "Haiti": "HT",
            "Heard Island and McDonald Islands": "HM",
            "Honduras": "HN",
            "Hong Kong": "HK",
            "Hungary": "HU",
            "Iceland": "IS",
            "India": "IN",
            "Indonesia": "ID",
            "Iran": "IR",
            "Iraq": "IQ",
            "Ireland": "IE",
            "Isle of Man": "IM",
            "Israel": "IL",
            "Italy": "IT",
            "Ivory Coast": "CI",
            "Jamaica": "JM",
            "Japan": "JP",
            "Jersey": "JE",
            "Jordan": "JO",
            "Kazakhstan": "KZ",
            "Kenya": "KE",
            "Kiribati": "KI",
            "Korea, North": "KP",
            "Korea, South": "KR",
            "Kuwait": "KW",
            "Kyrgystan": "KG",
            "Laos": "LA",
            "Latvia": "LV",
            "Lebanon": "LB",
            "Lesotho": "LS",
            "Liberia": "LR",
            "Libya": "LY",
            "Liechtenstein": "LI",
            "Lithuania": "LT",
            "Luxembourg": "LU",
            "Macau": "MO",
            "Macedonia": "MK",
            "Madagascar": "MG",
            "Malawi": "MW",
            "Malaysia": "MY",
            "Maldives": "MV",
            "Mali": "ML",
            "Malta": "MT",
            "Marshall Islands": "MH",
            "Martinique": "MQ",
            "Mauritania": "MR",
            "Mauritius": "MU",
            "Mayotte": "YT",
            "Mexico": "MX",
            "Micronesia Federal States": "FM",
            "Moldova": "MD",
            "Monaco": "MC",
            "Mongolia": "MN",
            "Montserrat": "MS",
            "Morocco": "MO",
            "Mozambique": "MZ",
            "Myanmar": "MM",
            "Namibia": "NA",
            "Nauru": "NR",
            "Nepal": "NP",
            "Netherlands": "NL",
            "Netherlands Antilles": "AN",
            "New Caledonia": "NC",
            "New Zeland": "NZ",
            "Nicaragua": "NI",
            "Niger": "NE",
            "Nigeria": "NG",
            "Niue": "NU",
            "Norfolk Island": "NF",
            "Northern Mariana Islands": "MP",
            "North Korea": "KP",
            "Norway": "NO",
            "Oman": "OM",
            "Pakistan": "PK",
            "Palau": "PW",
            "Palestine": "PS",
            "Panama": "PA",
            "Papua New Guinea": "PG",
            "Paraguay": "PY",
            "Peru": "PE",
            "Philippines": "PH",
            "Pitcairn Island": "PN",
            "Poland": "PL",
            "Portugal": "PT",
            "Puerto Rico": "PR",
            "Qatar": "QA",
            "Reunion": "RE",
            "Romania": "RO",
            "Russia": "RU",
            "Rwanda": "RW",
            "Saint Helena": "SH",
            "Saint Kitts & Nevis": "KN",
            "Saint Lucia": "LC",
            "Saint Pierre and Miquelon": "PM",
            "Saint Vincent and The Grenadines": "VC",
            "Samoa": "WS",
            "San Marino": "SM",
            "Sao Tome and Principe": "ST",
            "Saudi Arabia": "SA",
            "Senegal": "SN",
            "Serbia and Montenegro (Yugoslavia)": "YU",
            "Seychelles": "SC",
            "Sierra Leone": "SL",
            "Singapore": "SG",
            "Slovakia": "SK",
            "Slovenia": "SI",
            "Solomon Islands": "SB",
            "Somalia": "SO",
            "South Africa": "ZA",
            "South Korea": "KR",
            "South Georgia and the South Sandwich Islands": "GS",
            "Spain": "ES",
            "Sri Lanka": "LK",
            "Sudan": "SD",
            "Suriname": "SR",
            "Svalbard (Spitzbergen) and Jan Mayen Islands": "SJ",
            "Swaziland": "SZ",
            "Sweden": "SE",
            "Switzerland": "CH",
            "Syria": "SY",
            "Taiwan": "TW",
            "Tajikistan": "TJ",
            "Tanzania": "TZ",
            "Thailand": "TH",
            "Togo": "TG",
            "Tokelau": "TK",
            "Tonga": "TO",
            "Trinidad & Tobago": "TT",
            "Tromelin Island": "TE",
            "Tunisia": "TN",
            "Turkey": "TR",
            "Turkmenistan": "TM",
            "Turks and Caicos Islands": "TC",
            "Tuvalu": "TV",
            "Uganda": "UG",
            "Ukraine": "UA",
            "United Arab Emirates": "AE",
            "United Kingdom (Great Britain)": "UK",
            "United States of America (USA)": "US",
            "United States Minor Outlying Islands": "UM",
            "Uruguay": "UY",
            "Uzbekistan": "UZ",
            "Vanuatu": "VU",
            "Vatican City": "VA",
            "Venezuela": "VE",
            "Vietnam": "VN",
            "Virgin Islands  (British)": "VI",
            "Virgin Islands (United States)": "VQ",
            "Wallis and Futuna Islands": "WF",
            "Western Sahara": "EH",
            "Yemen": "YE",
            "Zambia": "ZM",
            "Zimbabwe": "ZW"
        }
        /*  return _.map(countries, function(code, country) {
     return code
     }).sort(); */
        return _.map(countries, function(code, country) {
            return country
        }).sort();
    }
    App.expert_specialties_data = function() {
        var specialities = {
            "ADHD and Autism": "ADHD and Autism",
            "Addiction Medicine": "Addiction Medicine",
            "Adolescent Medicine": "Pediatric Adolescent Medicine",
            "Aerospace Medicine": "Aerospace Medicine",
            "Aesthetic Medicine": "Aesthetic Medicine",
            "Allergy": "Allergy",
            "Allergy and Immunology": "Allergy and Immunology",
            "Anesthesiology": "Anesthesiology",
            "Bariatrics": "Bariatrics",
            "Breast Surgery": "Breast Surgery",
            "Cardiology": "Cardiology",
            "Child Psychiatry": "Child Psychiatry",
            "Clinical Genetics": "Clinical Genetics",
            "Clinical Psychology": "Clinical Psychology",
            "Colon and Rectal Surgery": "Colon and Rectal Surgery",
            "Critical Care": "Critical Care",
            "Dentistry": "Dentistry",
            "Dermatology": "Dermatology",
            "Developmental and Behavioral Pediatrics": "Pediatrics - Developmental and Behavioral",
            "Diabetology": "Diabetology",
            "ENT and Head and Neck Surgery": "ENT and Head and Neck Surgery",
            "Emergency Medicine": "Emergency Medicine",
            "Endocrinology": "Endocrinology",
            "Environmental Health": "Environmental Health",
            "Facial Plastic Surgery": "Facial Plastic Surgery",
            "Family Medicine": "Family Medicine",
            "Fertility Medicine": "Fertility Medicine",
            "Gastroenterology": "Gastroenterology",
            "General Practice": "General Practice",
            "General Surgery": "Surgery",
            "Geriatrics": "Geriatrics",
            "Gynecologic Oncology": "Gynecologic Oncology",
            "Gynecology": "Gynecology",
            "Hair Restoration": "Hair Restoration",
            "Hand Surgery": "Hand Surgery",
            "Head and Neck Surgery": "Head and Neck Surgery",
            "Hematology": "Hematology",
            "Hematology and Oncology": "Hematology and Oncology",
            "Hepatology": "Hepatology",
            "Holistic Medicine": "Holistic Medicine",
            "Hospital-based practice": "Hospital-based practice",
            "Immunology": "Immunology",
            "Infectious Disease": "Infectious Disease",
            "Internal Medicine": "Internal Medicine",
            "Internal Medicine and Pediatrics": "Internal Medicine and Pediatrics",
            "Maternal-Fetal Medicine": "Maternal-Fetal Medicine",
            "Medical Oncology": "Medical Oncology",
            "Neonatology": "Neonatology",
            "Nephrology and Dialysis": "Nephrology and Dialysis",
            "Neurology": "Neurology",
            "Neurosurgery": "Neurosurgery",
            "Nuclear Medicine": "Nuclear Medicine",
            "Obstetric Medicine": "Internal Medicine - Obstetric Medicine",
            "Obstetrics and Gynecology": "Obstetrics and Gynecology",
            "Occupational Medicine": "Occupational Medicine",
            "Ophthalmology": "Ophthalmology",
            "Oral and Maxillofacial Surgery": "Oral and Maxillofacial Surgery",
            "Orthopedic Foot and Ankle Surgery": "Orthopedic Foot and Ankle Surgery",
            "Orthopedic Reconstructive Surgery": "Orthopedic Reconstructive Surgery",
            "Orthopedic Spine Surgery": "Orthopedic Spine Surgery",
            "Orthopedic Surgery": "Orthopedic Surgery",
            "Pain Management": "Pain Management",
            "Palliative Care": "Palliative Care",
            "Palliative/Hospice and Transitional Care": "Palliative/Hospice and Transitional Care",
            "Pathology": "Pathology",
            "Pediatric Allergy": "Pediatric Allergy",
            "Pediatric Allergy and Asthma": "Pediatric Allergy and Asthma",
            "Pediatric Cardiology": "Pediatric Cardiology",
            "Pediatric Critical Care": "Pediatric Critical Care",
            "Pediatric Dermatology": "Pediatric Dermatology",
            "Pediatric ENT": "ENT - Pediatric",
            "Pediatric Emergency Medicine": "Pediatric Emergency Medicine",
            "Pediatric Endocrinology": "Pediatric Endocrinology",
            "Pediatric Gastroenterology": "Pediatric Gastroenterology",
            "Pediatric Hematology and Oncology": "Pediatric Hematology and Oncology",
            "Pediatric Infectious Disease": "Pediatric Infectious Disease",
            "Pediatric Nephrology and Dialysis": "Pediatric Nephrology and Dialysis",
            "Pediatric Neurology": "Pediatric Neurology",
            "Pediatric Oncology": "Pediatric Oncology",
            "Pediatric Ophthalmology": "Pediatric Ophthalmology",
            "Pediatric Orthopedic Surgery": "Pediatric Orthopedic Surgery",
            "Pediatric Pulmonology": "Pediatric Pulmonology",
            "Pediatric Rheumatology": "Pediatric Rheumatology",
            "Pediatric Sports Medicine": "Pediatric Sports Medicine",
            "Pediatric Surgery": "Pediatric Surgery",
            "Pediatric Urology": "Pediatric Urology",
            "Pediatrics": "Pediatrics",
            "Pharmacology": "Pharmacology",
            "Phlebology": "Phlebology",
            "Physical Medicine and Rehabilitation": "Physical Medicine and Rehabilitation",
            "Plastic Surgery": "Plastic Surgery",
            "Podiatry": "Podiatry",
            "Preventive Medicine": "Preventive Medicine",
            "Psychiatry": "Psychiatry",
            "Public Health": "Public Health",
            "Pulmonary Critical Care": "Pulmonary Critical Care",
            "Pulmonology": "Pulmonology",
            "Radiation Oncology": "Radiation Oncology",
            "Radiology": "Radiology",
            "Retinal Surgery": "Retinal Surgery",
            "Rheumatology": "Rheumatology",
            "Sleep Medicine": "Sleep Medicine",
            "Sports Medicine": "Sports Medicine",
            "Surgical Oncology": "Surgical Oncology",
            "Thoracic Surgery": "Thoracic Surgery",
            "Transplant Surgery": "Transplant Surgery",
            "Trauma Surgery": "Trauma Surgery",
            "Travel Medicine": "Travel Medicine",
            "Urgent Care": "Urgent Care",
            "Urogynecology": "Urogynecology",
            "Urologic Oncology": "Urologic Oncology",
            "Urology": "Urology",
            "Vascular Surgery": "Vascular Surgery",
            "Wilderness Medicine": "Wilderness Medicine",
            "Wound care": "Wound care"
        };
        return specialities;
    }
    App.expert_specialties = function(query) {
        console.log(query);
        if (!query) {
            return App.expert_specialties_data()
        } else {
            console.log(query);
            var temp = App.expert_specialties_data();
            var temp2 = {};
            for (key in temp) {
                if (temp[key].toLowerCase().indexOf(query.toLowerCase()) > -1) {
                    temp2[key] = temp[key]
                }
            }
            console.log(temp2);
            return temp2;
        }
    }
    App.expert_degree = function() {
        var degree = {
            'MD': 'MD',
            'MDCM': 'MDCM',
            'DO': 'DO',
            'MBBS': 'MBBS',
            'MBChB': 'MBChB',
            'DMD': 'DMD',
            'DDS': 'DDS',
            'DPM': 'DPM',
            'EdD': 'EdD',
            'PsyD': 'PsyD',
            'PhD': 'PhD',
            'PharmD': 'PharmD',
        }
        return degree;
    }
    var leapYearcheck = function(yearId) {
        if (yearId % 400 == 0 || (yearId % 100 != 0 && yearId % 4 == 0)) {
            return true;
        } else {
            return false;
        }
    }
    $.fn.checkDate = function(valtext) {
        var re1format = /^[0-9]{8}$/;
        var re2format = /^[0-9]{0,2}-[0-9]{0,2}-[0-9]{4}$/;
        var re3format = /^[0-9]{0,2}\/[0-9]{0,2}\/[0-9]{4}$/;
        var re4format = /^[0-9]{0,2} [0-9]{0,2} [0-9]{4}$/;
        var arr31 = [1, 3, 5, 7, 8, 10, 31];
        var arr30 = [4, 6, 9, 11];
        var comJson = {};
        if ((valtext.search(re1format) == -1) && (valtext.search(re2format) == -1) && (valtext.search(re3format) == -1) && (valtext.search(re4format) == -1)) {
            comJson.msg = "Please enter the date";
            comJson.rt = false;
        } else {
            var splitF;
            var month;
            var days;
            var year;
            if (valtext.indexOf("-") > -1) {
                splitF = valtext.split("-");
                month = splitF[0];
                days = splitF[1];
                year = splitF[2];
            } else if (valtext.indexOf("/") > -1) {
                splitF = valtext.split("/");
                month = splitF[0];
                days = splitF[1];
                year = splitF[2];
            } else if (valtext.indexOf(" ") > -1) {
                splitF = valtext.split(" ");
                month = splitF[0];
                days = splitF[1];
                year = splitF[2];
            } else {
                month = parseInt(valtext.charAt(0) + valtext.charAt(1));
                days = parseInt(valtext.charAt(2) + valtext.charAt(3));
                year = parseInt(valtext.charAt(4) + valtext.charAt(5) + valtext.charAt(6) + valtext.charAt(7));
            }
            if (month > 12 || month == 0) {
                comJson.msg = "Not a valid month";
                comJson.rt = false;
            } else if (days > 31 || days == 0) {
                comJson.msg = "Not a valid day";
                comJson.rt = false;
            } else if (year < 1910 || year > new Date().getFullYear()) {
                comJson.msg = "Not a valid year";
                comJson.rt = false;
            } else if ((month == 4 || month == 6 || month == 9 || month == 11) && days > 30) {
                comJson.msg = "April, June, September, November have 30 days";
                comJson.rt = false;
            } else if (month == 2) {
                if (days > 29) {
                    comJson.msg = "Feb month allows upto 29 days";
                    comJson.rt = false;
                } else if (leapYearcheck(year) == false && days > 28) {
                    comJson.msg = "This is a not a leap year and days can not be greater than 28";
                    comJson.rt = false;
                } else {
                    comJson.msg = "Ok";
                    comJson.rt = true;
                }
            } else {
                comJson.msg = "Ok";
                comJson.rt = true;
            }
        }
        comJson.month = month;
        comJson.days = days;
        comJson.year = year;
        if (comJson.msg !== 'Ok') {
            $_field = (this.parents('p.field'));
            $_field.find("p.error_msg").remove();
            $_field = (this.parents('p.field'));
            $_field.addClass('error');
            $_field.append('<p class="error_msg">' + comJson.msg + '</p>');
            App.vent.trigger('form:error', {
                form: $_field.parents('form'),
                errors: {}
            });
        } else {
            $_field = (this.parents('p.field'));
            $_field.removeClass('error');
            $_field.find("p.error_msg").remove();
        }
        return comJson;
    }, $.fn.resetForm = function() {
        this.find('input:text, input:password, input:file, select, textarea').val('');
        this.find('input:radio, input:checkbox').removeAttr('checked').removeAttr('selected');
    }
})();