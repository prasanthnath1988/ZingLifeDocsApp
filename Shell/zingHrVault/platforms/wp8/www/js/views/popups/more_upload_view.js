window.app = window.app || {};
app.views = app.views || {};
app.views.uploadMoreDocumets = Backbone.Marionette.ItemView.extend({
	template: '#uploadMoreDocuments',
	events: {
		'click .close_h': 'closePopup',
		'click .take_from_camera_h':'takeImageFromCamera',
		'click .take_from_gallery_h':'takeImageFromGallery',
	},
	initialize: function() {
		App.vent.trigger("closePopups");
		_.bindAll(this, 'takeImageFromCamera','takeImageFromGallery');
	},
	onRender: function() {
		
	},
	closePopup: function() {
		App.vent.trigger("closePopups");
		App.routers.mainRouter.navigate('#userHome', {
			trigger : true
		});
	},
	takeImageFromCamera : function(){
		var _this =this;
		console.log("images from Camera");

		navigator.camera.getPicture(onSuccess, onFail, {
			quality : 100,
			destinationType : Camera.DestinationType.DATA_URL,
			sourceType : Camera.PictureSourceType.CAMERA,
			allowEdit : true,
			encodingType : Camera.EncodingType.JPEG,
			targetWidth : 500,
			targetHeight : 500,
			popoverOptions : CameraPopoverOptions,
			saveToPhotoAlbum : false
		});
		function onSuccess(imageData) {
			console.log("Image URI success");
			App.sessionVars.imageData = imageData;
			App.vent.trigger("closePopups");
			$("div.doc_img_con").remove();
			$( '<div class="doc_img_con"><div class="doc_img" id="imageToUpload"><span class="delete_img delete_h">Remove</span><img src="data:image/jpeg;base64,'+imageData+'" class="imagesIds"></div></div>' ).insertBefore(App.pages.imageDetails.$el.find('#docNameDiv'));
			//App.pages.imageDetails.$el.find('#imagesIds').attr("src","");
			//App.pages.imageDetails.$el.find('#imagesIds').attr("src","data:image/jpeg;base64,"+imageData);
			App.pages.imageDetails.$el.find('#documentName').val("");
			App.pages.imageDetails.$el.find('#documentsTags').val(App.sessionVars.mainTags);
			App.pages.imageDetails.$el.find(".terms_condition_check_h").prop("checked",false);
		}
		function onFail(message) {
			var options = {
					"title":App.settings.messageTitle.failureTitle,
					"message":'You closed camera, please try again'
			}
			App.controller.showPopup('alert',options);
		}
	},
	takeImageFromGallery : function(){
		console.log("images from Gallery");
		var _this =this;
		navigator.camera.getPicture(onSuccess, onFail, {
			quality : 100,
			destinationType : Camera.DestinationType.DATA_URL,
			sourceType : Camera.PictureSourceType.SAVEDPHOTOALBUM,
			allowEdit : true,
			encodingType : Camera.EncodingType.JPEG,
			targetWidth : 500,
			targetHeight : 500,
			popoverOptions : CameraPopoverOptions,
			saveToPhotoAlbum : false
		});
		function onSuccess(imageData) {
			App.sessionVars.imageData = imageData;
			console.log("Image URI success");
			console.log(imageData);
			App.vent.trigger("closePopups");
			$("div.doc_img_con").remove();
			$( '<div class="doc_img_con"><div class="doc_img" id="imageToUpload"><span class="delete_img delete_h">Remove</span><img src="data:image/jpeg;base64,'+imageData+'" class="imagesIds"></div></div>' ).insertBefore(App.pages.imageDetails.$el.find('#docNameDiv'));
			//App.pages.imageDetails.$el.find('#imagesIds').attr("src","");
			//App.pages.imageDetails.$el.find('#imagesIds').attr("src","data:image/jpeg;base64,"+imageData);
			App.pages.imageDetails.$el.find('#documentName').val("");
			App.pages.imageDetails.$el.find('#documentsTags').val(App.sessionVars.mainTags);
			App.pages.imageDetails.$el.find(".terms_condition_check_h").prop("checked",false);
//			App.pages.imageDetails.$el.find('#imagesIds');
		}
		function onFail(message) {
			App.vent.trigger("closePopups");
			var options = {
					"title":App.settings.messageTitle.failureTitle,
					"message":'You did not choose any file, please try again'
			}
			App.controller.showPopup('alert',options);
		}
	},
	onClose: function() {
		
	}
});