var PG_facebook_fb_clientID = '726338797408155';
// Facebook API Key
var PG_facebook_fb_secret = 'bd050a4e3bdfb28fd9e1be272c01f8ac';

function PG_loginFacebook() {
	App.pages.userLogin.$el.find('#userEmailAdd').val("");
	App.pages.userLogin.$el.find('#userPassword').val("");
	App.controller.showPopup('loader');
	var FB_token =window.localStorage.getItem("facebook_token"); 
	if(FB_token){
		userFBDetailsFetch(FB_token);
	}else{
		try{
			var my_client_id = PG_facebook_fb_clientID;
			var my_redirect_uri = "https://www.facebook.com/connect/login_success.html";
			var my_type = "user_agent";
			var my_display = "touch";
			var authorize_url = "https://graph.facebook.com/oauth/authorize?";
			authorize_url += "client_id=" + my_client_id;
			authorize_url += "&redirect_uri=" + my_redirect_uri;
			authorize_url += "&display=" + my_display;
			authorize_url += "&scope=email,read_stream,publish_stream,offline_access,publish_checkins";
			
			authorize_url =encodeURI(authorize_url);
			var iabRef = null;
			iabRef = window.open(authorize_url, '_blank', 'clearsessioncache=yes,location=no,clearcache=no');
			iabRef.addEventListener('loaderror', function(error) {
				console.log(JSON.stringify(error));
				iabRef.close();
			});
			iabRef.addEventListener('exit', function() {
				App.vent.trigger("closePopups"); 
			});
			iabRef.addEventListener('loadstart',
					function(loc, params) {
				var loc = JSON.stringify(loc);
				console.log("________________________loc__________________");
				console.log(JSON.stringify(loc));
				if (((loc.indexOf("http://www.facebook.com/connect/login_success.html") > -1) || (loc.indexOf("https://www.facebook.com/connect/login_success.html") > -1)) && loc.indexOf("&redirect_uri") == -1) {
                                    iabRef.executeScript({ code: " localStorage.clear(); sessionStorage.clear() " });
					iabRef.close();
					var fbCode = loc.match(/code=(.*)$/)[1];
					if (!fbCode)
						return false;
					var redirectUriVar = "http://www.facebook.com/connect/login_success.html";
					if (loc.indexOf("https://www.facebook.com/connect/login_success.html") > -1) {
						redirectUriVar = "https://www.facebook.com/connect/login_success.html"
					}
					var fb_url = "https://graph.facebook.com/oauth/access_token?client_id="
						+ PG_facebook_fb_clientID
						+ "&client_secret="
						+ PG_facebook_fb_secret
						+ "&redirect_uri="
						+ redirectUriVar
						+ "&code=" + fbCode;
					console.log(fb_url);
					App.controller.showPopup('loader');
					$.ajax({
						url : fb_url,
						async : true,
						data : {},
						success : function(data, status) {
							App.vent.trigger("closePopups");
							var token = data.split("=")[1].split("&")[0];
							window.localStorage.setItem("facebook_token",token);
							console.log('D: got facebook token');
							iabRef.close();
							userFBDetailsFetch(token,"login");
						},
						error : function(error) {
							App.vent.trigger("closePopups");
							console.log("FB LOG: ERROR");
							console.log(JSON
									.stringify(error));
							var options = {
									"title":App.settings.messageTitle.failureTitle,
									"message":"Error in Facebook login, Please try again."
							}
							App.controller.showPopup('alert',options);
							try {
								iabRef.close();
							} catch (e) {
							}
						},
						dataType : 'text',
						type : 'POST'
					})
				}
			});
		} catch (e) {
			App.vent.trigger("closePopups");
			console.log("PG_loginFacebook: " + e)
		}
	}
}


function loginInParseFacebbok(data,dataFrom) {
	
	console.log("FB data"+JSON.stringify(data));
	App.controller.showPopup('loader');

	var myExpDate = new Date();
	myExpDate.setMonth(myExpDate.getMonth() + 1);
	myExpDate = myExpDate.toISOString();

	var facebookAuthData = {
		"id" : data.userInfo.id,
		"expiration_date" : myExpDate,
		"access_token" : data.token
		
	}
	console.log(JSON.stringify(facebookAuthData));

	Parse.FacebookUtils.logIn(facebookAuthData, {
		success : function(user) {
			console.log("______________Intalation_ID_:  "+App.sessionVars.pushInstallationId);
			if(App.sessionVars.pushInstallationId){
				user.set("Installation_Id", App.sessionVars.pushInstallationId);
				user.set("push_objectId", App.sessionVars.ObjectpushInstallationId);
			}
			user.set("name",data.userInfo.first_name);
			if(data.userInfo.email){
				user.set("email",data.userInfo.email);
			}
			if(data.userInfo.userImage){
				user.set("userFBProfilePicture",data.userInfo.userImage);
			}
			
			
            user.save(null, {
                success : function(user) {
                    App.models.user = new app.models.user();
                    App.models.user.set(user);
                    App.vent.trigger("closePopups");

					if ($('#signedCheck').is(':checked')) {
						App.sessionVars.keepMeSignedIn = 1;
						localStorage.setItem('keepMeSignedIn',1);

					}
					else
					{
						App.sessionVars.keepMeSignedIn = 0;
						localStorage.setItem('keepMeSignedIn',0);
					}

                    if(dataFrom != "silent"){
                    	if(App.sessionVars.pushInstallationId && data.userInfo.email){                            
                    		updatePushInstallationTable(data.userInfo.email, App.sessionVars.pushInstallationId,App.sessionVars.ObjectpushInstallationId);

                    	}else{
                    		App.routers.mainRouter.navigate('#userHome', {
    	                        trigger : true
    	                    });
                    	}
	                    
                    }
                },
                error: function(e) {
                	// condition in case user registered from facebook login and signup process
                    App.vent.trigger("closePopups");
                    var options = {
					"title":App.settings.messageTitle.failureTitle,
					"message": "User already exists."
					};
					App.controller.showPopup('alert',options);
					window.localStorage.removeItem('facebook_token');
					window.localStorage.removeItem('user_emailId');
					window.localStorage.removeItem('user_Password');
                      Parse.User.logOut();
                      window.localStorage.clear();
                      
					if (App.settings.isDeviceReady) {
						window.cookies.clear(function() {
							console.log('Cookies cleared!');
						});
					} else {
						console.log("Device is not ready");
					}
                    console.log("Oh crap", e);
                    App.sessionVars.fbEmail="";
                }

            });
			
		},
		error : function(error1, error2) {
			App.vent.trigger("closePopups");
			var options = {
					"title":App.settings.messageTitle.failureTitle,
					"message":"Unable to create/login to as Facebook user. Please try again."
			}
			App.controller.showPopup('alert',options);
			console.log("Unable to create/login to as Facebook user");
			console.log("  ERROR1 = " + JSON.stringify(error1));
			console.log("  ERROR2 = " + JSON.stringify(error2));
		}
	});

}

function userFBDetailsFetch(token,fromData){
	App.controller.showPopup('loader');
	console.log(token);
	var userIdUrl = "https://graph.facebook.com/me?access_token="+token;
	console.log(userIdUrl);
	$.ajax({
		type : "GET",
		url : userIdUrl,
		async : true,
		data : {},
		dataType : "json",
		success : function(data) {
			App.vent.trigger("closePopups");
			console.log(JSON.stringify(data));

			if(data.email==undefined && (App.sessionVars.fbEmail==undefined  || App.sessionVars.fbEmail=="") )
			{
				console.log('fb email not found and also not present in session');
				App.controller.showPopup('emailFacebook');
				return false;
			}
			else
			{
				if(data.email==undefined)
				{
					console.log('fb email not found but present in session');
					var userData = {
					"userInfo" : {
						"id" : data.id,
						"email" : App.sessionVars.fbEmail,
						"first_name" : data.first_name,
						"last_name" : data.last_name
					},
					"token" : token
					}
				}
				else
				{
					var userData = {
						"userInfo" : {
							"id" : data.id,
							"email" : data.email,
							"first_name" : data.first_name,
							"last_name" : data.last_name
						},
						"token" : token
					}
				}

			}
			
			
			var userIdUrl = "http://graph.facebook.com/"+data.id+"/picture?redirect=0&height=200&type=normal&width=200";
			console.log(userIdUrl);
			$.ajax({
				type : "GET",
				url : userIdUrl,
				async : true,
				data : {},
				dataType : "json",
				success : function(data) {
					App.vent.trigger("closePopups");
					console.log(JSON.stringify(data));
					userData.userInfo.userImage = data.data.url;
					loginInParseFacebbok(userData,fromData);
				},
				error : function(error) {
					App.vent.trigger("closePopups");
					console.log("FB LOG: ERROR IN ME_Requrest");
					var options = {
							"title":App.settings.messageTitle.failureTitle,
							"message":"Unable to create/login to as Facebook user. Please try again."
					}
					App.controller.showPopup('alert',options);
					console.log(JSON.stringify(error));
				}
			})
		},
		error : function(error) {
			App.vent.trigger("closePopups");
			console.log("FB LOG: ERROR IN ME_Requrest");
			var options = {
					"title":App.settings.messageTitle.failureTitle,
					"message":"Unable to create/login to as Facebook user. Please try again."
			}
			App.controller.showPopup('alert',options);
			console.log(JSON.stringify(error));
		}
	})
}


function updatePushInstallationTable(userEmailId, deviceId, objectId){
	var updatePushTable = Parse.Object.extend("pushInstallationTable");
	var changePushTable = new updatePushTable();
	var query = new Parse.Query(updatePushTable);
	query.equalTo("Installation_Id", deviceId);
	App.controller.showPopup('loader');
	query.find({
		success: function(results) {
			
			if(results.length > 0){
				console.log(results[0].id);
				var changePushTable = new updatePushTable();
				var query = new Parse.Query(updatePushTable);
				changePushTable.id=results[0].id;
				changePushTable.set('Installation_Id','');
				changePushTable.set('push_objectId', objectId);
				changePushTable.save(null,{
					success:function(){
						var changePushTable = new updatePushTable();
						var query = new Parse.Query(updatePushTable);
						query.equalTo("email", userEmailId);
						query.find({
							success:function(results){
								if(results.length>0){
									var changePushTable = new updatePushTable();
									var query = new Parse.Query(updatePushTable);
									changePushTable.id=results[0].id;
									changePushTable.set('Installation_Id',deviceId);
									changePushTable.set('push_objectId', objectId);
									changePushTable.save(null,{
										success:function(){
											App.vent.trigger("closePopups");
											App.routers.mainRouter.navigate('#userHome', {
												trigger : true
											});
									},
									error:function(e){
										App.vent.trigger("closePopups");
										var options = {
												"title" : App.settings.messageTitle.failureTitle,
												"message" : App.settings.messageSubject.connectionLoss
										}
										App.controller.showPopup('alert', options);
										}
									});
								}else{
									
									var changePushTable = new updatePushTable();
									var query = new Parse.Query(updatePushTable);
									changePushTable.set('Installation_Id',deviceId);
									changePushTable.set('push_objectId', objectId);
									changePushTable.set("email",userEmailId);

									changePushTable.save(null,{
										success:function(){
											App.vent.trigger("closePopups");
											App.routers.mainRouter.navigate('#userHome', {
												trigger : true
											});
									},error:function(e){
										var options = {
												"title" : App.settings.messageTitle.failureTitle,
												"message" : App.settings.messageSubject.connectionLoss
										}
										App.controller.showPopup('alert', options);
										}
									});
								}
							},error:function(e){
								App.vent.trigger("closePopups");
								var options = {
										"title" : App.settings.messageTitle.failureTitle,
										"message" : App.settings.messageSubject.connectionLoss
								}
								App.controller.showPopup('alert', options);
							}
						});
					},error:function(e){
						App.vent.trigger("closePopups");
						var options = {
								"title" : App.settings.messageTitle.failureTitle,
								"message" : App.settings.messageSubject.connectionLoss
						}
						App.controller.showPopup('alert', options);
					}
				});
			}else{					
					
					var changePushTable = new updatePushTable();
					var query = new Parse.Query(updatePushTable);
					query.equalTo("email", userEmailId);
					query.find({
						success:function(results){
							if(results.length>0){
								var changePushTable = new updatePushTable();
								var query = new Parse.Query(updatePushTable);
								changePushTable.id=results[0].id;
								changePushTable.set('Installation_Id',deviceId);
								changePushTable.set('push_objectId', objectId);
								changePushTable.save(null,{
									success:function(){
										App.routers.mainRouter.navigate('#userHome', {
											trigger : true
										});
								},
								error:function(e){
									var options = {
											"title" : App.settings.messageTitle.failureTitle,
											"message" : App.settings.messageSubject.connectionLoss
									}
									App.controller.showPopup('alert', options);
									}
								});
							}else{
								var changePushTable = new updatePushTable();
								var query = new Parse.Query(updatePushTable);

								changePushTable.set('Installation_Id',deviceId);
								changePushTable.set('push_objectId', objectId);
								changePushTable.set("email",userEmailId);

								changePushTable.save(null,{
									success:function(){
										App.routers.mainRouter.navigate('#userHome', {
											trigger : true
										});
								},error:function(e){
									var options = {
											"title" : App.settings.messageTitle.failureTitle,
											"message" : App.settings.messageSubject.connectionLoss
									}
									App.controller.showPopup('alert', options);
									}
								});
							}
						},error:function(e){

							var options = {
									"title" : App.settings.messageTitle.failureTitle,
									"message" : App.settings.messageSubject.connectionLoss
							}
							App.controller.showPopup('alert', options);
						}
					});
				
			}
			
			// Do something with the returned Parse.Object values
		},
		error: function(error) {
			var options = {
					"title" : App.settings.messageTitle.failureTitle,
					"message" : "No Internet Connection"
			}
			App.controller.showPopup('alert', options);
		}
	});

};