var PG_facebook_fb_clientID = '477583645687701';
// Facebook API Key
var PG_facebook_fb_secret = '7ac57f146a60a9096386ec327a927012';

function PG_loginFacebook() {
	App.pages.userLogin.$el.find('#userEmailAdd').val("");
	App.pages.userLogin.$el.find('#userPassword').val("");
	App.controller.showPopup('loader');
	var FB_token =window.localStorage.getItem("facebook_token"); 
	if(FB_token){
		userFBDetailsFetch(FB_token);
	}else{
		try{
			var my_client_id = PG_facebook_fb_clientID;
			var my_redirect_uri = "https://www.facebook.com/connect/login_success.html";
			var my_type = "user_agent";
			var my_display = "touch";
			var authorize_url = "https://graph.facebook.com/oauth/authorize?";
			authorize_url += "client_id=" + my_client_id;
			authorize_url += "&redirect_uri=" + my_redirect_uri;
			authorize_url += "&display=" + my_display;
			authorize_url += "&scope=read_stream,publish_stream,offline_access,publish_checkins";
			
			authorize_url =encodeURI(authorize_url);
			var iabRef = null;
			iabRef = window.open(authorize_url, '_blank', 'clearcache=yes,location=no');
			iabRef.addEventListener('loaderror', function(error) {
				console.log(JSON.stringify(error));
				iabRef.close();
			});
			
			iabRef.addEventListener('exit', function() {
				App.vent.trigger("closePopups"); 
			});
			
			iabRef.addEventListener('loadstart',
					function(loc, params) {
				var loc = JSON.stringify(loc);
				console.log("________________________loc__________________");
				console.log(JSON.stringify(loc));
				if (((loc.indexOf("http://www.facebook.com/connect/login_success.html") > -1) || (loc.indexOf("https://www.facebook.com/connect/login_success.html") > -1)) && loc.indexOf("&redirect_uri") == -1) {
					iabRef.close();
					var fbCode = loc.match(/code=(.*)$/)[1];
					if (!fbCode)
						return false;
					var redirectUriVar = "http://www.facebook.com/connect/login_success.html";
					if (loc.indexOf("https://www.facebook.com/connect/login_success.html") > -1) {
						redirectUriVar = "https://www.facebook.com/connect/login_success.html"
					}
					var fb_url = "https://graph.facebook.com/oauth/access_token?client_id="
						+ PG_facebook_fb_clientID
						+ "&client_secret="
						+ PG_facebook_fb_secret
						+ "&redirect_uri="
						+ redirectUriVar
						+ "&code=" + fbCode;
					console.log(fb_url);
					App.controller.showPopup('loader');
					$.ajax({
						url : fb_url,
						async : true,
						data : {},
						success : function(data, status) {
							App.vent.trigger("closePopups");
							var token = data.split("=")[1].split("&")[0];
							window.localStorage.setItem("facebook_token",token);
							console.log('D: got facebook token');
							iabRef.close();

							userFBDetailsFetch(token,"login");

						},

						error : function(error) {
							App.vent.trigger("closePopups");
							console.log("FB LOG: ERROR");
							console.log(JSON
									.stringify(error));
							var options = {
									"title":App.settings.messageTitle.failureTitle,
									"message":"Error in Facebook login, Please try again."
							}
							App.controller.showPopup('alert',options);

							try {
								iabRef.close();
							} catch (e) {
							}

						},
						dataType : 'text',
						type : 'POST'
					})

				}

			});

		} catch (e) {
			App.vent.trigger("closePopups");
			console.log("PG_loginFacebook: " + e)
		}
	}
}


function loginInParseFacebbok(data,dataFrom) {
	
	console.log("FB data"+JSON.stringify(data));
	App.controller.showPopup('loader');

	var myExpDate = new Date();
	myExpDate.setMonth(myExpDate.getMonth() + 2);
	myExpDate = myExpDate.toISOString();

	var facebookAuthData = {
		"id" : data.userInfo.id,
		"expiration_date" : myExpDate,
		"access_token" : data.token
		
	}

	Parse.FacebookUtils.logIn(facebookAuthData, {
		success : function(user) {
			console.log("______________Intalation_ID_:  "+App.sessionVars.pushInstallationId);
			user.set("Installation_Id", App.sessionVars.pushInstallationId);
			user.set("name",data.userInfo.first_name);
			user.set("email",data.userInfo.email);
			user.set("userFBProfilePicture",data.userInfo.userImage);
            user.save(null, {
                success : function(user) {
                	                                
                    App.models.user = new app.models.user();
                    App.models.user.set(user);
                    App.vent.trigger("closePopups");
                    if(dataFrom != "silent"){
	                    App.routers.mainRouter.navigate('#userHome', {
	                        trigger : true
	                    });
                    }
                }
            });
			
		},
		error : function(error1, error2) {
			App.vent.trigger("closePopups");
			var options = {
					"title":App.settings.messageTitle.failureTitle,
					"message":"Unable to create/login to as Facebook user. Please try again."
			}
			App.controller.showPopup('alert',options);
			console.log("Unable to create/login to as Facebook user");
			console.log("  ERROR1 = " + JSON.stringify(error1));
			console.log("  ERROR2 = " + JSON.stringify(error2));
		}
	});

}

function userFBDetailsFetch(token,fromData){
	App.controller.showPopup('loader');
	console.log(token);
	var userIdUrl = "https://graph.facebook.com/me?access_token="+token;
	console.log(userIdUrl);
	$.ajax({
		type : "GET",
		url : userIdUrl,
		async : true,
		data : {},
		dataType : "json",
		success : function(data) {
			App.vent.trigger("closePopups");
			console.log(JSON.stringify(data));
			var userData = {
					"userInfo" : {
						"id" : data.id,
						"email" : data.email,
						"first_name" : data.first_name,
						"last_name" : data.last_name
					},
					"token" : token
				}
			
			
			
			
			var userIdUrl = "http://graph.facebook.com/"+data.id+"/picture?redirect=0&height=200&type=normal&width=200";
			console.log(userIdUrl);
			$.ajax({
				type : "GET",
				url : userIdUrl,
				async : true,
				data : {},
				dataType : "json",
				success : function(data) {
					App.vent.trigger("closePopups");
					console.log(JSON.stringify(data));
					userData.userInfo.userImage = data.data.url;
					loginInParseFacebbok(userData,fromData);
				},
				error : function(error) {
					App.vent.trigger("closePopups");
					console.log("FB LOG: ERROR IN ME_Requrest");
					var options = {
							"title":App.settings.messageTitle.failureTitle,
							"message":"Unable to create/login to as Facebook user. Please try again."
					}
					App.controller.showPopup('alert',options);
					console.log(JSON.stringify(error));
				}
			})
		},
		error : function(error) {
			App.vent.trigger("closePopups");
			console.log("FB LOG: ERROR IN ME_Requrest");
			var options = {
					"title":App.settings.messageTitle.failureTitle,
					"message":"Unable to create/login to as Facebook user. Please try again."
			}
			App.controller.showPopup('alert',options);
			console.log(JSON.stringify(error));
		}
	})
}