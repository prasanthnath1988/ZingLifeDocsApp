<?php
echo(generate_random_string());
function generate_random_string($length = 10, $alphaNumericOnly = false) {
    $chars = " _?!@#~ 23456789 _?!@#~ AzBbCcDdEeFfGgHhJjKkMmNn _?!@#~ PpQqRrSsTtUuVvWwXxYyZz _?!@#~ 23456789 _?!@#~ ";
    if ($alphaNumericOnly) {
        $chars = preg_replace("/[^a-z0-9]/i", "", $chars);
    }
    $l = strlen($chars) - 1;
    $output = "";
    for ($i = 0; $i < $length; $i++) {
        $r = mt_rand(0, $l);
        $output.= $chars[$r];
    }
    return $output;
}
?>