/**
 * Name    : controller
 * Purpose : contains callbacks for application routes.
 * Params  : --
 * Returns : --
 **/
;
(function() {
	App.controller = {
			/**
			 * Name    : initialize
			 * Purpose : initializes the application controller and performs bootstrap operations.
			 * Params  : --
			 * Returns : --
			 **/
			initialize: function() {
				console.log('METHOD : App Controller initialize');
//				_.bindAll(this,"showUserLogin","userLoginFromParseAuto");
	
				App.bind('start', function(options) {
					// Operations to be performed immediately after app start.
				});
			},
			/**
			 * Name    : checkNetwork
			 * Purpose : Alerts the user of network failure.
			 * Params  : --
			 * Returns : --
			 **/
			checkNetwork: function() {
				App.controller.showPopup("alert", {
					title: App.settings.messageTitle.failureTitle,
					subject: App.settings.messageSubject.connectionLoss,
					message: App.settings.messages.connectionLossMsg
				});
			},
			/**
			 * Name    : beforeRoute
			 * Purpose : initializes the application controller and performs bootstrap operations.
			 * Params  : route - the url to navigate, args object containing e - event object, name - route name
			 * Returns : --
			 **/
			beforeRoute: function(route, args) {
				console.log('METHOD : App Controller beforeRoute');
				window.scrollTo(0, 0);
				var r;
				for (r in App.regions) {
					if (App.regions.hasOwnProperty(r)) {
						if (!App.regions[r].permanent) {
							if (App.regions[r]) {
								App.regions[r].close();
							}
						}
					}
				}
			},
			/**
			 * Name    : backButtonHandler
			 * Purpose : customized back button behaviour.
			 * Params  : --
			 * Returns : --
			 **/
			backButtonHandler: function(options) {
				console.log('METHOD : App Controller backButtonHandler');
				var manuallyHandled = false;
				if($("#popupRegionMask").hasClass("mask_active_h")){


					App.vent.trigger('closePopups');
					if(options && options.from=="userSettings"){
						manuallyHandled = false;						
					}else{
						manuallyHandled = true;
					}
				}else if(App.pages.header.menuOpened){
						App.pages.header.showMenu();
						manuallyHandled = true;
				}else if(App.pages.header.searchState){
					App.pages.header.hideSearch();
					manuallyHandled = true;
				}else{
					
					switch (App.sessionVars.currentPage) {
					case "userLogin":

						var options = {
							'callBack':function(){
								navigator.app.exitApp();
							}
					}
						App.controller.showPopup('confirm', options);
						manuallyHandled = true;

						break;
					case "userHome":
						var options = {
							'from':'exitPop',
							'callBack':function(){
								navigator.app.exitApp();
							}
					}
						App.controller.showPopup('confirm', options);
						manuallyHandled = true;
						break;
					case "imageDetails":
						if(App.sessionVars.imageDetailsTags == 'editDocs'){
							var options = {
									"title" : 'Are you sure to discard changes.',
									"callBack":function(){
										App.routers.mainRouter.navigate('#imageZoom', {
											trigger : true
										});
									}
							}
							App.controller.showPopup('confirm', options);
						}else{
							var options = {
									"title" : 'Are you sure to discard this image.',
									"callBack":function(){
										App.routers.mainRouter.navigate('#userHome', {
											trigger : true
										});
									}
							}
							App.controller.showPopup('confirm', options);
						}
						manuallyHandled = true;
						break;
					case "dataList":
						App.routers.mainRouter.navigate('#userHome', {
							trigger : true
						});
						manuallyHandled = true;
						break;
					case "imageZoom":
						App.routers.mainRouter.navigate('#dataList/'+App.sessionVars.tagName, {
							trigger : true
						});
						manuallyHandled = true;
						break;
					default:
						break;
					}
				}

				if (!manuallyHandled) {
					window.history.back();
				}
			},
			/**
			 * Name    : loadPage
			 * Purpose : loads the view into the dom (region).
			 * Params  : layoutName - Layout to be loaded, viewName - view name if body id should be similar to view name rather than layout name else null, options object to be passed to the view, region -
			 * Returns : --
			 **/
			loadPage: function(layoutName, viewName, options) {
				console.log('METHOD : App Controller loadPage');
				var view;
				// if (options) {
				if (viewName) {
					App.applyBodyID(viewName);
					App.sessionVars.currentPage = viewName;
				} else {
					App.applyBodyID(layoutName);
					App.sessionVars.currentPage = layoutName;
				}
				if (layoutName) {
					App.pages[layoutName] = new app.pages[layoutName](options);
					view = App.pages[layoutName];
				} else {
					App.views[viewName] = new app.views[viewName](options);
					view = App.views[viewName];
				}
				// } else {
				// if(layoutName){
				// App.pages[layoutName] = new app.pages[layoutName];
				// }else{
				// App.views[viewName] = new app.view[viewName]
				// }
				// }
				// view = App.pages[layoutName];
				if (App.pages.header) {
					App.pages.header.updateHeader();
				}
				if (App.pages.footer) {
					App.pages.footer.updateFooter();
				}

				App.regions.body.show(view);
				App.vent.trigger("page:load");
			},
			onPageLoad: function(e) {
				App.controller.setBodyHeight();
			},
			/**
			 * Name    : showPopup
			 * Purpose : Shows alerts, confirm or any other view in a popup style.
			 * Params  : name - name of the view to be shown as popup, options object containing noMask - to have a mask or not, topOffset - the top positon of the popup, black - the color of the overlay
			 * Returns : --
			 **/
//			showPopup: function(viewName, options, layoutName) {
//			console.log('METHOD : App Controller showPopup');
//			window.scrollTo(0, 0);
//			options = options || {};
//			$("#popupRegion").css({
//			'top': '30%'
//			});
//			if (viewName) {
//			popup = new app.views[viewName](options);
//			App.regions.popup.show(popup);
//			} else {
//			popup = new app.pages[layoutName](options);
//			App.regions.popup.show(popup);
//			}
//			App.controller.setMaskHeight();
//			$("#popupRegionMask").addClass("mask_active_h");
//			$("#popupRegion").show();
//			$("#popupRegionMask").show();
//			},


			showPopup : function(viewName, options, layoutName) {
				console.log('METHOD : App Controller showPopup');


				window.scrollTo(0, 0); 
				options = options || {};

				$("#popupRegion").css({
					'top' : '30%'
				});
				if(viewName){
					popup = new app.views[viewName](options);
					App.regions.popup.show(popup);
				}else{
					popup = new app.pages[layoutName](options);
					App.regions.popup.show(popup);
				}
				App.controller.setMaskHeight();
//				if (options.noMask) {
//				$("#popupRegionMask").css("background", "transparent");
//				} else {
//				$("#popupRegionMask").css({"background": "url(./imgs/body_bg2.jpg)", "background-size":"cover"});
//				}
				$("#popupRegionMask").addClass("mask_active_h");
				$("#popupRegion").show();
				$("#popupRegionMask").show();
			},

			/**
			 * Name    : popupsClose
			 * Purpose : Close all popups.
			 * Params  : --
			 * Returns : --
			 **/
			popupsClose: function() {
				console.log('METHOD : App Controller popupsClose');
				var bodyId = $('body').attr('id');
				App.controller.setBodyHeight();
				$('#bodyHeightRegion').css({
					'pointer-events': '',
					'overflow': 'auto'
				});
				$('#headerRegion').css({
					'pointer-events': ''
				});
				try {
					App.regions.popup.close();
				} catch (e) {
					console.log("ERROR: CLOSING POPUP: " + e);
				}
				$("#popupRegionMask").removeClass("mask_active_h");
				$("#popupRegion").hide();
				$("#popupRegionMask").hide();
			},
			setMaskHeight: function() {
				console.log("setMaskHeight");
				var popupHeight = $('#popupRegion').height() + 100;
				var maskHeight = (App.settings.deviceHeight < popupHeight) ? popupHeight : App.settings.deviceHeight;
				var bodyId = $("body").attr('id');
				window.scrollTo(0, 0);
				switch (bodyId) {
				case 'userLogin_Body':
				{
					$('#bodyHeightRegion').css({
						'height': (maskHeight)
					});
					$('#popupRegionMask').css({
						'opacity': '1',
						'height': maskHeight + 'px'
					});
				}
				break;
				case "userHome_Body":
				{
					$('#bodyHeightRegion').css({
						'height': (maskHeight)
					});
					$('#popupRegionMask').css({
						
						'opacity': '1',
						'height': maskHeight + 'px'
					});
				}
				break;
				default:
				{
					$('#bodyHeightRegion').css({
						'height': maskHeight
					});
					$('#popupRegionMask').css({
						'top': '0px',
						'opacity': '1',
						'height': maskHeight + 'px'
					});
				}
				break;
				}
				$('#headerRegion').css({
					'pointer-events': 'none'
				});
				$('#bodyHeightRegion').css({
					'pointer-events': 'none',
					'overflow': 'hidden'
				});
				$('#footerRegion').css({
					'position': 'absolute',
					'bottom': '0px'
				})
			},
			setBodyHeight : function(){
				console.log("setBodyHeight");
				var bodyId = $("body").attr('id');
				var maskHeight = App.settings.deviceHeight;
				switch(bodyId){
				case 'userLogin_Body' : {
					$('#bodyRegion').css('min-height', (maskHeight - ($('#headerRegion').height())));

					$('#bodyHeightRegion').css({'height': maskHeight});
				}
				break;
				
				case "userHome_Body":{
					$('#bodyRegion').css('min-height', (maskHeight-($('#headerRegion').height()+48)));
					$('#bodyHeightRegion').css({'height': maskHeight});
		        	if(navigator.userAgent.match(/(iPad|iPhone|iPod touch);.*CPU.*OS 7_\d/i)){
		        		$('#bodyRegion').css({'padding-top':'20px'});
		        	}else{
		        		$('#bodyRegion').css({'padding-top':'0px'});
		        	}
					
					
				}
				break;
				case "signUp_Body" :
				case "dataList_Body":{
					$('#bodyRegion').css('min-height', (maskHeight-($('#headerRegion').height()+48)));
		        	if(navigator.userAgent.match(/(iPad|iPhone|iPod touch);.*CPU.*OS 7_\d/i)){
		        		$('#bodyRegion').css({'padding-top':'68px'});
		        	}else{
		        		$('#bodyRegion').css({'padding-top':'48px'});
		        	}
					$('#bodyHeightRegion').css({'height': maskHeight});
				}
				break;
				default : {
					$('#bodyRegion').css('min-height', (maskHeight-($('#headerRegion').height()+48)));
		        	if(navigator.userAgent.match(/(iPad|iPhone|iPod touch);.*CPU.*OS 7_\d/i)){
		        		$('#bodyRegion').css({'padding-top':'68px'});
		        	}else{
		        		$('#bodyRegion').css({'padding-top':'48px'});
		        	}
					$('#bodyHeightRegion').css({'height': maskHeight});

				}
				break;
				}



			},
			/**
			 * Name    : index
			 * Purpose : Route Callback - navigates to the page where user was previously on if logged in else to landing page.
			 * Params  : --
			 * Returns : --
			 **/
			index: function() {
	
				var _this = this;
				if ((navigator.userAgent.toLocaleLowerCase().indexOf('android') != -1) || (navigator.userAgent.toLocaleLowerCase().indexOf('iphone') != -1) || (navigator.userAgent.toLocaleLowerCase().indexOf('ipod') != -1) || (navigator.userAgent.toLocaleLowerCase().indexOf('ipad') != -1)) {
                    try{
                        navigator.splashscreen.hide();
                    }catch(e){
                        console.log(e)
                    }
				}
				this.showUserLogin();
			},
			/**
			 * Name    : showMenu
			 * Purpose : loads the menu view in the dom.
			 * Params  : --
			 * Returns : --
			 **/
			showMenu: function() {
				console.log('METHOD : App Controller showMenu');
				$(App.regions.menu.$el).show();
				App.pages.menu = new app.pages.menu;
				App.regions.menu.show(App.pages.menu);
				//App.controller.setMaskHeight();
				
			},
			/**
			 * Name    : showHeader
			 * Purpose : loads the header view in the dom.
			 * Params  : --
			 * Returns : --
			 **/
			showHeader: function() {
				console.log('METHOD : App Controller showHeader');
				App.pages.header = new app.pages.header;
				App.regions.header.show(App.pages.header);
			},
			/**
			 * Name    : showFooter
			 * Purpose : loads the footer view in the dom.
			 * Params  : --
			 * Returns : --
			 **/
			showFooter: function() {
				console.log('METHOD : App Controller showHeader');
				App.pages.footer = new app.pages.footer;
				App.regions.footer.show(App.pages.footer);
			},
			/**
			 * Name    : showUserLogin
			 * Purpose : Router callback - navigates to the user login page.
			 * Params  : --
			 * Returns : --
			 **/
			showUserLogin: function(options) {

				var _this = this;
				console.log('METHOD : App Controller showUserLogin');
				//Below is the code for fetching the push notifications
				if(options != "logout"){
					if ((navigator.userAgent.toLocaleLowerCase().indexOf('android') != -1) || (navigator.userAgent.toLocaleLowerCase().indexOf('iphone') != -1) || (navigator.userAgent.toLocaleLowerCase().indexOf('ipod') != -1) || (navigator.userAgent.toLocaleLowerCase().indexOf('ipad') != -1)) {


						if(parsePlugin){

							parsePlugin.getInstallationId(function(id) {
								App.sessionVars.pushInstallationId = id;
								console.log("__________installation id is: "+App.sessionVars.pushInstallationId);
							}, function(e) {
								console.log('error' + e);
								var options = {
										"title":App.settings.messageTitle.failureTitle,
										"message":App.settings.messages.genericError,
								}
								App.controller.showPopup('alert',options);
							});
						} else{
							var options = {
									"title":App.settings.messageTitle.failureTitle,
									"message":App.settings.messages.genericError,
							}
							App.controller.showPopup('alert',options);
						}
						

						if((navigator.userAgent.toLocaleLowerCase().indexOf('android') != -1)){
							parsePlugin.getInstallationId(function(id) {
								App.sessionVars.pushInstallationId = id;
							}, function(e) {
								var options = {
										"title":App.settings.messageTitle.failureTitle,
										"message":"Something went wrong, please try again."
								}
								App.controller.showPopup('alert',options);
								console.log('error' + e);
							});

							// parsePlugin gets the installationId

							//webintent gets you the messages sent from php server.
							window.plugins.webintent.hasExtra("com.parse.Data", function(has) {
								if (has) {
									window.plugins.webintent.getExtra("com.parse.Data", function(response) {

										console.log(JSON.stringify(response));
										console.log("parse data---->   "+JSON.parse(response));

										var parseResponse = JSON.parse(response);
										var route =  parseResponse.action;
										var image_url = parseResponse.image_url;

										App.controller.showPopup('loader');
										if(Parse.User.current()){
											App.models.user = new app.models.user();
											App.models.user.set(Parse.User.current());
											App.vent.trigger("closePopups");
											App.routers.mainRouter.navigate(route + '/' + image_url, {
												trigger : true
											});
										}else{
											App.vent.trigger("closePopups");
											App.controller.loadPage('userLogin', null, null);
										}


									}, function() {
										_this.userLoginFromParseAuto();
										// There was no extra supplied.
									});
								} else {
									_this.userLoginFromParseAuto();
								}
							}, function() {
								var options = {
										"title":App.settings.messageTitle.failureTitle,
										"message":"Something went wrong, please try again."
								}
								App.controller.showPopup('alert',options);
							});
						}else{
							_this.userLoginFromParseAuto();

						}

					} else {
						_this.userLoginFromParseAuto();
					}

				}else{
					_this.userLoginFromParseAuto();
				}

			},
			/**
			 * Name    : showUserHome
			 * Purpose : Router callback - navigates to the user home page.
			 * Params  : --
			 * Returns : --
			 **/
			showUserHome: function() {
				console.log('METHOD : App Controller showUserHome');
				App.controller.loadPage('userHome', null, null);
			},

			/**
			 * Name    : showForgotPwd
			 * Purpose : Router callback - navigates to the forgot password page.
			 * Params  : --
			 * Returns : --
			 **/
			showForgotPwd: function() {
				console.log('METHOD : App Controller showForgotPwd');
				App.controller.loadPage('forgotPwd', null, null);
			},
			/**
			 * Name    : showSignUp
			 * Purpose : Router callback - navigates to the sign up  page.
			 * Params  : --
			 * Returns : --
			 **/
			showSignUp: function() {
				console.log('METHOD : App Controller showSignUp');
				App.controller.loadPage('signUp', null, null);
			},
			/**
			 * Name    : showDataList
			 * Purpose : Router callback - navigates to the data list page.
			 * Params  : --
			 * Returns : --
			 **/

			showDataList: function(tagId) {
				console.log('METHOD : App Controller showDataList');
				App.controller.loadPage('dataList', null, {
					'tagId' : tagId
				});
			},


			/**
			 * Name    : showImageDetails
			 * Purpose : Router callback - navigates to the showImageDetails page.
			 * Params  : --
			 * Returns : --
			 **/
			showImageDetails: function(imageUrl) {
				console.log('METHOD : App Controller showImageDetails');
				App.controller.loadPage('imageDetails', null,{
					'imageUrl' : imageUrl
				});
			},
			/**
			 * Name    : showSettings
			 * Purpose : Router callback - navigates to the settings page
			 * Params  : --
			 * Returns : --
			 **/
			showSettings:function(){
				console.log('METHOD : App Controller showSettingsCall');
				App.controller.loadPage('settings', null,null);
			},
			/**
			 * Name    : showContactUs
			 * Purpose : Router callback - navigates to the contactus page
			 * Params  : --
			 * Returns : --
			 **/
			showContactUs:function(){
				console.log('METHOD : App Controller showContactUs call');
				App.controller.loadPage('contactus', null,null);
			},
			showHowToUse :function(){
				console.log('METHOD: App Controller show HowToUse call');
				App.controller.loadPage('howtouse', null, null);
			},
			showImageZoomDetails : function(options){
				console.log('Method: App controleer showImageZoomDetails');
				App.controller.loadPage('imageZoom', null,options);
			},
			userLoginFromParseAuto:function(){
				//var _this = this;

				App.controller.showPopup('loader');
				if(Parse.User.current()){
					App.models.user = new app.models.user();
					App.models.user.set(Parse.User.current());
					App.vent.trigger("closePopups");
					App.routers.mainRouter.navigate('#userHome', {
						trigger : true
					});
				}else{
					App.vent.trigger("closePopups");
					App.controller.loadPage('userLogin', null, null);
				}

			}
	};
})();