/**
 * Name    : settings
 * Purpose : Local App Settings.
 * Params  : --
 * Returns : --
 **/
window.App = window.App || {};
App.settings = {
    server: "http://180.179.212.93/zingHR/",
    urls: {
        createUrl : 'getExpiryUrl.php'
    },
    messages : {
        blankFieldLoginMsg : 'Please fill the required fields',
        loginError : 'Invalid Username/Password, please try again',
        passwordMatchError : 'Passwords do not match, please try again',
        registerError : 'Invalid username, username must be atleast 6 characters long with no special characters',
        checkTermAndConditionsError: 'Please check terms and conditions before proceeding',
        logOutError : 'We could not log out. Please try again.',
        uploadSuccess: 'Image uploaded successfully',
        genericError : 'Oops, something went wrong at our end, please try again',
        userAlreadyExistsError : 'A user with this email id already exists',
        connectionLossMsg : 'Looks like we lost network connectivity',
        userPasswordUpdateErrorMsg : 'Unable to update user password. Please try again.',
        passwordChangedSuccessMsg : 'Password updated successfully',
        inValidEmailId : 'Please enter valid email Id.',
        inValidExpiry : 'Please enter valid expiry days.',
        inValidShare : 'Cannot share attachment via Whatsapp',        
        selectOption : 'Please select an option.',
        passwordNotMatchMsg : 'Passwords do not match. Try again.',
        acceptTermsAndConditions : 'Please accept terms and conditions.',
        discardDocument : 'Are you sure you want to discard this document?',
        imageUploadedSuccess : 'Image uploaded successfully. Do you want to upload more documents?',
        invalidSingupPassword : 'Invalid password, spaces are not allowed!',
        timeOutMessage : "Request time out. Please check network connection",
    },
    messageSubject : {
        contactAdmin : 'Contact Admin',
        connectionLoss : 'No Internet connection available on the device',
        loginError : 'Authentication Error',
        loginDataError : 'Authentication Error',
        blankField : 'Fields cannot be empty.',
        settingError : 'Settings Error',
        settingChanged : 'Settings changed',
        logOutError : 'Log out Error',
        userPasswordUpdateError : 'Password Update Error',
        passwordChanged : 'Password Changed',
        syncError : 'Sync Failed',
        assignSuccess : 'User Assigned',
        settingSuccess : 'Settings Saved',
        deviceError : 'This device is not yet configured',
        invalidPasswordErrorSub : 'Incorrect Password',
        noMoreQuestionSub : 'No more question left in this category.',
        resetPasswordLink : 'We have sent an email containing a temporary link that will allow you to reset your password for the next 24 hours.'
    },
    messageTitle : {
        failureTitle : 'Sorry!',
        successTitle : 'Success',
        resetPassword: 'Restore Password'
    },
    confirmationTitle : {
        logOutTitle : 'Are you sure you want to log out?'
    },
    isDeviceReady: false,
    production_mode: false, //SUPPRESSES ALL DEBUGGING ALERTS & LOGS
    appPaused: false,
    screenHeight: null,
    windowHeight: null,
    isDesktop : false
    
};