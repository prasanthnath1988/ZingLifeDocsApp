;(function() {
    app.pages.dataList = Backbone.Marionette.Layout.extend({
        template : '#dataListLayout',
        regions : {
            content : ".content_wrapper_h"
        },
        events : {
            'click .upload_doc_h' : 'showUpload'
        },
        init : function() {
            console.log("data list LAYOUT INIT");
            App.views.dataList = new app.views.dataList(this.options);
            this.content.show(App.views.dataList);
        },
        showUpload : function() {
            App.controller.showPopup('uploadOptions');
        },
        onCloseLayout : function() {
            console.log("LAYOUT CLOSE");
        }
    });
})();
