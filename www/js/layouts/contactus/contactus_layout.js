 ;
 (function() {
         app.pages.contactus = Backbone.Marionette.Layout.extend({
             template: '#contactusLayout',
             regions: {
                 content: ".content_wrapper_h"
             },
             init: function() {
                 console.log("CONTACT US LAYOUT INIT");
             },
             onCloseLayout: function() {
                 console.log("CONTACT US LAYOUT CLOSE");
             }
         });
     })();