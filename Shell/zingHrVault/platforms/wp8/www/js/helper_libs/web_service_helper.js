/**
 * @author Nivedha Kaliaperumal
 * Ajax Web Service Calls Helper.
 * Description : Defines method for calling web services through ajax.
 */
(function() {
    $.ajaxSetup({
        cache: false,
        headers: {
            'Cache-Control': 'no-cache'
        }
    });
    App.communication = {
        /**
     * Name    : GET
     * Purpose : Method for making the GET request service call.
     * Params  : options - json objects containing (url - The service url to call, data - the query params to be passed,
     successCallback  - On Success, the control will be given back to the function
     errorCallback - On Error, the control will be given back to the function,
     dataType - indicates the response type.)
     * Returns : --
     **/
        GET: function(options) {
            console.log(JSON.stringify(options));
            $.ajax({
                type: "GET",
                url: App.settings.server + options.url,
                data: options.data,
                dataType: "json",
                async: options.async || true,
                timeout: 20000,
//                beforeSend: function(jqXHR, settings) {
//                    jqXHR.setRequestHeader('X-Parse-Application-Id', 'BJYBrQf1wjRwvAb4pcpnuPxRr205QjmhXjNSPc1D');
//                    jqXHR.setRequestHeader('X-Parse-REST-API-Key', 'Y3k9ejJm2zymQsQ1wxcRZDAGJQwgansTi4jNnb8k');
//                },
                complete: function(jqXHR, textStatus) {},
                success: function(data, textStatus, jqXHR) {
                    console.log(JSON.stringify(data));
                    if (data) {
                        if (options.successCallback) {
                            options.successCallback(data);
                            
                            
                        }
                    } else {
                        var errorOptions = {
                            "jqXHR": jqXHR,
                            "textStatus": "error",
                            "errorThrown": "no records found"
                        };
                        if (options.errorCallback) {
                            options.errorCallback(errorOptions);
                        }
                    }
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    var errorOptions = {
                        "jqXHR": jqXHR,
                        "textStatus": textStatus,
                        "errorThrown": errorThrown
                    };
                    if (errorThrown) {
                        console.log('Error message ' + errorThrown);
                    }
                    if (jqXHR.status == 0) {
                        App.vent.trigger("checkNetworkStatus");
                    }
                    if (options.errorCallback) {
                        options.errorCallback(errorOptions);
                    }
                }
            });
        },
        /**
     * Name    : POST
     * Purpose : Method for making the POST request service call
     * Params  : options - json object containing (url - The service url to call,
     data - Request body to be sent to the server
     successCallback  - On Success, the control will be given back to the function
     errorCallback - On Error, the control will be given back to the function,
     dataType - indicates the response type.)
     * Returns : --
     **/
        POST: function(options) {
            console.log(JSON.stringify(options))
            $.ajax({
                url: App.settings.server + options.url,
                data: options.data,
                dataType: "json",
                type: 'POST',
                async: options.async || true,
                timeout: 20000,
                complete: function(jqXHR, textStatus) {},
                beforeSend: function(jqXHR, settings) {
                    jqXHR.setRequestHeader('X-Parse-Application-Id', 'BJYBrQf1wjRwvAb4pcpnuPxRr205QjmhXjNSPc1D');
                    jqXHR.setRequestHeader('X-Parse-REST-API-Key', 'Y3k9ejJm2zymQsQ1wxcRZDAGJQwgansTi4jNnb8k');
                    jqXHR.setRequestHeader('Content-Type', 'application/json');
                },
                success: function(data, textStatus, jqXHR) {
                    console.log(JSON.stringify(data));
                    if (options.batch) {
                        var error = _.filter(data, function(key) {
                            console.log(key);
                            if (key.error) {
                                console.log("exists");
                                return 1;
                            }
                        });
                        if (error && error.length) {
                            var errorOptions = {
                                "jqXHR": jqXHR,
                                "textStatus": "error",
                                "errorThrown": "some records not updated"
                            };
                            if (options.errorCallback) {
                                options.errorCallback(errorOptions);
                            }
                        } else {
                            if (options.successCallback) {
                                options.successCallback(data);
                            }
                        }
                    } else {
                        if (data && data.objectId) {
                            if (options.successCallback) {
                                options.successCallback(data);
                            }
                        } else {
                            var errorOptions = {
                                "jqXHR": jqXHR,
                                "textStatus": "error",
                                "errorThrown": "object not created"
                            };
                            if (options.errorCallback) {
                                options.errorCallback(errorOptions);
                            }
                        }
                    }
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    var errorOptions = {
                        "jqXHR": jqXHR,
                        "textStatus": textStatus,
                        "errorThrown": errorThrown
                    };
                    if (errorThrown) {
                        console.log('Error message ' + errorThrown);
                    }
                    if (jqXHR.status == 0) {
                        App.vent.trigger("checkNetworkStatus");
                    }
                    if (options.errorCallback) {
                        options.errorCallback(errorOptions);
                    }
                }
            });
        },
        /**
     * Name    : DELETE
     * Purpose : Method for making the DELETE request service call
     * Params  : options - json object containing (url - The service url to call,
     data - Request body to be sent to the server
     successCallback  - On Success, the control will be given back to the function
     errorCallback - On Error, the control will be given back to the function,
     dataType - indicates the response type.)
     * Returns : --
     **/
        DELETE: function(options) {
            console.log(JSON.stringify(options))
            $.ajax({
                url: App.settings.server + options.url,
                data: options.data,
                dataType: options.dataType || "json",
                async: options.async || true,
                type: 'DELETE',
                timeout: 20000,
                complete: function(jqXHR, textStatus) {},
                success: function(data, textStatus, jqXHR) {
                    if (data && data.success && data.data && data.data.response) {
                        if (options.successCallback) {
                            options.successCallback(data.data.response);
                        }
                    } else {
                        var errorOptions = {
                            "jqXHR": jqXHR,
                            "textStatus": "error",
                            "errorThrown": data.data.message
                        };
                        if (options.errorCallback) {
                            options.errorCallback(errorOptions);
                        }
                    }
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    var errorOptions = {
                        "jqXHR": jqXHR,
                        "textStatus": textStatus,
                        "errorThrown": errorThrown
                    };
                    if (errorThrown) {
                        console.log('Error message ' + errorThrown);
                    }
                    if (jqXHR.status == 0) {
                        App.vent.trigger("checkNetworkStatus");
                    }
                    if (options.errorCallback) {
                        options.errorCallback(errorOptions);
                    }
                }
            });
        },
        /**
     * Name    : PUT
     * Purpose : Method for making the PUT request service call
     * Params  : options - json object containing (url - The service url to call,
     data - Request body to be sent to the server
     successCallback  - On Success, the control will be given back to the function
     errorCallback - On Error, the control will be given back to the function,
     dataType - indicates the response type.)
     * Returns : --
     **/
        PUT: function(options) {
            console.log(JSON.stringify(options));
            $.ajax({
                url: App.settings.server + options.url,
                data: options.data,
                dataType: "json",
                async: options.async || true,
                type: 'PUT',
                timeout: 20000,
                complete: function(jqXHR, textStatus) {},
                beforeSend: function(jqXHR, settings) {
                    jqXHR.setRequestHeader('X-Parse-Application-Id', 'BJYBrQf1wjRwvAb4pcpnuPxRr205QjmhXjNSPc1D');
                    jqXHR.setRequestHeader('X-Parse-REST-API-Key', 'Y3k9ejJm2zymQsQ1wxcRZDAGJQwgansTi4jNnb8k');
                    jqXHR.setRequestHeader('Content-Type', 'application/json');
                },
                success: function(data, textStatus, jqXHR) {
                    console.log(JSON.stringify(data));
                    if (data && data.updatedAt) {
                        if (options.successCallback) {
                            options.successCallback(data);
                        }
                    } else {
                        var errorOptions = {
                            "jqXHR": jqXHR,
                            "textStatus": "error",
                            "errorThrown": "record not updated"
                        };
                        if (options.errorCallback) {
                            options.errorCallback(errorOptions);
                        }
                    }
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    var errorOptions = {
                        "jqXHR": jqXHR,
                        "textStatus": textStatus,
                        "errorThrown": errorThrown
                    };
                    if (errorThrown) {
                        console.log('Error message ' + errorThrown);
                    }
                    if (jqXHR.status == 0) {
                        App.vent.trigger("checkNetworkStatus");
                    }
                    if (options.errorCallback) {
                        options.errorCallback(errorOptions);
                    }
                }
            });
        }
    };
})();