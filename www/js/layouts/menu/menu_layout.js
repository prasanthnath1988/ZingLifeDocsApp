;
(function() {
	app.pages.menu = Backbone.Marionette.Layout.extend({
		template : '#menuLayout',
		events : {
			'click .m_logout_h':"doLogout",
			'click .m_home_h':"showHomeScreen",
			'click .m_upload_h':"showUploadScreen",
			'click .m_how_to_use_h':'showHowToUseScreen',
			'click .m_contactus_h':'showContactUsScreen',
			'click .m_settings_h':'showSettingsScreen',
			'click .menu_outer_h':'hideMenu',
			'click .fb_page_h':'showFBPage',
			'click .twitter_page_h':'showTwitterPage'
		},
		init : function() {
			console.log("footer LAYOUT INIT");
		},
		onCloseLayout : function() {
			console.log("LAYOUT CLOSE");
		},
		doLogout : function(){
			App.pages.header.showMenu();

			var checkNetworkConnection = PG_checkNetwork();
			if (checkNetworkConnection.result) {
				var options = {
						'title':'Do you want to logout?',
						'callBack':function(){
							App.controller.showPopup('loader');
							window.localStorage.removeItem('facebook_token');
							window.localStorage.removeItem('user_emailId');
							window.localStorage.removeItem('user_Password');

							App.pages.header.hideSearch();
							if(App.pages.header.menuOpened){
								App.pages.header.showMenu();
							}


							var CurrentUser = Parse.User.current();
							CurrentUser.set("Installation_Id","");
							CurrentUser.save(null, {
								success : function(user) {

									console.log("_____________Logout Success_________");
									console.log(JSON.stringify(user));
									Parse.User.logOut();
									window.localStorage.clear();
									App.vent.trigger("closePopups");
									App.routers.mainRouter.navigate('#userLogin/logout', {
										trigger : true
									});

								}
							,fail:function(error){
								App.vent.trigger("closePopups");
								var options = {
										"title" : App.settings.messageTitle.failureTitle,
										"message" : "Unable to logout. Please try again."
								}
								App.controller.showPopup('alert', options);
							}
							});

						},
						'from':'logout'
				}
				App.controller.showPopup('confirm', options);
			}else{
				App.controller.showPopup('alert', {
					"title" : App.settings.messageTitle.failureTitle,
					"message" : App.settings.messageSubject.connectionLoss
				});
			}


		},



		showHomeScreen : function(){
			App.pages.header.showMenu();
			App.routers.mainRouter.navigate('#userHome', {
				trigger : true
			});
		},
		showUploadScreen: function(){
			App.pages.header.showMenu();
			App.sessionVars.mainTagsFalse=true;
			App.controller.showPopup('uploadOptions');
		},
		showHowToUseScreen : function(){
			App.pages.header.showMenu();
			App.routers.mainRouter.navigate('#howtouse', {
				trigger : true
			});
		},
		showContactUsScreen : function(){
			App.pages.header.showMenu();
			App.routers.mainRouter.navigate('#contactus', {
				trigger : true
			});
		},
		showSettingsScreen : function(){
			App.pages.header.showMenu();
			App.routers.mainRouter.navigate('#settings', {
				trigger : true
			});
		},
		onRender : function(){


			if(App.models.user && App.models.user.get('attributes.userProfilePicture')){
				this.$el.find('.user_menu_img_h').css('background','url("'+App.models.user.get('attributes.userProfilePicture')+'") no-repeat');
				this.$el.find('.user_menu_img_h').css('background-size','cover');
			}else if(App.models.user && App.models.user.get('attributes.userFBProfilePicture')){
				this.$el.find('.user_menu_img_h').css('background','url("'+App.models.user.get('attributes.userFBProfilePicture')+'") no-repeat');
				this.$el.find('.user_menu_img_h').css('background-size','cover');
			}

			if(App.models.user && App.models.user.get('attributes.name')){
				this.$el.find('.user_name_h').text(App.models.user.get('attributes.name'));
				if(App.models.user.get('attributes.email')){
					this.$el.find('.user_email_h').text(App.models.user.get('attributes.email'));
				}

			}
		},
		hideMenu : function(){
			App.pages.header.hideSearch();
			if(App.pages.header.menuOpened){
				App.pages.header.showMenu();
			}
		},
		showFBPage : function(){
			this.hideMenu();
			var checkNetworkConnection = PG_checkNetwork();
			if (checkNetworkConnection.result) {

				var fbPageUrl = "https://www.facebook.com/zinghr"
					var iabRef = null;
				iabRef = window.open(fbPageUrl, '_blank', 'location=yes');
				iabRef.addEventListener('loaderror', function(error) {
					console.log(JSON.stringify(error));
					App.controller.showPopup('alert', {
						"title" : App.settings.messageTitle.failureTitle,
						"message" : App.settings.messageSubject.error.message
					});
					iabRef.close();
				});

				iabRef.addEventListener('exit', function() {
					App.vent.trigger("closePopups"); 
				});


			}else{
				App.controller.showPopup('alert', {
					"title" : App.settings.messageTitle.failureTitle,
					"message" : App.settings.messageSubject.connectionLoss
				});
			}
		},
		showTwitterPage : function(){
			this.hideMenu();
			var checkNetworkConnection = PG_checkNetwork();
			if (checkNetworkConnection.result) {

				var twitterPageUrl = "https://twitter.com/zinghr"
					var iabRef = null;
				iabRef = window.open(twitterPageUrl, '_blank', 'location=yes');
				iabRef.addEventListener('loaderror', function(error) {
					console.log(JSON.stringify(error));
					App.controller.showPopup('alert', {
						"title" : App.settings.messageTitle.failureTitle,
						"message" : App.settings.messageSubject.error.message
					});
					iabRef.close();
				});

				iabRef.addEventListener('exit', function() {
					App.vent.trigger("closePopups"); 
				});

			}else{
				App.controller.showPopup('alert', {
					"title" : App.settings.messageTitle.failureTitle,
					"message" : App.settings.messageSubject.connectionLoss
				});
			}
		}



	});
})();
