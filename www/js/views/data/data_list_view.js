;(function() {
    app.views.dataList = Backbone.Marionette.ItemView.extend({
        template : '#dataListView',
        follow_model_changes : true,
        events : {
        	'click .image_zoom_details_h':'showImageZoom'

        },
        initialize : function() {

        },
        onRender : function() {
        	console.log("in render");
        },
        onClose : function() {
            console.log("VIEW CLOSE");
        },
        showImageZoom : function(e){
        	console.log(e.currentTarget);
        	var imageUrl = $(e.currentTarget).attr('imageUrl');
        	var imageName =$(e.currentTarget).attr('imageName');
        	var imageTags =$(e.currentTarget).attr('imageTags');
        	
        	var imageObjectId = $(e.currentTarget).attr('document_object_id');
        	App.sessionVars.imageZoomOptions = {
        			'imageUrl':imageUrl,
        			'imageName':imageName,
        			'imageTags':imageTags,
        			'imageObjectId':imageObjectId
        	} 
        	App.routers.mainRouter.navigate('#imageZoom', {
				trigger : true
			});
        }
    });
    app.views.dataList = Backbone.Marionette.CollectionView.extend({
        itemView : app.views.dataList,
        follow_model_changes : true,
        events : {

        },
        initialize : function() {
            var _this = this;
            this.collection = App.collections.tagsList = new app.collections.tagsList();
            if(this.options.tagId != "searchData"){
            	App.sessionVars.tagName = this.options.tagId;
				var uploadedImages = Parse.Object.extend("user_uploaded_images");
            var query = new Parse.Query(uploadedImages);
            var user_name = null;
            var inputText = [];
            if(App.models.user && App.models.user.get("attributes.username")){
				user_name = App.models.user.get("attributes.username")	
			}else{
				App.routers.mainRouter.navigate('#userLogin/logout', {
					trigger : true
				})
			}
            query.equalTo("username",user_name );
            query.matches('DocumentsTags',this.options.tagId);
            App.controller.showPopup('loader');
            query.find({
                success : function(results) {
                    App.vent.trigger('closePopups');
                    if(results.length>0){
                    	var i = 0;
                        while (i < results.length) {
                            inputText.push(results[i].attributes);
                            inputText[i].objectId=results[i].id;
                            inputText[i].noResult = "";
                            i++;
                        }
                        _this.collection.reset(inputText);
                    }else{
                    	var noResult = {"noResult":"No result found"}
                    	inputText.push(noResult);
                    	_this.collection.reset(inputText);
                    	
                    }
                    
                },
                error : function(e) {
                    console.log("error" + e);
                    App.vent.trigger('closePopups');
                }
            });
			}else{
				App.sessionVars.mainTagsFalse=true;
				_this.collection.reset(App.sessionVars.searchResult);
				
			}

        },
        onRender : function() {
        	console.log("in render");
        },
        onClose : function() {
            console.log("VIEW CLOSE");
        },
    });
})();
