window.app = window.app || {};
app.views = app.views || {};
app.views.loader = Backbone.Marionette.ItemView.extend({
    template: '#loaderView',
    events: {},
    initialize: function() {
        App.vent.trigger("closePopups");
    },
    onRender: function() {
        var _this = this;
        if (this.options.autoLogin) {
            this.$el.find('.auto_login_msg_h').show();
            if (!$('body').hasClass('grm_brd')) {
                $('body').addClass('grm_brd');
            }
        } else {
            this.$el.find('.auto_login_msg_h').hide();
        }
    },
    onClose: function() {}
});