;(function() {
    app.views.dataList = Backbone.Marionette.ItemView.extend({
        template : '#dataListView',
        follow_model_changes : true,
        events : {
        	'click .image_zoom_details_h':'showImageZoom'

        },
        initialize : function() {

        },
        onRender : function() {
        	console.log("in render");
            $(App.regions.header.el).find('.header_title_h').text(App.sessionVars.tagName);
            $(App.regions.header.el).find('.header_title_h').show();
            $(App.regions.header.el).find('.header_title_h').css('margin-left', '0px');
        },
        onClose : function() {
            console.log("VIEW CLOSE");
        },
        showImageZoom : function(e){
        	console.log(e.currentTarget);
        	var imageUrl = $(e.currentTarget).attr('imageUrl');
        	var imageName =$(e.currentTarget).attr('imageName');
        	var imageTags =$(e.currentTarget).attr('imageTags');
        	var objectId =$(e.currentTarget).attr('document_object_id');
        	var pdfUrl = $(e.currentTarget).attr('pdfUrl');
        	App.sessionVars.imageZoomOptions = {
        			'imageUrl':imageUrl,
        			'imageName':imageName,
        			'imageTags':imageTags,
        			'pdfUrl':pdfUrl,
                    'objectId':objectId
        	}; 
        	App.routers.mainRouter.navigate('#imageZoom', {
				trigger : true
			});
        }
    });
    app.views.dataList = Backbone.Marionette.CollectionView.extend({
        itemView : app.views.dataList,
        follow_model_changes : true,
        events : {

        },
        initialize : function() {
            var _this = this;
            this.collection = App.collections.tagsList = new app.collections.tagsList();
            if(this.options.tagId != "searchData" && App.sessionVars.listType == 'tag'){
            	App.sessionVars.tagName = this.options.tagId;
				var uploadedImages = Parse.Object.extend("user_uploaded_images");
            var query = new Parse.Query(uploadedImages);
            var user_name = null;
            var inputText = [];
            if(App.models.user && App.models.user.get("attributes.username")){
				user_name = App.models.user.get("attributes.username");	
			}else{
				App.routers.mainRouter.navigate('#userLogin/logout', {
					trigger : true
				});
			}
            query.equalTo("username",user_name );
            query.matches('DocumentsTags',this.options.tagId);
            App.controller.showPopup('loader');
            query.find({
                success : function(results) {
                    App.vent.trigger('closePopups');
                    if(results.length>0){
                    	var i = 0;
                        while (i < results.length) {
                            inputText.push(results[i].attributes);
                            inputText[i].objectId=results[i].id;
                            inputText[i].noResult = "";
                            i++;
                        }
                        _this.collection.reset(inputText);
                    }else{
                    	var noResult = {"noResult":"No Document Found"};
                    	inputText.push(noResult);
                    	_this.collection.reset(inputText);
                    	
                    }
                    
                },
                error : function(e) {
                    console.log("error" + e);
                    App.vent.trigger('closePopups');
                }
            });
			}else{
				App.sessionVars.mainTagsFalse=true;
				                _this.collection.reset(App.sessionVars.searchResult);                
                //_this.collection.reset(App.sessionVars.searchResult);        




            var user_name = null;
            if (App.models.user && App.models.user.get("attributes.username")) {
                user_name = App.models.user.get("attributes.username");
            } else {
                App.routers.mainRouter.navigate('#userLogin/logout', {
                    trigger : true
                });
            }

            var _this = this;
            var newString = $('.search_text_h').val().replace(/\s+/g, ' ').trim();
            if (!newString) {
                var options = {
                    "title" : App.settings.messageTitle.failureTitle,
                    "message" : "Please provide valid tags to search."
                };
                App.controller.showPopup('alert', options);
                return false;
            }

            var tags = newString.split(' ');
            App.sessionVars.tagName = tags;
            if (tags[0] && tags.length > 0) {
                var searchString = new Array();
                var searchTable = Parse.Object.extend('user_uploaded_images');

                for ( i = 0; i < tags.length; i++) {
                    console.log(tags[i]);
                    searchString[i] = new Parse.Query(searchTable);
                    searchString[i].matches('DocumentsTags', tags[i]);
                    searchString[i].equalTo("username", user_name);

                }
                for ( i = 0; i < tags.length; i++) {
                    console.log(tags[i]);
                    searchString[searchString.length] = new Parse.Query(searchTable);
                    searchString[searchString.length - 1].matches('DocumentName', tags[i]);
                    searchString[searchString.length - 1].equalTo("username", user_name);

                }

                var checkNetworkConnection = PG_checkNetwork();
                if (checkNetworkConnection.result) {
                    var MainQueryParse = Parse.Query.or.apply(Parse.Query, searchString);
                    App.controller.showPopup('loader');
                    MainQueryParse.find({
                        success : function(result) {
                            App.vent.trigger("closePopups");
                            if (result.length > 0) {
                                console.log(JSON.stringify(result))
                                App.sessionVars.searchResult = new Array();
                                var i = 0;
                                while (i < result.length) {
                                    App.sessionVars.searchResult.push(result[i].attributes);
                                    App.sessionVars.searchResult[i].objectId = result[i].id;
                                    App.sessionVars.searchResult[i].noResult = "";
                                    i++;
                                }
                                
                                App.sessionVars.listType = 'search';
                                if (App.sessionVars.currentPage == "dataList") {
                                    App.collections.tagsList.reset(App.sessionVars.searchResult);
                                } else {                                    
                                    App.routers.mainRouter.navigate('#dataList/' + "searchData", {
                                        trigger : true
                                    });
                                }

                            } else {
                                var options = {
                                    "title" : App.settings.messageTitle.failureTitle,
                                    "message" : "No Document Found"
                                };
                                App.controller.showPopup('alert', options);
                            }

                        },
                        fail : function(error) {
                            App.vent.trigger("closePopups");
                            console.log(JSON.stringify(error))
                        }
                    });
                } else {
                    App.controller.showPopup('alert', {
                        "title" : App.settings.messageTitle.failureTitle,
                        "message" : App.settings.messageSubject.connectionLoss
                    });
                }
            }



				
			}

        },
        onRender : function() {
        	console.log("in render");
        },
        onClose : function() {
            console.log("VIEW CLOSE");
        },
    });
})();
