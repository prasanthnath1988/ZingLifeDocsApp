;
(function() {
    app.pages.imageDetails = Backbone.Marionette.Layout.extend({
        template : '#imageDetailsLayout',
        regions : {
            content : ".content_wrapper_h"
        },

        init : function() {
            console.log("data list LAYOUT INIT");
            App.views.imageDetails = new app.views.imageDetails(this.options);
            this.content.show(App.views.imageDetails);
        },

        onCloseLayout : function() {
            console.log("LAYOUT CLOSE");
        }
    });
})();
