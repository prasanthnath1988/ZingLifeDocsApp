;
(function() {
    app.pages.imageZoom = Backbone.Marionette.Layout.extend({
        template : '#imageZoomLayout',
        regions : {
            content : ".content_wrapper_h"
        },

        init : function() {
            console.log("data list LAYOUT INIT");
            App.views.imageZoom = new app.views.imageZoom(this.options);
            this.content.show(App.views.imageZoom);
        },

        onCloseLayout : function() {
            console.log("LAYOUT CLOSE");
        }
    });
})();
