﻿

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Runtime.Serialization;
using System.Windows;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Notification;
using Microsoft.Phone.Shell;
using Newtonsoft.Json;
using com.zinghr.zingvault;
using Parse;

namespace WPCordovaClassLib.Cordova.Commands
{
    public class ParsePlugin : BaseCommand
    {
        String installationIdError = "Fail to fetch instalation id. Please try again.";
        public void getInstallationId(string options)
        {
            var curInst = ParseInstallation.CurrentInstallation;     

            if(curInst.InstallationId == null ){
                this.DispatchCommandResult(new PluginResult(PluginResult.Status.ERROR, installationIdError));
            }else{
                this.DispatchCommandResult(new PluginResult(PluginResult.Status.OK, curInst.InstallationId));
            }

           
        }

        public void getInstallationObjectId(string options)
        {
            var curInst = ParseInstallation.CurrentInstallation;

            if (curInst.ObjectId == null)
            {
                this.DispatchCommandResult(new PluginResult(PluginResult.Status.ERROR, installationIdError));
            }
            else
            {
                this.DispatchCommandResult(new PluginResult(PluginResult.Status.OK, curInst.ObjectId));
            }


        }

    }
}