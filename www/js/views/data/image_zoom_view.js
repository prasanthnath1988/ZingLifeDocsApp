;
(function () {
    app.views.imageZoom = Backbone.Marionette.ItemView.extend({
        template: '#imageZoomView',
        events: {

            "click .edit_document_h": "showDocumentEditScreen",
            "click .share_document_h": "showSharePopup"

        },
        initialize: function () {

            _.bindAll(this, 'onRender', 'showSharePopup');

        },
        onRender: function () {

            var _this = this;
            this.$el.find('#imagesIds').attr('src', "");
            this.options.imageUrl ? this.$el.find('.doc_img').children("img").attr('src', this.options.imageUrl) : this.$el.find('#imagesIds').attr('src', App.sessionVars.imageZoomOptions.imageUrl);
            this.$el.find('.image_name_h').html(App.sessionVars.imageZoomOptions.imageName);

            var tagsNewString = App.sessionVars.imageZoomOptions.imageTags.replace(/\s+/g, ' ').trim().toLowerCase();
            var tagsArray = tagsNewString.split(' ');
            this.$el.find('.image_tags_h').html('');
            for (var i = 0; i < tagsArray.length; i++) {
                this.$el.find('.image_tags_h').append('<span class="tag">' + tagsArray[i] + '</span>');
            }
            setTimeout(function () {
                if (_this.imageZoomView) {
                    _this.imageZoomView.refresh();
                } else {
                    _this.imageZoomView = new iScroll("imageToUpload", {
                        hideScrollbar: true,
                        zoom: true,
                        // So Swiper will not swipe/slide when zooming is enabled
                        //					      onZoomEnd: function(e) {
                        //					        var slide = $(this.wrapper);
                        //
                        //					        if(parseInt(this.scale) == 1) {
                        //					          slide.removeClass('swiper-no-swiping');
                        //					        } else {
                        //					          slide.addClass('swiper-no-swiping');
                        //					        }
                        //					      }
                    });
                    _this.imageZoomView.refresh();
                }

            }, 200)

        },
        onClose: function () {
            console.log("VIEW CLOSE");
        },
        showSharePopup: function () {
            var _this = this;
            var userName = App.models.user.get("attributes.username");
            var expiryDays = 5;
            if (App.models.user && App.models.user.get("attributes.linkExpiryDays")) {
                expiryDays = App.models.user.get("attributes.linkExpiryDays");
            }
            var checkNetworkConnection = PG_checkNetwork();
            if (checkNetworkConnection.result) {
                _this.createShareURL(App.sessionVars.imageZoomOptions.imageUrl, userName, expiryDays);
            } else {
                App.controller.showPopup('alert', {
                    "title": App.settings.messageTitle.failureTitle,
                    "message": App.settings.messageSubject.connectionLoss
                });
            }
        },
        showDocumentEditScreen: function () {
            App.routers.mainRouter.navigate('#imageDetails/editDocs', {
                trigger: true
            })

        },
        createShareURL: function (imageUrl, userName, expiryDays) {
            var paramasData = {
                "userid": userName,
                "image_url": imageUrl,
                "expires_in": expiryDays
            }

            App.controller.showPopup('loader');
            App.communication.GET({
                data: paramasData,
                url: App.settings.urls.createUrl,
                successCallback: function (data) {
                    App.vent.trigger("closePopups");
                    console.log('success');
                    console.log(data);
                    if (data && data.url) {
                        var user = App.models.user.get('attributes.name') ? App.models.user.get('attributes.name') : "Anonymous";
                        window.plugin.email.isServiceAvailable(
                            function (isAvailable) {
                                window.plugin.email.open({
                                    to: [],
                                    //cc:      ['erika.mustermann@appplant.de'],
                                    //bcc:     ['john.doe@appplant.com', 'jane.doe@appplant.com'],
                                    subject: user.toUpperCase() + ' has shared a document -' + App.sessionVars.imageZoomOptions.imageName,
                                    body: user.toUpperCase() + ' has shared a document -' + App.sessionVars.imageZoomOptions.imageName + ' with you using Zing Lifedocs. <br/><br/> You can access this document on this link:- <a href="http://' + data.url + '" style="border:none;color:#0084b4;text-decoration:none" target="_blank">Click Here</a> <br/><br/> Zing Lifedocs mobile is a secure and easy way to save important documents in your life and share them. Click to download - iOS, Android, Windows 8. ',
                                    isHtml: true
                                });




                            }
                        );
                    } else {
                        return "error";
                    }

                },
                errorCallback: function (options) {
                    App.vent.trigger("closePopups");
                    return "error";

                }
            });
        }
    });
})();
